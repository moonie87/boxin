﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]      
        public static int RegisterUser(string userName, string Email, string FName, string LName,string Password,string outh_provider, string outhId)
        {

            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                 
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Users_AddUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userName", userName));
                    stmt.Parameters.Add(new SqlParameter("@ContactEmail", Email));
                    stmt.Parameters.Add(new SqlParameter("@LoginFirstName", FName));
                    stmt.Parameters.Add(new SqlParameter("@LoginLastName", LName));
                    stmt.Parameters.Add(new SqlParameter("@Password", Password));
                    stmt.Parameters.Add(new SqlParameter("@outh_provider", outh_provider));
                    stmt.Parameters.Add(new SqlParameter("@outhId", outhId));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }
    }
}