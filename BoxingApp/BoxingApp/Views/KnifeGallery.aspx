﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KnifeGallery.aspx.cs" Inherits="BoxingApp.Views.KnifeGallery" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

        <!-- JQuery -->
     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/css/mdb.min.css" rel="stylesheet"/>

     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../knives/knivesStyle.css" rel="stylesheet" />   
    <script src="../knives/knivesGallery.js"></script>
    <script src="../knives/bpenDrawer-master/js/bpen.drawer.js"></script>  
     <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0"/>


    <link href="../knives/Gallery/photoswipe.css" rel="stylesheet" />
    <link href="../knives/Gallery/default-skin/default-skin.css" rel="stylesheet" />
    <script src="../knives/Gallery/photoswipe.min.js"></script>
    <script src="../knives/Gallery/photoswipe-ui-default.min.js"></script>
 
    

</head>
<body>
    <form id="form1" runat="server">

          <%-- <div class="row">--%>
        <%-- header --%>
        <div class="center-block">
              <img class="center-block" src="../knives/images/Swift%20logo%20White.png" style="width: 30%;padding: 1%;" />
                </div>

         <div id="mainMenu"></div>


        <div class="container">
            <div>
        <div class="gallery">
            <div class="center-block" style="display: table; margin: 0 auto;">
        <p class="" style="color: white;
    font-size: xx-large;"
  >Gallery</p>
                </div>
  <div class="my-gallery col-md-2" itemscope itemtype="http://schema.org/ImageGallery">

    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
      <a href="../knives/images/51480008_299997877369694_504184465871229694_n.jpg" itemprop="contentUrl" data-size="1024x1024">
          <img src="../knives/images/51480008_299997877369694_504184465871229694_n.jpg" itemprop="thumbnail" alt="Image description" />
      </a>
     <figcaption itemprop="caption description">Image caption  1</figcaption>
                                          
    </figure>

    <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
      <a href="../knives/images/52626395_126428941802021_5072567642770252786_n.jpg" itemprop="contentUrl" data-size="964x1024">
          <img src="../knives/images/52626395_126428941802021_5072567642770252786_n.jpg" itemprop="thumbnail" alt="Image description" />
      </a>
      <figcaption itemprop="caption description">Image caption 2</figcaption>
    </figure>

         <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
      <a href="../knives/images/50488033_116018269502878_4987212869283964533_n.jpg" itemprop="contentUrl" data-size="964x1024">
          <img src="../knives/images/50488033_116018269502878_4987212869283964533_n.jpg" itemprop="thumbnail" alt="Image description" />
      </a>
      <figcaption itemprop="caption description">Image caption 3</figcaption>
    </figure>

         <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
      <a href="../knives/images/53503996_1077243969331451_3951615209952360333_n.jpg" itemprop="contentUrl" data-size="964x1024">
          <img src="../knives/images/53503996_1077243969331451_3951615209952360333_n.jpg" itemprop="thumbnail" alt="Image description" />
      </a>
      <figcaption itemprop="caption description">Image caption 4</figcaption>
    </figure>

         <figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
      <a href="../knives/images/53503996_1077243969331451_3951615209952360333_n.jpg" itemprop="contentUrl" data-size="964x1024">
          <img src="../knives/images/53503996_1077243969331451_3951615209952360333_n.jpg" itemprop="thumbnail" alt="Image description" />
      </a>
      <figcaption itemprop="caption description">Image caption 5</figcaption>
    </figure>

  </div>
</div>
                </div>
            </div>


        <!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button type="button" class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button type="button" class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

          </div>

        </div>

</div>

                        <!-- Footer -->
<footer class="page-footer font-large stylish-color">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Grid row-->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center">

          <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>          
          <!--Instagram-->
          <a href="https://www.instagram.com/swiftknivesuk/" class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"></i>
          </a>           
        </div>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
  <!-- Footer Elements -->

 <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://www.swiftknivesuk.com"> swiftknivesuk.com</a>
  </div>

</footer>
<!-- Footer -->

    </form>

    <!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/js/mdb.min.js"></script>

</body>
</html>
