﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class Leagues : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ArrayList getLeaguesForUser(Int64 user)
        {
            ArrayList leagueDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Leagues.getAllLeagueForUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        leaguesForUser newobj = new leaguesForUser();

                        newobj.id = reader.GetInt64(0);
                        newobj.parentId = reader.GetInt64(1);
                        newobj.Name = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        newobj.Code = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);                  

                        leagueDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return leagueDetails;
        }

        [WebMethod]
        public static int createLeague(Int64 user, string name)
        {
           var pay = false;

            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.CreateLeague]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userid", user));
                    stmt.Parameters.Add(new SqlParameter("@leagueName", name));
                    stmt.Parameters.Add(new SqlParameter("@payLeague", pay));                  
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static int DeleteLeague(string lCode, int user)
        {     
            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Delete.LeagueTable]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Lcode", lCode));
                    stmt.Parameters.Add(new SqlParameter("@userId", user));                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static int checkLeague(string lCode, int user)
        {
            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                  
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[checkLeagueTable]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Lcode", lCode));
                    stmt.Parameters.Add(new SqlParameter("@userId", user));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                string errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static int joinLeague(Int64 user, string code)
        {      

            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.AddUsertoLeague]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@leagueCode", code));
                    stmt.Parameters.Add(new SqlParameter("@userId", user));                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static ArrayList getLeagueforUser(Int64 leagueId)
        {
            ArrayList leaderboard = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Leagues.getLeagueTable]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@parentLeagueId", leagueId));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        leaderboard newobj = new leaderboard();

                        newobj.position = reader.GetInt64(0);
                        newobj.name = reader.GetString(2);
                        newobj.correctResult = reader.GetInt64(4);
                        newobj.correctRounds = reader.GetInt64(5);
                        newobj.correctBoxers = reader.GetInt64(6);
                        newobj.score = reader.GetInt64(7);


                        leaderboard.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return leaderboard;
        }
    }


    public class leaguesForUser
    {
        public Int64 id { set; get; }
        public Int64 parentId { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }

    }

    public class leagueforUser
    {
        public Int64 position { set; get; }
        public string name { set; get; }
        public Int64 correctResult { set; get; }
        public Int64 correctRounds { set; get; }
        public Int64 correctBoxers { set; get; }
        public Int64 score { set; get; }

     
        // public Int64 userId { set; get; }
    }
}