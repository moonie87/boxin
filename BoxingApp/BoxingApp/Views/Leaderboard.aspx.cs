﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class Leaderboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ArrayList getLeaderboard()
        {
            ArrayList leaderboard = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                { 
                    SqlCommand stmt = new SqlCommand();
                    
                    stmt.CommandText = "[Leaderboard.getLeaderboard]";
                    stmt.CommandType = CommandType.StoredProcedure;                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    SqlDataReader reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        leaderboard newobj = new leaderboard();

                        newobj.position = reader.GetInt64(0);
                        newobj.name = reader.GetString(3);
                        newobj.correctResult = reader.GetInt64(4);
                        newobj.correctRounds = reader.GetInt64(5);
                        newobj.correctBoxers = reader.GetInt64(6);
                        newobj.score = reader.GetInt64(7);
                      

                        leaderboard.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return leaderboard;
        }

        [WebMethod]
        public static ArrayList getSeasonPassLeaderboard()
        {
            ArrayList leaderboard = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();

                    stmt.CommandText = "[Leaderboard.getSeasonPassLeaderboard]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    SqlDataReader reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        leaderboard newobj = new leaderboard();

                        newobj.position = reader.GetInt64(0);
                        newobj.name = reader.GetString(3);
                        newobj.correctResult = reader.GetInt64(4);
                        newobj.correctRounds = reader.GetInt64(5);
                        newobj.correctBoxers = reader.GetInt64(6);
                        newobj.score = reader.GetInt64(7);
                        newobj.email = reader.GetString(8);


                        leaderboard.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return leaderboard;
        }

        [WebMethod]
        public static ArrayList getActiveSeasonAndWeek()
        {
            ArrayList SandW = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[GetActiveSeasonandWeek]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    SqlDataReader reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        activeSeasonAndWeek newobj = new activeSeasonAndWeek();

                        newobj.Season = reader.GetInt64(0);
                        newobj.week = reader.GetInt32(1);


                        SandW.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return SandW;
        }
    }


  

public class leaderboard
    {
        public Int64 position { set; get; }
        public string name { set; get; }
        public Int64 correctResult { set; get; }
        public Int64 correctRounds { set; get; }
        public Int64 correctBoxers { set; get; }
        public Int64 score { set; get; }

        public string email { set; get; }
       // public Int64 userId { set; get; }
    }

    public class activeSeasonAndWeek
    {
        public Int64 Season { set; get; }
        public int week { set; get; }
    }
}