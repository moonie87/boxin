﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string v = Request.QueryString["LC"];
            if (v != "b5105")
            {
                Response.Redirect("Selection.aspx");
            }          
        }

        [WebMethod]
        public static void updateSeasonAndWeek(int season,int week)
        {
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[UpdateSeasonAndWeek]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Season", season));
                    stmt.Parameters.Add(new SqlParameter("@week", week));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
        }

        [WebMethod]
        public static ArrayList getActiveSeasonAndWeek()
        {
            ArrayList Selections = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[GetActiveSeasonandWeek]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        activeSandW newobj = new activeSandW();

                        newobj.season = reader.IsDBNull(0) ? 0 : reader.GetInt64(0);
                        newobj.week = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);

                        Selections.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return Selections;
        }

        [WebMethod]
        public static void updateScores()
        {
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[updateCorrectResultsAndLeaderboard]";
                    stmt.CommandType = CommandType.StoredProcedure;              
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();                  
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
        }

        [WebMethod]
        public static string checkLastUpdated()
        {
            var date = "";
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Scores.getLastUpdatedDate]";
                    stmt.CommandType = CommandType.StoredProcedure;                    
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        date = reader.GetDateTime(0).ToString();
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }            
            return date;
        }

        [WebMethod]
        public static int checkboxer(string boxer,int div)
        {
            var returned = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Boxers.checkBoxer]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@boxersName", boxer));
                    stmt.Parameters.Add(new SqlParameter("@boxersDiv", div));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        returned = reader.GetInt32(0);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return returned;
        }

        [WebMethod]
        public static int insertBoxer(string name, string image, string nationality, string nickname, string stance, int? height, int? reach, string residence,
            int? age, int? division,int? won,int? lost, int? drawn, int? ko, int? ibf,int? wbc, int? ibo, int? wba, int? wbo, string last1, string last2, string last3, string last4, string last5, int? rounds, int? bouts)
        {
            var passed = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[InsertBoxer]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@name", name));
                    stmt.Parameters.Add(new SqlParameter("@image", image ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@nationality", nationality ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@nickname", nickname ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@stance", stance ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@height", height ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@reach", reach ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@residence", residence ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@age", age ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@division", division ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@won", won ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@lost", lost ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@drawn", drawn ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Ko", ko ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@ibf", ibf ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wbc", wbc ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@ibo", ibo ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wba", wba ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wbo", wbo ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last1", last1 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last2", last2 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last3", last3 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last4", last4 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last5", last5 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@rounds", rounds ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@bouts", bouts ?? (Object)DBNull.Value));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    passed = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return passed;
        }

        [WebMethod]
        public static int insertBout(int? season, int? week, int? bout, int? boxer1, int? boxer2, int? rounds, string date, string location, string division, string televised, string title)
        {
            DateTime dt = DateTime.ParseExact(date, "dd.m.yyyy", null);

            System.Data.SqlTypes.SqlDateTime? dtsql = System.Data.SqlTypes.SqlDateTime.Parse(dt.ToString("yyyy/MM/dd"));

            var passed = 0;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Bout_InsertBout]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Season", season));
                    stmt.Parameters.Add(new SqlParameter("@Week", week ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@bout", bout ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Boxer1", boxer1 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Boxer2", boxer2 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Rounds", rounds ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Date", dtsql ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Location", location ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Division", division ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@televised", televised ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@title", title ?? (Object)DBNull.Value));
                 
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    passed = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return passed;
        }

        [WebMethod]
        public static int insertResultsBout(int? season, int? week, int? bout, int? Outcome, int? Round, int? BoxerWon, int? BoxerLost, int? TieBreaker)
        {            
            var passed = 0;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Bout_InsertResultsForBout]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Season", season));
                    stmt.Parameters.Add(new SqlParameter("@Week", week ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@bout", bout ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Outcome", Outcome ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Round", Round ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@BoxerLost", BoxerLost ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@BoxerWon", BoxerWon ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@TieBreaker", TieBreaker ?? (Object)DBNull.Value));                   

                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    passed = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return passed;
        }

        [WebMethod]
        public static int updateBoxer(string image, int? Div, string name, int? age, int? won, int? lost, int? drawn, int? ko, int? ibf, int? wbc, int? ibo, int? wba, int? wbo, string last1, string last2, string last3, string last4, string last5, int? rounds, int? bouts)
        {
            var passed = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[UpdateBoxer]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@image", image));
                    stmt.Parameters.Add(new SqlParameter("@Div", Div));
                    stmt.Parameters.Add(new SqlParameter("@name", name));                  
                    stmt.Parameters.Add(new SqlParameter("@age", age ?? (Object)DBNull.Value));                  
                    stmt.Parameters.Add(new SqlParameter("@won", won ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@lost", lost ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@drawn", drawn ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@Ko", ko ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@ibf", ibf ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wbc", wbc ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@ibo", ibo ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wba", wba ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@wbo", wbo ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last1", last1 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last2", last2 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last3", last3 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last4", last4 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@last5", last5 ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@rounds", rounds ?? (Object)DBNull.Value));
                    stmt.Parameters.Add(new SqlParameter("@bouts", bouts ?? (Object)DBNull.Value));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    passed = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return passed;
        }
        public class activeSandW
        {
            public Int64 season { set; get; }
            public int week { set; get; }
        }
    }
}