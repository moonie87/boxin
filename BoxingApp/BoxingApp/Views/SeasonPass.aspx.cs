﻿using System;
using System.Web.Services;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Configuration;

namespace BoxingApp.Views
{
    public partial class SeasonPass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static int AddReceipt(int userId, string Reciept, string parentPayId, string state, string email, string fname, string lname, string payerId)
        {
           
             var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[receipt_add]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", userId));
                    stmt.Parameters.Add(new SqlParameter("@recieptId", Reciept));
                    stmt.Parameters.Add(new SqlParameter("@parentPayId", parentPayId));
                    stmt.Parameters.Add(new SqlParameter("@state", state));
                    stmt.Parameters.Add(new SqlParameter("@email", email));
                    stmt.Parameters.Add(new SqlParameter("@fname", fname));
                    stmt.Parameters.Add(new SqlParameter("@lname", lname));
                    stmt.Parameters.Add(new SqlParameter("@payerId", payerId));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }
    }
}