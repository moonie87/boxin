﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class rankings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ArrayList getRankings(string federation, int div)
        {
            ArrayList resultsDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Get_Rankings]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@federation", federation));
                    stmt.Parameters.Add(new SqlParameter("@div", div));

                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        rankingsData newobj = new rankingsData();


                        newobj.PosId = reader.GetInt32(0);
                        newobj.name = reader.GetString(1);
                        newobj.won = reader.GetInt32(2);
                        newobj.lost = reader.GetInt32(3);
                        newobj.drawn = reader.GetInt32(4);
                        newobj.id = reader.GetInt32(5);
                        newobj.Boxing5Id = reader.GetInt32(6);
                       


                        resultsDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return resultsDetails;
        }





        public class rankingsData
        {
            public int PosId { set; get; }   

            public string name { set; get; }
            public int won { set; get; }

            public int lost { set; get; }
            public int drawn { set; get; }
            public int id { set; get; }
            public int Boxing5Id { set; get; }


        }
    }
}
