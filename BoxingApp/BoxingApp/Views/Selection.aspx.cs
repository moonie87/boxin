﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ArrayList GetBoutDetails(string leftorRight)
        {
            ArrayList BoxerDetails = new ArrayList();    
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Boxers.getBoxersForWeek]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@leftorRight", leftorRight));  
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        boxerDetails newobj = new boxerDetails();

                        newobj.Id = reader.GetInt32(0);
                        newobj.Name = reader.GetString(1);
                        newobj.image = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        newobj.nationality = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        newobj.stance = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        newobj.height = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        newobj.reach = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        newobj.residence = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                        newobj.age = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);
                        newobj.won = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        newobj.lost = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        newobj.drawn = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        newobj.ko = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        newobj.division = reader.IsDBNull(15) ? string.Empty : reader.GetString(15);
                        newobj.Date = reader.IsDBNull(14) ? (DateTime?)null : (DateTime?)reader.GetDateTime(14);
                        newobj.location = reader.IsDBNull(16) ? string.Empty : reader.GetString(16);
                        newobj.last1 = reader.IsDBNull(17) ? string.Empty : reader.GetString(17);
                        newobj.last2 = reader.IsDBNull(18) ? string.Empty : reader.GetString(18);
                        newobj.last3 = reader.IsDBNull(19) ? string.Empty : reader.GetString(19);
                        newobj.last4 = reader.IsDBNull(20) ? string.Empty : reader.GetString(20);
                        newobj.last5 = reader.IsDBNull(21) ? string.Empty : reader.GetString(21);
                        newobj.televised = reader.IsDBNull(22) ? string.Empty : reader.GetString(22);
                        newobj.title = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);


                        BoxerDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";                
            }
            return BoxerDetails;
        }

        [WebMethod]
        public static ArrayList GetBoxerDetailsByName(string Name, int div)
        {
            ArrayList BoxerDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Boxers.getDetailsByName]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Name", Name));
                    stmt.Parameters.Add(new SqlParameter("@div", div));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        boxerDetails newobj = new boxerDetails();

                        newobj.Id = reader.GetInt32(0);
                        newobj.Name = reader.GetString(1);
                        newobj.image = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        newobj.nationality = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        newobj.nickname = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);                       
                        newobj.stance = reader.IsDBNull(5) ? string.Empty : reader.GetString(5);
                        newobj.height = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        newobj.reach = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        newobj.residence = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);
                        newobj.age = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                        newobj.DivId = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                        newobj.won = reader.IsDBNull(11) ? 0 : reader.GetInt32(11);
                        newobj.lost = reader.IsDBNull(12) ? 0 : reader.GetInt32(12);
                        newobj.drawn = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                        newobj.ko = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                        newobj.IBF = reader.IsDBNull(15) ? 0 : reader.GetInt32(15);
                        newobj.WBC = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                        newobj.IBO = reader.IsDBNull(17) ? 0 : reader.GetInt32(17);
                        newobj.WBA = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                        newobj.WBO = reader.IsDBNull(19) ? 0 : reader.GetInt32(19);                                  
                        newobj.last1 = reader.IsDBNull(20) ? string.Empty : reader.GetString(20);
                        newobj.last2 = reader.IsDBNull(21) ? string.Empty : reader.GetString(21);
                        newobj.last3 = reader.IsDBNull(22) ? string.Empty : reader.GetString(22);
                        newobj.last4 = reader.IsDBNull(23) ? string.Empty : reader.GetString(23);
                        newobj.last5 = reader.IsDBNull(24) ? string.Empty : reader.GetString(24);
                        newobj.Rounds = reader.IsDBNull(25) ? 0 : reader.GetInt32(25);
                        newobj.Bouts = reader.IsDBNull(26) ? 0 : reader.GetInt32(26);
                       


                        BoxerDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return BoxerDetails;
        }


        [WebMethod]
        public static ArrayList getBoxers(int bout)
        {
            ArrayList BoxerDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Boxers.getBoxersforBout]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@boutid", bout));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        boxers newobj = new boxers();

                        newobj.boxer1Id = reader.GetInt64(3);
                        newobj.boxerName = reader.IsDBNull(4) ? string.Empty : reader.GetString(4);
                        newobj.boxer2Id = reader.GetInt64(5);
                        newobj.boxer2Name = reader.IsDBNull(6) ? string.Empty : reader.GetString(6);
                       

                        BoxerDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return BoxerDetails;
        }

        [WebMethod]
        // Get boxers for bout by season , week and bout
        public static ArrayList getBoxersForSWB(int season, int week, int bout)
        {
            ArrayList BoxerDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Boxers.getNamesForBout]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@season", season));
                    stmt.Parameters.Add(new SqlParameter("@week", week));
                    stmt.Parameters.Add(new SqlParameter("@bout", bout));
                    
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        boxers newobj = new boxers();

                        newobj.boxer1Id = reader.GetInt64(0);
                        newobj.boxerName = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                        newobj.boxer2Id = reader.GetInt64(2);
                        newobj.boxer2Name = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);

                        BoxerDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return BoxerDetails;
        }

        [WebMethod]
        public static ArrayList getRounds(int bout)
        {
            ArrayList rounds = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[GetRounds]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@bout", bout));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        Rounds newobj = new Rounds();

                        newobj.id = reader.GetInt32(0);
                        newobj.round = reader.IsDBNull(1) ? string.Empty : reader.GetString(1);
                       


                        rounds.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return rounds;
        }
        [WebMethod]
        public static ArrayList getuserSelctions(Int64 user)
        {
            ArrayList Selections = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Selection.getSelectionsforUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@user", user));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        userSelections newobj = new userSelections();

                        newobj.boutId = reader.IsDBNull(4) ? 0 : reader.GetInt64(4);
                        newobj.round = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                        newobj.outcome = reader.IsDBNull(6) ? 0 : reader.GetInt32(6);
                        newobj.boxer = reader.IsDBNull(7) ? 0 : reader.GetInt32(7);
                        newobj.tieBreaker = reader.IsDBNull(8) ? 0 : reader.GetInt32(8);



                        Selections.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return Selections;
        }

        [WebMethod]
        public static ArrayList getActiveSeasonAndWeek()
        {
            ArrayList Selections = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[GetActiveSeasonandWeek]";
                    stmt.CommandType = CommandType.StoredProcedure;                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        activeSandW newobj = new activeSandW();

                        newobj.season = reader.IsDBNull(0) ? 0 : reader.GetInt64(0);
                        newobj.week = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);  

                        Selections.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return Selections;
        }

        [WebMethod]
        public static int saveSelection(Int64 user,Int64 bout,int round, int outcome, Int64 boxer,int tieBreaker)
        {
            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();                  
                    stmt.CommandText = "[Selection.SaveSelection]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@user", user));
                    stmt.Parameters.Add(new SqlParameter("@boutId", bout));
                    stmt.Parameters.Add(new SqlParameter("@round", round));
                    stmt.Parameters.Add(new SqlParameter("@outcome", outcome));
                    stmt.Parameters.Add(new SqlParameter("@boxer", boxer));
                    stmt.Parameters.Add(new SqlParameter("@TieBreaker", tieBreaker));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }
    }


    public class boxerDetails
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string image { set; get; }
        public string nationality { set; get; }
        public string nickname { set; get; }
        public string stance { set; get; }
        public int height { set; get; }
        public int reach { set; get; }
        public string residence { set; get; }
        public int age { set; get; }
        public int won { set; get; }
        public int lost { set; get; }
        public int drawn { set; get; }
        public int ko { set; get; }
        public string division { set; get; }
        public DateTime? Date { set; get; }

        public int DivId { set; get; }

        public string location { set; get; }
        public string last1 { set; get; }
        public string last2 { set; get; }
        public string last3 { set; get; }
        public string last4 { set; get; }
        public string last5 { set; get; }

        public string televised { set; get; }
        public string title { set; get; }

        public int IBF { set; get; }
        public int WBC { set; get; }
        public int IBO { set; get; }
        public int WBA { set; get; }
        public int WBO { set; get; }

        public int Rounds { set; get; }
        public int Bouts { set; get; }


    } 

    public class activeSandW
    {
        public Int64 season { set; get; }
        public int week { set; get; }
    }

    public class boxers
    {
        public Int64 boxer1Id { set; get; }
        public string boxerName { set; get; }
        public Int64 boxer2Id { set; get; }
        public string boxer2Name { set; get; }
    }

    public class userSelections
    {
        public Int64 boutId { set; get; }
        public int round { set; get; }
        public int outcome { set; get; }
        public Int64 boxer { set; get; }
        public Int32 tieBreaker { set; get; }
    }

    public class Rounds
    {
        public int id { set; get; }
        public string round { set; get; }
    }
    public class BoxingAppConnection
    {
        private static string connection = string.Empty;
        public static string Connection
        {
            get
            {
                if (connection.Length == 0)
                {
                  // connection = ConfigurationManager.ConnectionStrings["BoxingAppConnection"].ConnectionString;

                   connection = "server=db728600826.db.1and1.com;Initial Catalog=db728600826; uid=dbo728600826; pwd=Moonie1987!;";
                }
                return connection;
            }
        }
    }
}