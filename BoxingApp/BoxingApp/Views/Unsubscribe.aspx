﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Unsubscribe.aspx.cs" Inherits="BoxingApp.Views.Unsubscribe" %>


<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">  
    <script src="/Scripts/Login.js"></script>
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">
     <div class="headerTitleSelection">
        <h3 class="centreText">Unsubscribe</h3>
    </div>  

    <div class="leagueBorder">
     <div style="text-align:center;">                  
                    <p>You can always re subscribe to emails / notifications in ACCOUNT > UPDATE ACCOUNT </p>                   
                </div>       
        <div >
            <div class="centreText">
                <label class="centreText">By Clicking submit you are unsubscribing to all emails</label>
                <label id="EmailTounsubscribe" />
                <input id="emailToUnsubscribeBtn" value="Submit" class="topMenu centreText margintop5px" style="display:block;"/>
            </div>
          
        </div>
    </div>

    </asp:Content>