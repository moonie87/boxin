﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Admin.aspx.cs" Inherits="BoxingApp.Views.Admin" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <script src="/Scripts/Admin.js"></script>  

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

    <style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {1
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>


    <div class="tab">
  <button type="button" class="tablinks" onclick="openTab(event, 'AddBoxer')">Add Boxer</button>
  <button type="button" class="tablinks" onclick="openTab(event, 'AddBout')">Add Bout</button>
  <button type="button" class="tablinks" onclick="openTab(event, 'AddResult')">Add Result</button>
         <button type="button" class="tablinks" onclick="openTab(event, 'CalculateScores')">Update Scores</button>
         <button type="button" class="tablinks" onclick="openTab(event, 'updateSeasonAndWeek')">Update Season / Week</button>        
        <label id="HeaderSeasonandWeek" class="tablinks"></label>
    </div>
  
        <div id="AddBoxer" class="tabcontent centreText" style="color:white;">
            <h1>Add Boxer</h1>
            <label class="centreText">name U</label><input style="color:black;" class="centreText" type="text" id="addName" /> 
            <label class="centreText">Div U</label><input style="color:black;" class="centreText" type="text" id="addDiv" /> <button id="checkName" type="button" class="k-button centreText">Check Boxer</button> <br />
            <label class="centreText" style="color:red"; id="checknameLabel"></label><br />
             <label class="centreText">image</label><input style="color:black;" class="centreText" type="text" id="addImage" /> <br />
             <label class="centreText">nationality</label><input style="color:black;" class="centreText" type="text" id="addNationality" /><br />
             <label class="centreText">nickname</label><input style="color:black;" class="centreText" type="text" id="addNickname" /><br />
             <label class="centreText">stance</label><input style="color:black;" class="centreText" type="text" id="addStance" /><br />
             <label class="centreText">height</label><input style="color:black;" class="centreText" type="text" id="addheight" /><br />
             <label class="centreText">reach</label><input style="color:black;" class="centreText" type="text" id="addReach" /><br />
             <label class="centreText">residence</label><input style="color:black;" class="centreText" type="text" id="addresidence" /><br />
             <label class="centreText">age U</label><input style="color:black;" class="centreText" type="text" id="addage" /><br />
             <label class="centreText">division</label><input style="color:black;" class="centreText" type="text" id="addDivision" /><br />
            <p>1- minimum, 2- light fly, 3- fly, 4-super fly,5-bantam,6-super bantam,7-light,8-super light, 9-middle,10-super middle,11-light heavy,12-cruiser</p><br />
            <p>13-heavy,16-welter,17-super welter, 18 - Super Feather, 19 - Feather</p><br />
             <label class="centreText">won U</label><input style="color:black;" class="centreText" type="text" id="addWon" /><br />
             <label class="centreText">lost U</label><input style="color:black;" class="centreText" type="text" id="addLost" /><br />
             <label class="centreText">drawn U</label><input style="color:black;" class="centreText" type="text" id="addDrawn" /><br />
             <label class="centreText">Ko U</label><input style="color:black;" class="centreText" type="text" id="addKo" /><br />

             <label class="centreText">rounds U </label><input style="color:black;" class="centreText" type="text" id="addRounds" /><br />
             <label class="centreText">bouts U</label><input style="color:black;" class="centreText" type="text" id="addBouts" /><br />

             <label class="centreText">ibf U</label><input style="color:black;" class="centreText" type="text" id="addIBF" /><br />
             <label class="centreText">WBC U</label><input style="color:black;" class="centreText" type="text" id="addWBC" /><br />
             <label class="centreText">ibo U</label><input style="color:black;" class="centreText" type="text" id="addIBO" /><br />
             <label class="centreText">wba U</label><input style="color:black;" class="centreText" type="text" id="addWBA" /><br />
             <label class="centreText">Wbo U</label><input style="color:black;" class="centreText" type="text" id="addWBO" /><br />
             <label class="centreText">last1 U</label><input style="color:black;" class="centreText" type="text" id="addlast1" /><br />
             <label class="centreText">last2 U</label><input style="color:black;" class="centreText" type="text" id="addLast2" /><br />
             <label class="centreText">last3 U</label><input style="color:black;" class="centreText" type="text" id="addLast3" /><br />
             <label class="centreText">last4 U</label><input style="color:black;" class="centreText" type="text" id="addLast4" /><br />
             <label class="centreText">last5 U</label><input style="color:black;" class="centreText" type="text" id="addLast5" /><br />
            <button id="insertBoxer" type="button" class="k-button centreText">Submit Boxer</button><br /> <button id="UpdateBoxer" type="button" class="k-button centreText">Update Boxer</button>
              <label class="centreText" style="color:red"; id="insertBoxerLabel"></label><br />

        </div>
        <div id="AddBout"  class="tabcontent centreText" style="color:white;">
            <h1>Add Bout</h1>
            <label class="centreText">Season </label><input style="color:black;" class="centreText" type="text" id="addSeason" />           
             <label class="centreText">Week</label><input style="color:black;" class="centreText" type="text" id="addWeek" /> <br />
             <label class="centreText">Bout</label><input style="color:black;" class="centreText" type="text" id="addBoutNumber" /><br />
             <label class="centreText">Boxer1Id</label><input style="color:black;" class="centreText" type="text" id="addBoxer1" /><br />
             <label class="centreText">Boxer2Id</label><input style="color:black;" class="centreText" type="text" id="addBoxer2" /><br />
             <label class="centreText">rounds</label><input style="color:black;" class="centreText" type="text" id="Rounds" /><br />
            <label>Date format - 05.05.2018</label>
             <label class="centreText">date</label><input style="color:black;" class="centreText" type="text" id="addDate" /><br />
             <label class="centreText">Location</label><input style="color:black;" class="centreText" type="text" id="addLocation" /><br />
             <label class="centreText">Division Text</label><input style="color:black;" class="centreText" type="text" id="addBoutDiv" /><br />           
             <label class="centreText">Televised</label><input style="color:black;" class="centreText" type="text" id="addTelevised" /><br />            
             <label class="centreText">Title</label><input style="color:black;" class="centreText" type="text" id="addTitle" /><br />
             <button id="insertBout" type="button" class="k-button centreText">Insert Bout</button><br />
            </div>

         <div id="AddResult"  class="tabcontent centreText" style="color:white;">
            <h1>Add Results</h1>
            <label class="centreText">Season </label><input style="color:black;" class="centreText" type="text" id="addResultSeason" />           
             <label class="centreText">Week</label><input style="color:black;" class="centreText" type="text" id="addResultWeek" /> <br />
             <label class="centreText">Bout</label><input style="color:black;" class="centreText" type="text" id="addResultBoutNumber" /><br />
            <button id="GetBoxersBtn" type="button" class="k-button centreText">Get Boxers</button><br /> 
              <label class="centreText" style="color:red"; id="getBoxerLabel"></label><br />
            <p class="centreText">1 = points, 2 = Ko / TKO, 3 = draw, 4 = DQ </p><br />
             <label class="centreText">OutcomeId</label><input style="color:black;" class="centreText" type="text" id="addOutcome" /><br />
             <label class="centreText">Round</label><input style="color:black;" class="centreText" type="text" id="addWinnerRound" /><br />
             <label class="centreText">BoxerWonId</label><input style="color:black;" class="centreText" type="text" id="addBoxerWon" /><br />
             <label class="centreText">BoxerLostId</label><input style="color:black;" class="centreText" type="text" id="addBoxerLost" /><br />   
            <label class="centreText">WinningSecond</label><input style="color:black;" class="centreText" type="text" id="addTieBreaker" /><br /> 
            
            <button id="updateBout" type="button" class="k-button centreText">Submit Boxer</button><br />
              <label class="centreText" style="color:red"; id="insertBoutLabel"></label><br />
</div>

    <div id="CalculateScores" class="tabcontent centreText" style="color:white">

         <button id="lastUpdatedScores" type="button" class="k-button centreText">Last Updated Scores</button>
        <label id="lastUpdatedScoresLabel" class="centreText"></label><br />
        <label class="centreText">CHECK THE DATE - Have you updated all the results?????</label><br /><br />
        <button id="UpdateScores" type="button" class="k-button centreText">UpdateScores</button>
         <label id="scoreUpdatedLabel" class="centreText"></label><br />
    </div>

     <div id="updateSeasonAndWeek" class="tabcontent centreText" style="color:white">
        <label id="CurrentSeasonAndWeekLabel" class="centreText"></label><br />
        <br /><br />
         <input type="number" id="adminSeason"class="centreText" placeholder="Season" style="color:black" />
         <input type="number" id="adminWeek" class="centreText" placeholder="Week" style="color:black" /> <br />        
        <button id="SetSeasonandWeek" type="button" class="k-button centreText">Update Season and Week</button>
         <label id="seasonAndWeekUpdatedLabel" class="centreText"></label><br />
          <button id="SendSeasonPassEmails" type="button" class="k-button centreText">Send Emails</button>
    </div>

    <script>
function openTab(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>

</asp:Content>
