﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Services;

namespace BoxingApp.Views
{
    public partial class Results : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static ArrayList getWeeksforSeason(int season)
        {
            ArrayList weeksDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                   
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Get_Weeks]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@season", season));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        weeks newobj = new weeks();

                        newobj.week = reader.GetInt32(0);
                       

                        weeksDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return weeksDetails;
        }


        [WebMethod]
        public static ArrayList getResultsforweek(int week)
        {
            ArrayList resultsDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Get_Results]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@week", week));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        results newobj = new results();


                        newobj.round = reader.GetInt32(0);
                        newobj.outcome = reader.GetInt32(1);
                        newobj.boxerWon = reader.GetInt32(3);
                        newobj.boxerName = reader.GetString(5);
                        newobj.outcomeText = reader.GetString(2);
                        newobj.link = reader.GetString(6);
                        newobj.tieBreaker = reader.GetInt32(7);
                       

                        resultsDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return resultsDetails;
        }

        [WebMethod]
        public static ArrayList getpredictedforweek(int user,int week)
        {
            ArrayList resultsDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Get_predictionsResults]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@user", user));
                    stmt.Parameters.Add(new SqlParameter("@week", week));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        results newobj = new results();


                        newobj.round = reader.GetInt32(0);
                        newobj.outcome = reader.GetInt32(1);
                        newobj.boxerWon = reader.GetInt32(3);
                        newobj.boxerName = reader.GetString(4);
                        newobj.outcomeText = reader.GetString(2);
                       
                        newobj.link = reader.GetString(5);
                        newobj.tieBreaker = reader.GetInt32(6);


                        resultsDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return resultsDetails;
        }

        [WebMethod]
        public static ArrayList getUserDetails(int user)
        {
            ArrayList resultsDetails = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Users_GetDetails]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));                    
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        User newobj = new User();

                           
                        newobj.FirstName = reader.GetString(1);
                        newobj.LastName = reader.GetString(2);
                        newobj.UserName = reader.GetString(8);
                        newobj.Email = reader.GetString(3);
                        newobj.season = reader.GetBoolean(5);
                        newobj.emailSub = reader.GetBoolean(13);


                        resultsDetails.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return resultsDetails;
        }

        [WebMethod]
        public static int UpdateUserDetails(int user, string f_name, string l_name, string userName, string email, bool emailSub)
        {
            var id = 0;
           
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();                    
                    stmt.CommandText = "[Users_updateUserDetails]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));
                    stmt.Parameters.Add(new SqlParameter("@firstName", f_name));
                    stmt.Parameters.Add(new SqlParameter("@lastName", l_name));
                    stmt.Parameters.Add(new SqlParameter("@userName", userName));
                    stmt.Parameters.Add(new SqlParameter("@email", email));
                    stmt.Parameters.Add(new SqlParameter("@emailSub", emailSub));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static int UpdateUserpassword(int user, string password)
        {
            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Users_UpdatePassword]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));
                    stmt.Parameters.Add(new SqlParameter("@password", password));                   
                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

    }

    public class weeks
    {
        public int week { set; get; }
    }

    public class results
    {
        public int round { set; get; }
        public int outcome { set; get; }

        public string outcomeText { set; get; }
        public int boxerWon { set; get; }

        public string boxerLost { set; get; }
        public string boxerName { set; get; }

        public string link { set; get; }
        public int tieBreaker { set; get; }
    }

    public class User
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public Boolean season { set; get; }
        public Boolean emailSub { set; get; }
        
    }
}