﻿<%@ Page title="" Language="C#" MasterPageFile="~/application.Master" AutoEventWireup="true" CodeBehind="Leaderboard.aspx.cs" Inherits="BoxingApp.Views.Leaderboard" %>


<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <%--  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
     <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />--%>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"/>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css" rel="stylesheet" crossorigin="anonymous"/>
     <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>

     <script src="/Scripts/Leaderboard.js"></script>
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">
   <div class="leaderboardHeader">
      
           <h3 class="centreText">Leaderboard</h3>
       
       <div class="centreText">
       <label id="season"></label>&nbsp
       <label id="week"></label>
           </div>
       
   </div>
    <div class="LeaderboardBorder">
        <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Position</th>
                <th>Name</th>
                <th>Correct Results</th>
                <th>Correct Rounds</th>
                <th>Correct Boxers</th>
                <th>Score</th>
            </tr>
        </thead>
            </table>           
    </div>
    </asp:Content>
