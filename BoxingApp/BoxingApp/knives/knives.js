﻿var drawer;

window.addEventListener("load", function () {
    drawer = bpen.drawer("mainMenu", {
        Items: [{
            Text: "Home",
            Url: "swiftKnivesuk.aspx"
        }, 
        {
            Text: "Gallery",
            Url: "/Views/KnifeGallery.aspx"
        },
        {
            Text: "Contact",
            Url: "/Views/contactKnives.aspx"
        },
        ],
        Anchor: 'left'
    });
}, false);

$(document).ready(function () {

    try {

       
        var image1 = document.getElementsByClassName('thumbnail1');
        var image2 = document.getElementsByClassName('thumbnail2');
        var image3 = document.getElementsByClassName('thumbnail3');
        
        new simpleParallax(image1);
        new simpleParallax(image2);
        new simpleParallax(image3);
    }
    catch (notneeded) {}
});

function emailNotification(from, fromAlias, to, subject, mailBody,email) {
    $.ajax({
        type: "post",
        data: JSON.stringify({
            from: from, fromAlias: fromAlias, to: to, subject: subject, mailBody: mailBody, email:email
        }),
        url: "/Views/contactKnives.aspx/mailNotification",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });

}