﻿$(document).ready(function () {

    $("#JoinLeagueBtn").kendoButton({
        click: function() {
            joinLeague();
        }
    });
});

function joinLeague() {

    var leagueName = $("#JoinLeagueInput").val();
    var user = parserId();
    var returnData;

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user,code: leagueName
        }),
        url: "/Views/Leagues.aspx/joinLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            returnData = object.d;

           

        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    // added to league
    if (returnData == 1)
    {
        toastr.success('League', 'Joined League successfully');

        


       // popupMessage("large", "Joined League", "You have joined the league");
        window.location.href = 'Leagues.aspx';  

        var data = populateUserDetails(false);
        var email = CheckEmail(user);
        if (email == 1)
        {
            emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Joined ", "<h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
                "You joined a League <br> <br> Have Fun!");
        }
    }
    // already belongs to league
    if (returnData == 2)
    {
        toastr.error('You Already belong to this league!!', 'Error Joining League');
       // popupMessage("large", "Error Joining League", "You Already belong to this league!!");
    }

    // Code does not belong to a league
    if (returnData == 3) {
        toastr.error('Invalid Code', 'Error Joining League');
      //  popupMessage("large", "Error Joining League", "Invalid Code");
    }
}

