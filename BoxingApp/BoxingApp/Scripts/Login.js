﻿$(document).ready(function () {
    $("#LoginBtn").kendoButton({
        click: function() {
            LoginUser(null, null);
        }
    }); 

    $("#emailToUnsubscribeBtn").kendoButton({
        click: function ()
        {
            unsubscribeEmail();
        }
    });

    $("#ForgotUser").kendoButton({
        click: function ()
        {
            var mywindow = $("#ForgotUserDiv");
            mywindow.kendoWindow({
                width: "40%",
                title: "Forgot User Name",
                visible: false,
            }).data("kendoWindow").center().open();

        }
    });


    $("#sendUserNameButton").kendoButton({
        click: function ()
        {
            sendUserName();
        }

    });

    $("#getResetCode").kendoButton({
        click: function ()
        {
            resetPassword();
        }

    });

    $("#ForgotPassword").kendoButton({
        click: function ()
        {
            var mywindow = $("#ResetPasswordDiv");
            mywindow.kendoWindow({
                width: "40%",
                title: "Reset Password",
                visible: false,
            }).data("kendoWindow").center().open();

        }
    });

    $("#resetPasswordButton").kendoButton({
        click: function ()
        {
            resetChangePassword();
        }

    });
});


function unsubscribeEmail()
{
    var Email = getParameterByName('email');

    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            Email: Email
        }),
        url: "/Views/Unsubscribe.aspx/UnsubscribeEmail",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
        },
        complete: function ()
        {
            window.location.href = "/Index.aspx";
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

 function resetChangePassword()
{
    var code = $("#ResetCode").val();
    var pw = $("#resetpassword").val();
    var pw1 = $("#resetPassword2").val();
    if (pw != pw1)
    {
        popupMessage("large", "Error", "Passwords Do not Match");
        return
    }
    if (pw.length < 8)
    {
        popupMessage("large", "Invalid Password", "Password must be atleast 8 characters long.");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            code: code,
            password: pw
        }),
        url: "/Login.aspx/ResetupdatePassword",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d == 0)
            {
                popupMessage("large", "Error Password Change", "The code you entered is incorrect");
            }
            else
            {
                popupMessage("large", "Password Changed", "Your Password has been changed");
                $("#resetPwDiv").css("display", "none");
                $("#ResetCode").val("");
                $("#resetpassword").val("");
                $("#resetPassword2").val("");
                $("#resetEmail").val("");
                var mywindow = $("#ResetPasswordDiv").data("kendoWindow");
                mywindow.close();
            }
        },
        complete: function (object)
        {


        },
        error: function (object)
        {
        }
    });
}

function resetPassword()
{
    var email = $("#resetEmail").val();
    var Emailval = validateEmail(email);
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            email: email
        }),
        url: "/Login.aspx/resetUserCode",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d == 0)
            {
                popupMessage("large", "Email Invalid", "No account is associated with that Email");
            }
            else
            {
                popupMessage("large", "Email Sent", "please check your emails for the code we just sent.</br> note* Check your Junk Folder");
                $("#resetPwDiv").css("display", "block");
            }
        },
        complete: function (object)
        {
        },
        error: function (object)
        {
        }
    });
}

function sendUserName()
{
    var email = $("#ForgotUserEmail").val();
    var Emailval = validateEmail(email);
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            email: email
        }),
        url: "/Login.aspx/SendUserName",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d.length == 0)
            {
                popupMessage("large", "Email Invalid", "No account is associated with that Email");
            }
            else
            {
                emailNotification("forgotpassword@boxing5.com", "Boxing5", email, "Boxing5 Username", "<br/><br/> Thank you for requesting your Username <br/> " +
                    "<br/>Your Username is <br/><br/>" + object.d + "</br>");

                popupMessage("large", "Email Sent", "please check your emails for your userNaem.</br> note* Check your Junk Folder");
                $("#ForgotUserEmail").val("");
                var mywindow = $("#ForgotUserDiv").data("kendoWindow");
                mywindow.close();

            }
        },
        complete: function (object)
        {
        },
        error: function (object)
        {
        }
    });
}

function LoginUser(outhProvider,outhId) {
    var userName;
    var password;
    if (outhProvider == null && outhId == null) {
         userName = $("#RegisterUserNameInput").val();
         password = $("#RegisterPasswordInput").val();
    }
    else {
        userName = null;
        password = null;
    }
    var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                userName: userName, Password: password,outhProvider:outhProvider,outhId
            }),
            url: "/Login.aspx/LoginUser",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                data = object.d;

                if (object.d == -1) {
                    $("#incorrectDetails")[0].innerText = "Incorrect login details";
                }
                else {
                    $.cookie("cookie", window.btoa(object.d), { path: '/' });

                    var userD = getUserDetails(object.d);

                    document.cookie = "user=" + '<p>' + userD[0].FirstName + ' ' + userD[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
                }
            },
            complete: function ()
            {      
                if (data != -1)
                {
                    window.location.href = "/Views/Selection.aspx";
                    $("#signedIn").text("You are signed in");
                    $("#loginDetails").css('display', "none");
                    $("#socialLoginFB").css('display', 'none');
                }
            },
            error: function (object) {
                console.log("error" + object);
            }
        });         
}

function getUserDetails(user)
{
    var Data;

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Login.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            Data = object.d;
        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return Data;

}