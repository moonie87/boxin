$(document).ready(function ()
{
    var weeks = getWeeksForSeason(1);
    getResultsforWeek(weeks);
    var id = parserId();
    getpredictedresults(id, weeks);

    $("#left").kendoButton({
        click: function ()
        {
            var val = parseInt($("#weekLabel").text());
            if (val == 1)
            {
            } else
            {
                $("#weekLabel").text(val - 1);
                $("#results").empty();
                getResultsforWeek(val - 1);
                $("#predictResults").empty();
                getpredictedresults(id, val - 1);
                calculatePoints();
                checkresults();
            }
        }
    });

    $("#right").kendoButton({
        click: function ()
        {
            var val = parseInt($("#weekLabel").text());
            if (val == weeks)
            {
            } else
            {
                $("#weekLabel").text(val + 1);
                $("#results").empty();
                getResultsforWeek(val + 1);
                $("#predictResults").empty();
                getpredictedresults(id, val + 1);
                calculatePoints();
                checkresults();
            }
        }
    });

    calculatePoints();
    checkresults();

});

function getWeeksForSeason(season)
{
    var length;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: season
        }),
        url: "/Views/Results.aspx/getWeeksforSeason",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");
            div.style = "overflow-y: auto; display:table; margin:auto;";

            var div1 = document.createElement("div");
            div1.style = "float:left; width:75%;";

            var div2 = document.createElement("div");
            div2.style = "float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-left: 1%;margin-right: 1%; margin-top:1%;";
            div2.className = "ActualText";

            var buttonDiv = document.createElement("div");

            var button = document.createElement("button");
            button.id = "left";
            button.type = "button";
            button.dataset;
            button.dataset.id = data[length - 1].week;
            button.style = "width: 45%; margin-left: 5%;";

            var leftButton = document.createElement("i");
            leftButton.className = "fa fa-arrow-left fa-lg";

            button.appendChild(leftButton);

            var weekLab = document.createElement("h3");
            weekLab.innerHTML = "Week";
            weekLab.style = "display:table; margin:auto; margin-left: 50px; margin-right: 50px;";

            var lab = document.createElement("h3");
            lab.id = "weekLabel";
            lab.innerHTML = data[length - 1].week;
            lab.style = "width:3%; font-weight: bolder;";
            lab.classList = "centreText";

            var break1 = document.createElement("br");

            var buttonr = document.createElement("button");
            buttonr.id = "right";
            buttonr.type = "button";
            buttonr.dataset;
            buttonr.dataset.id = data[length - 1].week;
            buttonr.style = "width:45%; margin-right:5%;";

            var rightButton = document.createElement("i");
            rightButton.className = "fa fa-arrow-right fa-lg";

            buttonr.appendChild(rightButton);

            div2.appendChild(weekLab);
            div2.appendChild(lab);
            div2.appendChild(break1);
            buttonDiv.appendChild(button);
            buttonDiv.appendChild(buttonr);

            div2.appendChild(buttonDiv);

            div.appendChild(div1);
            div.appendChild(div2);

            $("#weeks").append(div);
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function getResultsforWeek(week)
{

    $.ajax({
        type: "post",
        data: JSON.stringify({
            week: week
        }),
        url: "/Views/Results.aspx/getResultsforweek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");

            var tieArray = [];
            for (var i = 0; i < data.length; i++)
            {
                var bouttext;

                if (i == 0) {
                    bouttext = "Main Event"
                }
                else {
                    bouttext = "Undercard " + i;
                }

                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.className = "actualTextDiv";
                //div2.style = "min-width:100%; float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-right: 1%; margin-top:1%; font-size: xx-large;";


                var boutTextDiv = document.createElement("div");
                boutTextDiv.className = "actualTextDiv1";


                var boutText = document.createElement("label");
                boutText.innerHTML = "<h2>" + bouttext + "</h2>";
                boutText.className = "ActualText";


                var imageDiv = document.createElement("div");
                imageDiv.className = "imageDiv";
                //imageDiv.style = "float: left; width:100%;";

                if (data[i].outcomeText == "Draw")
                {
                    var image = document.createElement("img");
                    image.src = "/Content/draw3.png";
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                else
                {
                    var image = document.createElement("img");
                    image.src = data[i].link;
                    image.className = "actualImage"
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                imageDiv.appendChild(image);

                var LabelDiv = document.createElement("div");
                LabelDiv.className = "ActualText"
                //LabelDiv.style = "float: right; width: 60%;";

                if (data[i].outcomeText == "Draw")
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + i + ">Draw<h2/> " + "<h2 id=outcome" + i + "> " + data[i].outcomeText + "<h2/><h2 id=round" + i + "> Round: " + data[i].round + "<h2/>";
                    lab.style = "float:left; width:100%;";
                }
                else
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + i + ">" + data[i].boxerName + "<h2/> " + "<h2 id=outcome" + i + "> " + data[i].outcomeText + "<h2/><h2 id=round" + i + "> Round: " + data[i].round + "<h2/>";
                    lab.style = "float:left; width:100%;";
                }
                LabelDiv.appendChild(lab);

                var breaker = document.createElement("br");

                boutTextDiv.appendChild(boutText);

                div2.appendChild(boutTextDiv)
                div2.appendChild(imageDiv);
                div2.appendChild(LabelDiv);
                div2.appendChild(breaker);

                div.appendChild(div1);
                div.appendChild(div2);

                $("#results").append(div);

                tieArray.push(data[i].tieBreaker);

                if (i == data.length - 1)
                {
                    var min = Math.min.apply(Math, tieArray)

                    var tieDiv = document.createElement("div");
                    tieDiv.style = "float: right; width: 75%;";

                    var tieLab = document.createElement("label");
                    tieLab.id = "resultsLabel" + i;
                    tieLab.innerHTML = "<h1 id=TieResult" + i + ">Tie Breaker: &nbsp" + min + "</h1>";
                    tieLab.style = "float:left; width:100%;";

                    tieDiv.appendChild(tieLab);

                    $("#results").append(tieDiv);
                }
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function getpredictedresults(user, week)
{
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, week: week
        }),
        url: "/Views/Results.aspx/getpredictedforweek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");
            var inc = 6;
            for (var i = 0; i < data.length; i++)
            {

                var bouttext;

                if (i == 0) {
                    bouttext = "Main Event"
                }
                else {
                    bouttext = "Undercard " + i;
                }

                var boutTextDiv = document.createElement("div");
                boutTextDiv.className = "actualTextDiv1";


                var boutText = document.createElement("label");
                boutText.innerHTML = "<h2>" + bouttext + "</h2>";
                boutText.className = "ActualText";

                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.id = "predictedBoxer" + i
                div2.className = "actualTextDiv";
                //div2.style = "min-width:100%; float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-right: 1%; margin-top:1%; font-size: xx-large;";

                var imageDiv = document.createElement("div");
                imageDiv.className = "imageDiv";
                //imageDiv.style = "float: left; width:35%;";

                if (data[i].outcomeText == "Draw")
                {
                    var image = document.createElement("img");
                    image.src = "/Content/draw3.png";
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                else
                {
                    var image = document.createElement("img");
                    image.src = data[i].link;
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                imageDiv.appendChild(image);

                var LabelDiv = document.createElement("div");
                LabelDiv.className = "ActualText"
                //LabelDiv.style = "float: right; width: 60%;";
                if (data[i].outcomeText == "Draw")
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + inc + ">Draw<h2/> " + "<h2 id=outcome" + inc + "> " + data[i].outcomeText + "<h2/><h2 id=round" + inc + "> Round: " + data[i].round + "<h2/>";
                    inc++;
                    lab.style = "float:left; width:100%;";
                }
                else
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + inc + ">" + data[i].boxerName + "<h2/> " + "<h2 id=outcome" + inc + "> " + data[i].outcomeText + "<h2/><h2 id=round" + inc + "> Round: " + data[i].round + "<h2/>";
                    inc++;
                    lab.style = "float:left; width:100%;";
                }


                LabelDiv.appendChild(lab);

                var breaker = document.createElement("br");

                boutTextDiv.appendChild(boutText);

                div2.appendChild(boutTextDiv);
                div2.appendChild(imageDiv);
                div2.appendChild(LabelDiv);
                div2.appendChild(breaker);

                div.appendChild(div1);
                div.appendChild(div2);

                $("#predictResults").append(div);

                if (i == data.length - 1)
                {
                    var tieDiv = document.createElement("div");
                    tieDiv.style = "float: right; width: 75%;";

                    var tieLab = document.createElement("label");
                    tieLab.id = "resultsLabel" + i;
                    tieLab.innerHTML = "<h1 id=Tie" + inc + ">Tie Breaker: &nbsp" + data[i].tieBreaker + "</h1>";
                    inc++;
                    tieLab.style = "float:left; width:100%;";

                    tieDiv.appendChild(tieLab);

                    $("#predictResults").append(tieDiv);

                }

                // boxers corresponds
                // 1 > 6
                // 2 > 7
                // 3 > 8
                // 4 > 9
                // 5 > 10

            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function calculatePoints()
{
    try
    {
        var Totpoints = 0;
        var correctBoxers = 0;
        var correctOutcomes = 0;
        var correctRounds = 0;

        // bout1
        var b0 = $("#boxer0")[0].innerText;
        var b6 = $("#boxer6")[0].innerText;
        if (b0 == b6)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O0 = $("#outcome0")[0].innerText;
        var O6 = $("#outcome6")[0].innerText;
        if (O0 == O6)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R0 = $("#round0")[0].innerText;
        var R6 = $("#round6")[0].innerText;
        if (R0 == R6)
        {
            Totpoints += 10;
            correctRounds += 1;
        }
        //bout2
        var b1 = $("#boxer1")[0].innerText;
        var b7 = $("#boxer7")[0].innerText;
        if (b1 == b7)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O1 = $("#outcome1")[0].innerText;
        var O7 = $("#outcome7")[0].innerText;
        if (O1 == O7)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R1 = $("#round1")[0].innerText;
        var R7 = $("#round7")[0].innerText;
        if (R1 == R7)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 3
        var b2 = $("#boxer2")[0].innerText;
        var b8 = $("#boxer8")[0].innerText;
        if (b2 == b8)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O2 = $("#outcome2")[0].innerText;
        var O8 = $("#outcome8")[0].innerText;
        if (O2 == O8)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R2 = $("#round2")[0].innerText;
        var R8 = $("#round8")[0].innerText;
        if (R2 == R8)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 4
        var b3 = $("#boxer3")[0].innerText;
        var b9 = $("#boxer9")[0].innerText;
        if (b3 == b9)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O3 = $("#outcome3")[0].innerText;
        var O9 = $("#outcome9")[0].innerText;
        if (O3 == O9)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R3 = $("#round3")[0].innerText;
        var R9 = $("#round9")[0].innerText;
        if (R3 == R9)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 5
        var b4 = $("#boxer4")[0].innerText;
        var b10 = $("#boxer10")[0].innerText;
        if (b4 == b10)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O4 = $("#outcome4")[0].innerText;
        var O10 = $("#outcome10")[0].innerText;
        if (O4 == O10)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R4 = $("#round4")[0].innerText;
        var R10 = $("#round10")[0].innerText;
        if (R4 == R10)
        {
            Totpoints += 10;
            correctRounds += 1;
        }
    }
    catch (score) { }
    try
    {
        $("#boxersValue")[0].innerText = correctBoxers.toString();
        $("#outcomesValue")[0].innerText = correctOutcomes.toString();
        $("#roundsValue")[0].innerText = correctRounds.toString();
        $("#pointsValue")[0].innerText = Totpoints.toString();
    }
    catch (showPointValues) { }
}

function checkresults()
{
    var bout1 = 0;
    var bout2 = 0;
    var bout3 = 0;
    var bout4 = 0;
    var bout5 = 0;

    //bout1
    var boxer0 = $("#boxer0");
    var boxer6 = $("#boxer6");
    try {
        if (boxer0[0].innerText == boxer6[0].innerText) {
            boxer6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            boxer6[0].style.color = "Red";
        }
        var outcome0 = $("#outcome0");
        var outcome6 = $("#outcome6");
        if (outcome0[0].innerText == outcome6[0].innerText) {
            outcome6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            outcome6[0].style.color = "Red";
        }
        var round0 = $("#round0");
        var round6 = $("#round6");
        if (round0[0].innerText == round6[0].innerText) {
            round6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            round6[0].style.color = "Red";
        }
        if (bout1 == 3) {
            var div = $("#predictedBoxer0")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer0")[0].style.borderColor = "red";
        }

        //bout2
        var boxer1 = $("#boxer1");
        var boxer7 = $("#boxer7");
        if (boxer1[0].innerText == boxer7[0].innerText) {
            boxer7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            boxer7[0].style.color = "Red";
        }
        var outcome1 = $("#outcome1");
        var outcome7 = $("#outcome7");
        if (outcome1[0].innerText == outcome7[0].innerText) {
            outcome7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            outcome7[0].style.color = "Red";
        }
        var round1 = $("#round1");
        var round7 = $("#round7");
        if (round1[0].innerText == round7[0].innerText) {
            round7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            round7[0].style.color = "Red";
        }
        if (bout2 == 3) {
            var div = $("#predictedBoxer1")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer1")[0].style.borderColor = "red";
        }

        //bout3
        var boxer2 = $("#boxer2");
        var boxer8 = $("#boxer8");
        if (boxer2[0].innerText == boxer8[0].innerText) {
            boxer8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            boxer8[0].style.color = "Red";
        }
        var outcome2 = $("#outcome2");
        var outcome8 = $("#outcome8");
        if (outcome2[0].innerText == outcome8[0].innerText) {
            outcome8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            outcome8[0].style.color = "Red";
        }
        var round2 = $("#round2");
        var round8 = $("#round8");
        if (round2[0].innerText == round8[0].innerText) {
            round8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            round8[0].style.color = "Red";
        }
        if (bout3 == 3) {
            var div = $("#predictedBoxer2")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer2")[0].style.borderColor = "red";
        }

        //bout4
        var boxer3 = $("#boxer3");
        var boxer9 = $("#boxer9");
        if (boxer3[0].innerText == boxer9[0].innerText) {
            boxer9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            boxer9[0].style.color = "Red";
        }
        var outcome3 = $("#outcome3");
        var outcome9 = $("#outcome9");
        if (outcome3[0].innerText == outcome9[0].innerText) {
            outcome9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            outcome9[0].style.color = "Red";
        }
        var round3 = $("#round3");
        var round9 = $("#round9");
        if (round3[0].innerText == round9[0].innerText) {
            round9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            round9[0].style.color = "Red";
        }
        if (bout4 == 3) {
            var div = $("#predictedBoxer3")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer3")[0].style.borderColor = "red";
        }

        //bout5
        var boxer4 = $("#boxer4");
        var boxer10 = $("#boxer10");
        if (boxer4[0].innerText == boxer10[0].innerText) {
            boxer10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            boxer10[0].style.color = "Red";
        }
        var outcome4 = $("#outcome4");
        var outcome10 = $("#outcome10");
        if (outcome4[0].innerText == outcome10[0].innerText) {
            outcome10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            outcome10[0].style.color = "Red";
        }
        var round4 = $("#round4");
        var round10 = $("#round10");
        if (round4[0].innerText == round10[0].innerText) {
            round10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            round10[0].style.color = "Red";
        }
        if (bout5 == 3) {
            var div = $("#predictedBoxer4")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer4")[0].style.borderColor = "red";
        }
        var tie4 = $("#TieResult4");
        var tie11 = $("#Tie11");
        if (tie4[0].innerText == tie11[0].innerText) {
            tie11[0].style.color = "Green";
        }
        else {
            tie11[0].style.color = "Red";
        }
    }
    catch (innerTextWarning) { }
}
$(document).ready(function () {

    $("#RegisterBtn").kendoButton({
        click: function ()
        {
            RegisterUser();
        }
    });
});



function RegisterUser(emailA,firstnameA,lastnameA,outhProvider,outhId) {
    var userName, email, firstName, lastName, password, password2,passwordCheck,emailVal;
    if (outhProvider != null && outhId != null)
    {
        email = emailA;
        firstName = firstnameA;
        lastName = lastnameA;
        password = null;
        userName = null;
    }
    else
    {
        userName = $("#RegisterUserNameInput").val();
        email = $("#RegisterEmailInput").val();
        firstName = $("#RegisterFirstNameInput").val();
        lastName = $("#RegisterLastNameInput").val();
        password = $("#RegisterPasswordInput").val();
        password2 = $("#RegisterPassword2Input").val();
        passwordCheck = true;       
       
            emailVal = validateEmail(email);
        
        if (password.length < 8)
        {
            popupMessage("large", "Invalid Password", "Password must be atleast 8 characters long.");
            return false;
        }
       
    }
  
    if (password != password2)
    {
        passwordCheck = false;
    }
    if (outhId != null)
    {       

        $.ajax({
            cache: false,
            headers: {"cache-control":"no-cache"},
            type: "post",
            data: JSON.stringify({
                userName: userName, Email: email, FName: firstName, LName: lastName, Password: password, outh_provider: outhProvider, outhId: outhId
            }),
            url: "/Views/Register.aspx/RegisterUser",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function () {                
            },
            complete: function () {
                popupMessage("small", "Registered", "You have registerd");
                emailNotification("forgotpassword@boxing5.com", "Boxing5", email, "Welcome to Boxing5", "<h1>Welcome to boxing5.com</h1></br>" + firstName + " " + lastName + ",</br> Thank you for registering with Boxing5. </br> Boxing5 is a boxing prediction application." +
                    "Predict 5 bouts a week and grab a possible 90 points. </br> You can either play just for fun or grab your self a season pass and win prizes.</br></br> Dont forget to invite your friends, family and co-workers and create a mini league for bragging rights.");
                window.location.href = "/Login.aspx";
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
    }
    else
    {
        var passwordconf = checkPassword();
        if (passwordconf == false)
        {
            popupMessage("large", "Error", "Passwords Do not Match");
            return;
        }

        if (emailVal != true)
        {
            popupMessage("large", "Invalid email", "Email address Is not valid");
            return false;
        }

        if (passwordCheck == true && emailVal == true) {
            $.ajax({
                cache: false,
                headers: { "cache-control": "no-cache" },
                type: "post",
                data: JSON.stringify({
                    userName: userName, Email: email, FName: firstName, LName: lastName, Password: password, outh_provider: null, outhId: null
                }),
                url: "/Views/Register.aspx/RegisterUser",
                dataType: "json",
                async: false,
                contentType: "application/json",
                success: function (test) {
                    var test1 = test;
                },
                complete: function (test) {
                    var test1 = test;
                    popupMessage("small", "Registered", "You have registerd");
                    emailNotification("forgotpassword@boxing5.com", "Boxing5", email, "Welcome to Boxing5", "<h1>Welcome to boxing5.com</h1></br>" + firstName + " " + lastName + ",</br> Thank you for registering with Boxing5. </br> Boxing5 is a boxing prediction application." +
                        "Predict 5 bouts a week and grab a possible 90 points. </br> You can either play just for fun or grab your self a season pass and win prizes.</br></br> Dont forget to invite your friends, family and co-workers and create a mini league for bragging rights.");
                    window.location.href = "/Login.aspx";
                },
                error: function (object) {
                    console.log("error" + object);
                }
            });
        }
        else if (passwordCheck == false) {
            $("#RegisterPasswordInput").addClass("userInputError");
            $("#RegisterPassword2Input").addClass("userInputError");
        }
        else if (emailVal == false) {
            $("#RegisterEmailInput").addClass("userInputError");
        }
    }
}
function checkPassword()
{
    var password = $("#RegisterPasswordInput").val();
    var passwordconfirm = $("#RegisterPassword2Input").val();
    var ok = true;
    if (password != passwordconfirm)
    {

        document.getElementById("RegisterPasswordInput").style.borderColor = "#E34234";
        document.getElementById("RegisterPassword2Input").style.borderColor = "#E34234";
        ok = false;
    }
    return ok;
} 
$(document).ready(function ()
{
    var data = [       
        { text: "Minimumweight", value: "1" },
        { text: "Light Flyweight", value: "2" },
        { text: "Flyweight", value: "3" },
        { text: "Super flyweight", value: "4" },
        { text: "Bantamweight", value: "5" },
        { text: "Super Bantamweight", value: "6" },
        { text: "Featherweight", value: "19" },
        { text: "Super Featherweight", value: "18" },
        { text: "Lightweight", value: "7" },
        { text: "Super lightweight", value: "8" },
        { text: "Welterweight", value: "16" },
        { text: "Super Welterweight", value: "17" },
        { text: "Middleweight", value: "9" },
        { text: "Super Middleweight", value: "10" },
        { text: "Light Heavyweight", value: "11" },
        { text: "Cruiserweight", value: "12" },
        { text: "Heavyweight", value: "13" },
    ];

    var Federationdata = [       
        { text: "WBA", value: "1" },
        { text: "IBF", value: "2" },
        { text: "IBO", value: "3" },
        { text: "WBO", value: "4" },
        { text: "WBC", value: "5" }

    ]

    // create DropDownList from input HTML element
    $("#Division").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeFederation
    });

    // create DropDownList from input HTML element
    $("#Federation").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: Federationdata,
        index: 0,
        change: onChangeFederation
    });

    onChangeFederation();

    function onChangeFederation()
    {
        var value = $("#Federation").val();
        var text = $("#Federation").data("kendoDropDownList").text();

        var division = $("#Division").val();
        var DivText = $("#Division").data("kendoDropDownList").text();
       
        var newTData = getRankings(text, division);
        populateTable(newTData);
        $("#rankingsName").text(text + " " + DivText);
    }  
   
});
    function populateTable(data){
        var a = 0;
        var table = $('#rankingsTable').DataTable({

            destroy: true,
            "ordering": false,
            "paging": false,
            select: {
                style: 'os',
                blurable: true
            },

            data: data,
            columns: [

                { data: 'id' },
                { data: 'Boxing5Id'},
                { data: 'name' },
                { data: 'won' },
                { data: 'lost' },
                { data: 'drawn' },

            ],
           "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull)
           {
               if (aData.PosId == "1")
               {
                   $('td', nRow).css('background-color', 'Gold');
                   $('td:eq(0)', nRow).html('<i class="fa fa-trophy" aria-hidden="true"></i>');
               }

           
                    var table = $("#rankingsTable").DataTable();

                    $('#rankingsTable tbody').off('click');

                    $("#rankingsTable tbody").on('dblclick', 'tr', function ()
                    {
                        var rowData = table.rows(this).data().toArray();
                        console.log(rowData);
                        //var att = $(this).attr('id');
                        //var tr = $(this).closest("tr");
                        //var data = table.rows(tr).data();
                        //alert('you clicked' + data[0].name);
                    })                    
                }
            
        });      
    }

    function getRankings(federation,div) {

        var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                federation: federation, div:div
            }),
            url: "/Views/rankings.aspx/getRankings",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object) {
                data = object.d;
            },
            complete: function () {
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
        return data;
    }


    paypal.Button.render({

        env: 'sandbox', // Or 'sandbox',

        locale: 'en_GB',

        style:{
            size: 'large',
            color:'blue',
            shape: 'pill',
            label: 'buynow',
            branding:true,
        },

        client: {
            sandbox: 'AXcUgnH3yEqnqeX_Gc26di_GnkAgeRx25CuQEOjOAPZyz9XbknsbCM9XxQwi96sTl1Ii0x9rUvH408jB',
            production: 'ATgldSOyHnviIJskZC7FJ0eaRKRjbKGG_YiJ9_RX5XMowyfvS5m6SfgwsbZK9llBlVw90D2JnYBE_HC5'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function (data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '2.99', currency: 'GBP' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function (data, actions) {
            return actions.payment.execute().then(function (payment) {

                var t = payment.transactions["0"].related_resources["0"].sale;

                var p_id = payment.id;
                var id = t.id;
                var state = t.state;
                var user = parserId();

                var payer = payment.payer.payer_info;
                var email = payer.email;
                var fname = payer.first_name;
                var lname = payer.last_name;
                var payerId = payer.payer_id;

                $.ajax({
                    type: "post",
                    data: JSON.stringify({
                        userId: user,
                        Reciept: id,
                        parentPayId: p_id,
                        state: state,
                        email: email,
                        fname: fname,
                        lname: lname,
                        payerId:payerId
                    }),
                    url: "/Views/SeasonPass.aspx/AddReceipt",
                    dataType: "json",
                    async: false,
                    contentType: "application/json",
                    success: function () {
                        popupMessage("large", "Season Pass", "Thank you for purchasing a season pass. Good Luck!!");
                    },
                    complete: function ()
                    {
                        data = populateUserDetails(false);                       
                        emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Season Pass Purchase", data[0].FirstName + " " + data[0].LastName + ",<br> This is your recipt for purchasing a season pass<br>"
                            + "your recipt id is :" + id + "<br> Name : " + fname + " " + lname + "<br> Email :" + email + "<br><br> <h2>GOOD LUCK THIS SEASON</h2>");
                        $("#seasonLabel")[0].innerText = "Season Pass Active";
                    },
                    error: function (object) {
                        console.log("error" + object);
                    }
                });             
            });
        },

        onError: function () {
            bootbox.alert({
                message: "Error purchasing",
                size: 'large'
            });
        }

    }, '#paypal-button');

$(document).ready(function () {
    $("#LoginBtn").kendoButton({
        click: function() {
            LoginUser(null, null);
        }
    }); 

    $("#emailToUnsubscribeBtn").kendoButton({
        click: function ()
        {
            unsubscribeEmail();
        }
    });

    $("#ForgotUser").kendoButton({
        click: function ()
        {
            var mywindow = $("#ForgotUserDiv");
            mywindow.kendoWindow({
                width: "40%",
                title: "Forgot User Name",
                visible: false,
            }).data("kendoWindow").center().open();

        }
    });


    $("#sendUserNameButton").kendoButton({
        click: function ()
        {
            sendUserName();
        }

    });

    $("#getResetCode").kendoButton({
        click: function ()
        {
            resetPassword();
        }

    });

    $("#ForgotPassword").kendoButton({
        click: function ()
        {
            var mywindow = $("#ResetPasswordDiv");
            mywindow.kendoWindow({
                width: "40%",
                title: "Reset Password",
                visible: false,
            }).data("kendoWindow").center().open();

        }
    });

    $("#resetPasswordButton").kendoButton({
        click: function ()
        {
            resetChangePassword();
        }

    });
});


function unsubscribeEmail()
{
    var Email = getParameterByName('email');

    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            Email: Email
        }),
        url: "/Views/Unsubscribe.aspx/UnsubscribeEmail",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
        },
        complete: function ()
        {
            window.location.href = "/Index.aspx";
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

 function resetChangePassword()
{
    var code = $("#ResetCode").val();
    var pw = $("#resetpassword").val();
    var pw1 = $("#resetPassword2").val();
    if (pw != pw1)
    {
        popupMessage("large", "Error", "Passwords Do not Match");
        return
    }
    if (pw.length < 8)
    {
        popupMessage("large", "Invalid Password", "Password must be atleast 8 characters long.");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            code: code,
            password: pw
        }),
        url: "/Login.aspx/ResetupdatePassword",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d == 0)
            {
                popupMessage("large", "Error Password Change", "The code you entered is incorrect");
            }
            else
            {
                popupMessage("large", "Password Changed", "Your Password has been changed");
                $("#resetPwDiv").css("display", "none");
                $("#ResetCode").val("");
                $("#resetpassword").val("");
                $("#resetPassword2").val("");
                $("#resetEmail").val("");
                var mywindow = $("#ResetPasswordDiv").data("kendoWindow");
                mywindow.close();
            }
        },
        complete: function (object)
        {


        },
        error: function (object)
        {
        }
    });
}

function resetPassword()
{
    var email = $("#resetEmail").val();
    var Emailval = validateEmail(email);
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            email: email
        }),
        url: "/Login.aspx/resetUserCode",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d == 0)
            {
                popupMessage("large", "Email Invalid", "No account is associated with that Email");
            }
            else
            {
                popupMessage("large", "Email Sent", "please check your emails for the code we just sent.</br> note* Check your Junk Folder");
                $("#resetPwDiv").css("display", "block");
            }
        },
        complete: function (object)
        {
        },
        error: function (object)
        {
        }
    });
}

function sendUserName()
{
    var email = $("#ForgotUserEmail").val();
    var Emailval = validateEmail(email);
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    $.ajax({
        type: "post",
        data: JSON.stringify({
            email: email
        }),
        url: "/Login.aspx/SendUserName",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            if (object.d.length == 0)
            {
                popupMessage("large", "Email Invalid", "No account is associated with that Email");
            }
            else
            {
                emailNotification("forgotpassword@boxing5.com", "Boxing5", email, "Boxing5 Username", "<br/><br/> Thank you for requesting your Username <br/> " +
                    "<br/>Your Username is <br/><br/>" + object.d + "</br>");

                popupMessage("large", "Email Sent", "please check your emails for your userNaem.</br> note* Check your Junk Folder");
                $("#ForgotUserEmail").val("");
                var mywindow = $("#ForgotUserDiv").data("kendoWindow");
                mywindow.close();

            }
        },
        complete: function (object)
        {
        },
        error: function (object)
        {
        }
    });
}

function LoginUser(outhProvider,outhId) {
    var userName;
    var password;
    if (outhProvider == null && outhId == null) {
         userName = $("#RegisterUserNameInput").val();
         password = $("#RegisterPasswordInput").val();
    }
    else {
        userName = null;
        password = null;
    }
    var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                userName: userName, Password: password,outhProvider:outhProvider,outhId
            }),
            url: "/Login.aspx/LoginUser",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                data = object.d;

                if (object.d == -1) {
                    $("#incorrectDetails")[0].innerText = "Incorrect login details";
                }
                else {
                    $.cookie("cookie", window.btoa(object.d), { path: '/' });

                    var userD = getUserDetails(object.d);

                    document.cookie = "user=" + '<p>' + userD[0].FirstName + ' ' + userD[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
                }
            },
            complete: function ()
            {      
                if (data != -1)
                {
                    window.location.href = "/Views/Selection.aspx";
                    $("#signedIn").text("You are signed in");
                    $("#loginDetails").css('display', "none");
                    $("#socialLoginFB").css('display', 'none');
                }
            },
            error: function (object) {
                console.log("error" + object);
            }
        });         
}

function getUserDetails(user)
{
    var Data;

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Login.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            Data = object.d;
        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return Data;

}
var codes = [];

$(document).ready(function () {
    $("#createleague").kendoButton({
        click: function() {
            window.location.href = 'CreateLeague.aspx';
        }
    });

    $("#joinLeague").kendoButton({
        click: function() {
            window.location.href = 'joinLeague.aspx';
        }
    });
    var user = parserId();
    var leagues = getLeaguesForUser(user);

    for (var i = 0; i < leagues.length; i++)
    {  
        $("#FB" + codes[i]).click(function (e) {
            data = this.dataset.code;
            e.preventDefault();
            FB.ui(
            {
                method: 'share',
                href: "https://boxing5.com", 
                quote: "join my league on Boxing5.com. The code to enter on the leagues screen is: " + data,              
            });
        });

        $("#" + codes[i]).kendoButton({
            click: function ()
            {
                var league = this;
                var check = checkLeague(league);

                if (check == 0)
                {
                    bootbox.alert({
                        message: "You dont own this league, You can not delete it.",
                        size: 'large'
                    });
                }
                else if (check > 0)
                {
                    bootbox.confirm({
                        title: "Delete League?",
                        message: "Are you sure you want to delete this league? This cannot be undone.",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> No'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Yes'
                            }
                        },
                        callback: function (result)
                        {
                            if (result == true)
                            {
                                DeleteLeague(league);                                
                            }
                            window.location.href = 'Leagues.aspx';
                        }
                    });
                }
            }
        });
        $("#" + leagues[i] + i).kendoButton({
            click: function() {
                data = this.element[0].dataset.id;
                var newTData = getLeagues(data);

                $("#headerLeague").text(this.element[0].dataset.name);

                $('#leaguesTable').DataTable({
                    destroy: true,
                    "ordering": false,
                    responsive: true,
                    data: newTData,
                    columns: [
                        { data: 'position' },
                        { data: 'name' },
                        { data: 'correctResult' },
                        { data: 'correctRounds' },
                        { data: 'correctBoxers' },
                        { data: 'score' }
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        if (aData.position == "1") {
                            $('td', nRow).css('background-color', 'Gold');
                        } else if (aData.position == "2") {
                            $('td', nRow).css('background-color', 'silver');
                        } else if (aData.position == "3") {
                            $('td', nRow).css('background-color', 'orange');
                        }
                    }
                });

                document.getElementById("leaguesTable_wrapper").scrollIntoView();
            }
        });
    }  
});

function getLeaguesForUser(user) {
    var leagues = [];
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Views/Leagues.aspx/getLeaguesForUser",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
          
            data = object.d;

            var div = document.createElement("div");            
            div.style = "overflow-y: auto; margin-bottom: 5px;";

            for(var i = 0; i < data.length; i++)
            {
                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.classList = "leagueContent";

                var label = document.createElement("label");
                label.innerHTML = data[i].Name;
                label.style = "display: table; margin: auto;";

                var button = document.createElement("input");
                button.id = data[i].Name + i;
                button.value = "View League";
                button.type = "button";
                button.title = "View League";
                button.dataset;
                button.dataset.id = data[i].parentId;
                button.dataset.name = data[i].Name;
                button.style = "margin-top: 10px !important; margin-bottom: 10px !important;display: table; margin: auto;";
                button.classList = "topMenu";

                var buttonDelete = document.createElement("button");
                buttonDelete.id = data[i].Code;
                buttonDelete.type = "button";
                buttonDelete.title = "Delete league";
                buttonDelete.dataset;
                buttonDelete.dataset.id = data[i].Code;                         

                var deleteIcon = document.createElement("i");
                deleteIcon.className = "fa fa-trash fa-lg";

                var codeLabel = document.createElement("label");
                codeLabel.id = "codeLabel";
                codeLabel.innerHTML = "League Code &nbsp";
                codeLabel.style = "display:table; margin:auto;";

                var code = document.createElement("Label");
                code.innerHTML = data[i].Code;
                code.style = "display:table; margin:auto; border-style:groove; border-radius: 10px; border-spacing:3px; margin-top:5px;";

                var codeshareFB = document.createElement("img");
                codeshareFB.src = "//login.create.net/images/icons/user/facebook_30x30.png";
                codeshareFB.id = "FB" + data[i].Code;
                codeshareFB.dataset;
                codeshareFB.dataset.code = data[i].Code;
                codeshareFB.className = "facebookShare";

                var codeshareT = document.createElement("a");
                codeshareT.href = "https://twitter.com/intent/tweet?text=I just created a league. Join the league: '"+ data[i].Name + "' head to boxing5.co.uk and join the league with this code: '"+ data[i].Code + "'#boxingApp";              
                codeshareT.id = "T" + data[i].Code;
                codeshareT.dataset;
                codeshareT.dataset.code = data[i].Code;

                var tPic = document.createElement("img");
                tPic.src = "//login.create.net/images/icons/user/twitter-b_30x30.png";

                var socialDiv = document.createElement("div");
                socialDiv.style = "float:right;";

                var socialLabel = document.createElement("label");
                socialLabel.innerHTML = "Share on&nbsp&nbsp";

                div2.appendChild(label);
                div2.appendChild(button);               
                div2.appendChild(codeLabel);
                div2.appendChild(code);
                // social media 
                buttonDelete.appendChild(deleteIcon);
                div2.appendChild(buttonDelete);

                codeshareT.appendChild(tPic);
                socialDiv.appendChild(socialLabel);
                socialDiv.appendChild(codeshareFB);
                socialDiv.appendChild(codeshareT);

                div2.appendChild(socialDiv);

                div.appendChild(div1);
                div.appendChild(div2);                
               
                $("#myLeagues").append(div);         
                
                leagues.push(data[i].Name);
                codes.push(data[i].Code);              
            }
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return leagues;
}

function getLeagues(leagueId)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            leagueId:leagueId
        }),
        url: "/Views/Leagues.aspx/getLeagueforUser",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return data;
}

function DeleteLeague(league)
{
    var code = league.element[0].id;
    var user = parserId();
    
    $.ajax({
        type: "post",
        data: JSON.stringify({
            lCode: code, user:user
        }),
        url: "/Views/Leagues.aspx/DeleteLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function ()
        {            
        },
        complete: function ()
        {

            toastr.warning('Selections saved', 'Selection ');

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            //bootbox.alert({
            //    message: "League Deleted",
            //    size: 'large'
            //});
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });   
}

function checkLeague(league)
{
    var code = league.element[0].id;
    var user = parserId();

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            lCode: code, user: user
        }),
        url: "/Views/Leagues.aspx/checkLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            count = object.d;
        },
        complete: function ()
        {
        },
        error: function ()
        {            
        }
    });  
    return count;
}

function onChange() {
    alert("change");
};

$(document).ready(function () {
   var data = getleaderboard();
    $('#example').DataTable({

        responsive: true,
        "ordering": false,
      
        data: data,
        columns: [
            { data: 'position' },
            { data: 'name' },
            { data: 'correctResult' },
            { data: 'correctRounds' },
            { data: 'correctBoxers' },
            { data: 'score' }

        ],
        "fnRowCallback": function (nRow, aData) {
            if (aData.position == "1") {
                $('td', nRow).css('background-color', 'Gold');
            }
            else if (aData.position == "2") {
                $('td', nRow).css('background-color', 'silver');
            }
            else if (aData.position == "3") {
                $('td', nRow).css('background-color', 'orange');
            }
        }
        
   });
    var seasonPassData = getSeasonPassleaderboard();
    $('#SeasonPassTable').DataTable({
        responsive: true,
        "ordering": false,
        

        data: seasonPassData,
        columns: [
            { data: 'position' },
            { data: 'name' },
            { data: 'correctResult' },
            { data: 'correctRounds' },
            { data: 'correctBoxers' },
            { data: 'score' }

        ],
        "fnRowCallback": function (nRow, aData) {
            if (aData.position == "1") {
                $('td', nRow).css('background-color', 'Gold');
            }
            else if (aData.position == "2") {
                $('td', nRow).css('background-color', 'silver');
            }
            else if (aData.position == "3") {
                $('td', nRow).css('background-color', 'orange');
            }
        }

    });

    getActiveSeasonandWeek();
});

function getleaderboard() {
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({          
        }),
        url: "/Views/Leaderboard.aspx/getLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function getSeasonPassleaderboard() {
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getSeasonPassLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function getActiveSeasonandWeek() {
   
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getActiveSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            $("#season").text('Season: ' + object.d[0].Season);
            $("#week").text('Week: ' + object.d[0].week);
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

}
$(document).ready(function () {

    $("#JoinLeagueBtn").kendoButton({
        click: function() {
            joinLeague();
        }
    });
});

function joinLeague() {

    var leagueName = $("#JoinLeagueInput").val();
    var user = parserId();
    var returnData;

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user,code: leagueName
        }),
        url: "/Views/Leagues.aspx/joinLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            returnData = object.d;

           

        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    // added to league
    if (returnData == 1)
    {
        toastr.success('League', 'Joined League successfully');

        


       // popupMessage("large", "Joined League", "You have joined the league");
        window.location.href = 'Leagues.aspx';  

        var data = populateUserDetails(false);
        var email = CheckEmail(user);
        if (email == 1)
        {
            emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Joined ", "<h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
                "You joined a League <br> <br> Have Fun!");
        }
    }
    // already belongs to league
    if (returnData == 2)
    {
        toastr.error('You Already belong to this league!!', 'Error Joining League');
       // popupMessage("large", "Error Joining League", "You Already belong to this league!!");
    }

    // Code does not belong to a league
    if (returnData == 3) {
        toastr.error('Invalid Code', 'Error Joining League');
      //  popupMessage("large", "Error Joining League", "Invalid Code");
    }
}


(function ($)
{
    $(document).ready(function ()
    {
        $('#cssmenu').prepend('<div id="menu-button">Menu</div>');
        $('#cssmenu #menu-button').on('click', function ()
        {
            var menu = $(this).next('ul');
            if (menu.hasClass('open'))
            {
                menu.removeClass('open');
            }
            else
            {
                menu.addClass('open');
            }
        });
    });
})(jQuery);

$(document).ready(function ()
{   

    $(document).keypress(function (e)
    {
        if (e.which == 13)
        {
            if ($("#RegisterPasswordInput").is(":focus"))
            {
                LoginUser(null,null);}
            }
    });  

    var user = parserId(); 

    if (user == 1)
    {
        $("#Admin").css("display", "block");
    }

    $("#Feedback").click(function(){
       
            feedback();
        
    });

    $("#updateAccountDetails").click(function(){
        
            updateAccountDetails();
        
    });  
    
    $("#Terms").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Terms.aspx";
        }
    });

    $("#Privacy").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Privacy.aspx";
        }
    });

    $("#Unsubscribe").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Unsubscribe.aspx";
        }
    });
    
    $("#SeasonPassLeaderboard").click(function(){
        
            window.location.href = "/Views/SeasonPassLeaderboard.aspx";
        
    });

    $("#HomeRegisterBtn").click(function(){
      
            window.location.href = "Views/Register.aspx";
        
    });

    $("#About").click(function () {

        window.location.href = "Views/about.aspx";

    });
    

    $("#RegisterSigninBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#LoginRegisterBtn").kendoButton({
        click: function () {
            window.location.href = "/Views/Register.aspx";
        }
    });

   
    $("#HomeLoginBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Home").click(function(){
       
            window.location.href = "/Index.aspx";
            var user = parserId();
       
    });

    $("#Admin").click(function ()
    {

        window.location.href = "/Views/Admin.aspx?LC=b5105";      

    });

    $("#SeasonPass").click(function(){       
            window.location.href = "/Views/SeasonPass.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#rankings").click(function(){
       
            window.location.href = "/Views/rankings.aspx";
            var user = parserId();

            if (user === -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Selection").click(function(){       
            window.location.href = "/Views/Selection.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leaderboard").click(function(){       
            window.location.href = "/Views/Leaderboard.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leagues").click(function(){       
            window.location.href = "/Views/Leagues.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }
        
    });


    $("#Register").click(function(){
       
            window.location.href = "/Views/Register.aspx";
        
    });

    $("#Login").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Results").click(function(){       
            window.location.href = "/Views/Results.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#logOut").click(function ()
    {  
        
            window.location.href = "/Index.aspx";
            fbLogout();
        
    });

    var cookie = $.cookie('user');  

    $("#userData").html(cookie);

    if (cookie == undefined)
    {  
        try {
            $("#userData").html("");
            $("#Login").css("display", "block");
            $("#Register")[0].style.display = "block";
            $("#logOut")[0].style.display = "none";
            $("#updateAccountDetails").css("display", "none");
            $("#SeasonPass").css("display", "none");
            $("#SeasonPassLeaderboard").css("display", "none");
            $("#HomeRegisterBtn").css("display", "initial");
            $("#HomeLoginBtn").css("display", "initial");
        }
        catch (test) {

        }
    }
    else
    {
        $("#Login").css("display", "none");
        $("#Register").css("display", "none");
        $("#logOut").css("display", "block");
        $("#updateAccountDetails").css("display", "block");
        $("#SeasonPass").css("display", "block");
        $("#HomeRegisterBtn").css("display", "none");
        $("#HomeLoginBtn").css("display", "none");
        $("#SeasonPassLeaderboard").css("display", "none");
    }
    var season = CheckSeasonForUSer();
    //if (season == true)
    //{
    //    $("#SeasonPass").css("display", "none");
    //    $("#seasonLabel")[0].innerText = "Season Pass Active";
    //    $("#SeasonPassLeaderboard").css("display", "block");

    //}
});

function parserId() {
    var userID1 = $.cookie("cookie");
    try {
       
        if (userID1 == undefined) {
            userID1 = -1;
        }
        else {
            var cookieDec = window.atob(userID1);
            userID1 = parseInt(cookieDec);
        }
    }
    catch (id) { }
    return userID1;
}

window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '1539533289441157', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    //// Check whether the user already logged in
    //FB.getLoginStatus(function (response) {
    //    if (response.status === 'connected') {
    //        //display user data
    //        getFbUserData();
    //    }
    //});
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function RegisterFb() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getregisterData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function getFbUserData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
        function (response)
        {
            try
            {
                LoginUser("Facebook", response.id);
            }
            catch(FB){}
        document.cookie = "user=" + '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img class=imageRadius src="' + response.picture.data.url + '"/></p>';
        //document.getElementById('userData').innerHTML = '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img src="' + response.picture.data.url + '"/></p>';
        $("#Login").css("display", "None");
        $("#Register").css("display", "None");      
    });
}

function getregisterData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
function (response) {
    RegisterUser(response.email, response.first_name, response.last_name, 'Facebook', response.id);
});
}

function fbLogout() {
    FB.logout(function () {
        document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="//login.create.net/images/icons/user/facebook_30x30.png"/>';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
        $("#Login").css("display", "inline");
        $("#Register").css("display", "inline");        
    });
    $.removeCookie('cookie', { path: '/' });
    $.removeCookie('user', { path: '/' });
    $.removeCookie('fbm_1539533289441157', { path: '/' });
    $.removeCookie('user', { path: '/Views' });
}

function popupMessage(size, title, message)
{
    bootbox.alert({
        size: size,
        title: title,
        message: message
    });
}

function updateAccountDetails()
{
    
        // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='example'><div class='demo-section k-content'><div style='height:400px;'><div><label class='centreText accountLabel'>First Name</label>\
                                 <input id='firstName' type='text' name='firstName' class='loginBox boxInput'/><br>\
                                <label class='centreText accountLabel'>Last Name</label><input id='lastName' type='text' name='Lastname' class='loginBox boxInput' /><br>\
                                <label class='centreText accountLabel'>User Name</label><input id='UserName' type='text' name='UserName' class='loginBox boxInput'/><label id='usernameError' class='errorTextHide' >UserName Unavailable</label><br>\
                                <label class='centreText accountLabel'>Email</label><input id='Email' type='text' name='Email' class='loginBox boxInput'/><label id='emailError' class='errorTextHide'>User already exists with that email</label><br>\
        <label class='centreText'>No matter if you opt in or opt out You will recieve emails for season Pass reciept,</label><label class='centreText'> change of password and details updated </label><br>\
        <label class='accountLabel' style= 'margin-left:37%; margin-top:3%; margin-bottom:3%;' for='eq1' > Recieve Emails</label > <input type='checkbox' id='eq1' class='k-checkbox' style='margin-left:5%; checked=' checked'><br>\
                                 <br> <input type='button' runat='server' id='UpdateDetails' value='UpdateDetails' onclick='updateDetails()' class='loginBtn k-button accountBtn' /> <br>\
                                 <br><label class='centreText accountLabelHeader'>Change Password</label><br><label class='centreText accountLabel'>New Password</label>\
                                 <input id='newPassword' type='text' name='newPassword' class='loginBox boxInput'/><br>\
                                 <label class='centreText accountLabel'>Confirm Password</label><input id='ConfirmPassword' type='text' name='ConfirmPassword' class='loginBox boxInput'/><br>\
                                <label class='errorTextHide' id='updatePasswordMessage'></label>\
                                 <br><input type='button' runat='server' id='ChangePassword' value='ChangePassword' onclick='changePassword()' class='loginBtn k-button accountBtn'/>\
                                </br></br></div></div>";

        kendoWindow = $("<div id='window'/>").kendoWindow({
            title: "Update Account Details",
            width: "90%",
            height: "90%",
            resizable: false,
            modal: true,
            content: {
                template: tabTemplate
            },
            deactivate: function (e)
            {
                e.sender.destroy();
            }

        }).data("kendoWindow");

        var tab = $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
        });       

        kendoWindow.open().center();
        populateUserDetails(true);
}

function feedback() {
    // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='FeedbackWin' class='redBackground'><div class='demo-section k-content redBackground'><div style='height:400px;' class='redBackground'><div class='redBackground'><label class='centreText accountLabel' style='color:white;'>Tell us about</label>\
                                 <input id='IssueDd' type='text' name='IssueDd' class='loginBox boxInput' style='border-color:white;'/><br>\
                                <label class='centreText accountLabel' style='color:white;'>Feedback</label> <textarea id='tx' rows='25' class='textAreaFeedback centreText' placeholder='Enter your message here .....' ></textarea><br>\
                                <label class='centreText accountLabel' style='color:white'>How would you rate us</label><input id='0to10' name='0to10' class='loginBox boxInput' style='border-color:white;'/></br></br>\
    <button type='button' id='SubmitFeedbackBtn' onclick='submitFeedback()' class='topMenu centreText k-button' style='width:95%;'>Submit</button></div ></div > ";

    kendoWindow = $("<div id='FeedbackWindow'/>").kendoWindow({
        title: "Feedback",
        width: "600px",
        height: "650px",
        resizable: false,
        modal: true,
        content: {
            template: tabTemplate
        }

    }).data("kendoWindow");

    kendoWindow.open().center();

    var IssueData = [
        { text: "Something I Like", value: "1" },
        { text: "Something that could be better", value: "2" },
        { text: "Technichal issue ", value: "3" },
        { text: "Something Different", value: "4" }
    ];

    $("#IssueDd").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: IssueData,
        index: 0,
        change: onchange
    });

    var zeroToTenData = [
        { text: "0", value: "1" },
        { text: "1", value: "2" },
        { text: "2", value: "3" },
        { text: "3", value: "4" },
        { text: "4", value: "5" },
        { text: "5", value: "6" },
        { text: "6", value: "7" },
        { text: "7", value: "8" },
        { text: "8", value: "9" },
        { text: "9", value: "10" },
        { text: "10", value: "11" }        
    ];

    $("#0to10").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: zeroToTenData,
        index: 6,
        change: onchange
    });
}

function submitFeedback() {

    var type = $("#IssueDd").data("kendoDropDownList").text();
    var rating = $("#0to10").data("kendoDropDownList").text();
    var message = $("#tx").val().replace(/(\r\n|\n|\r)/gm, '<br>');

    emailNotification("feedback@boxing5.com", "Boxing5", "feedback@boxing5.com", "Feedback", "type:" +type + "<br> rating:" + rating + "<br>Message:" + message);

    popupMessage("large", "Feedback Left", "Thank you for your feedback. It is very much appreciated.");
    var mywindow = $("#FeedbackWindow").data("kendoWindow");
    mywindow.close();

}

function changePassword(){
    //var code = $("#ResetCode").val();
    var user = parserId();
    var pw = $("#newPassword").val();
    var pw1 = $("#ConfirmPassword").val();
if (pw != pw1)
{
    $("#updatePasswordMessage")[0].innerText = "Passwords Do Not Match";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
    
    return false;
}
if (pw.length < 8)
{
    $("#updatePasswordMessage")[0].innerText = "Password must be atleast 8 characters long.";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
  
    return false;
}
$.ajax({
    type: "post",
    data: JSON.stringify({
       // code: code,
        user:user,
        password: pw
    }),
    url: "/Views/results.aspx/UpdateUserpassword",
    dataType: "json",
    async: false,
    contentType: "application/json",
    success: function (object)
    {
        var data = populateUserDetails(false);
        popupMessage("large", "Password Changed", "Your Password has been changed");
        emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Password Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Password has been updated");
    },
    complete: function (object)
    {
        var mywindow = $("#window").data("kendoWindow");
        mywindow.close();
    },
    error: function (object)
    {
    }
    });
 }

function updateDetails()
{   
    var user = parserId();
    var data;
    var firstName = $("#firstName").val();
    var LastName = $("#lastName").val();
    var userName = $("#UserName").val();
    var email = $("#Email").val();
    var Emailval = validateEmail(email);
    var checkbox = $("#eq1").is(':checked');
    
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    var returnData;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, f_name: firstName, l_name: LastName, userName: userName, email: email, emailSub: checkbox
        }),
        url: "/Views/results.aspx/updateUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {     
            returnData = object.d;
            if (returnData == 101)
            {
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");          
            }
            if (returnData == 201)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");        
            }
            if (returnData == 301)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");                
            }
            if (returnData == 0)
            {
                data = populateUserDetails(false);
                popupMessage("large", "details Changed", "Your Details have been changed");
                var email = CheckEmail(user);
                if (email == 1)
                {
                    emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Details Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Details have been updated");
                }
            }
        },
        complete: function ()
        {
            populateUserDetails(true);   
            var mywindow = $("#window").data("kendoWindow");
            mywindow.close();
            document.cookie = "user=" + '<p>' + data[0].FirstName + ' ' + data[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
            var cookie = $.cookie('user');
            $("#userData").html(cookie);
            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  
}

function populateUserDetails(pop)
{
    var user = parserId();
    
    var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                user: user
            }),
            url: "/Views/results.aspx/getUserDetails",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                data = object.d;
            },
            complete: function ()
            {
                if (pop == true)
                {
                    $("#firstName")[0].value = data[0].FirstName;
                    $("#lastName")[0].value = data[0].LastName;
                    $("#UserName")[0].value = data[0].UserName;
                    $("#Email")[0].value = data[0].Email;
                    $("#eq1").prop("checked",data[0].emailSub);
                }
            },
            error: function (object)
            {
                console.log("error" + object);
            }
    });
        return data;       
}

function CheckEmail(userid)
{
    var returned;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            userId: userid
        }),
        url: "/Index.aspx/checkEmail",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returned = object.d;
        },
        complete: function ()
        {
           
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return returned;
}

function CheckSeasonForUSer() {
    
    var user = parserId();
    if (user == -1)
    {
        return false;
    }
    var season;
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Views/results.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {
            try
            {
                season = data[0].season;
            }
            catch (season)
            {
                if (season == undefined)
                {
                    season = 0;
                }}
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return season;
}

function validateEmail(emailField)
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false)
    {
        return false;
    }
    return true;
}

function emailNotification(from,fromAlias,to,subject,mailBody)
{
    $.ajax({
        type: "post",
        data: JSON.stringify({
            from: from, fromAlias: fromAlias, to: to, subject: subject, mailBody: mailBody
        }),
        url: "/Index.aspx/mailNotification",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {           
        },
        complete: function ()
        {            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  
}

function checkSeasonPass(user)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Index.aspx/checkSeasonPass",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return data;
}

function getParameterByName(name, url)
{
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function nth(n) { return ["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th" }
$(document).ready(function () {

    $("#createLeagueBtn").kendoButton({
        click: function() {
            createLeague();
        }
    });
});

function createLeague() {
  
    var leagueName = $("#createLeagueInput").val();
    var user = parserId();

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, name:leagueName
        }),
        url: "/Views/Leagues.aspx/createLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function () {
           
        },
        complete: function ()
        {
            var data = populateUserDetails(false);
            var email = CheckEmail(user);
            if (email == 1)
            {
                emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Created ", "<h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
                    "You created League - " + leagueName + "<br> Dont Forget to invite people to join your league. <br> Have Fun!");
            }
            window.location.href = 'Leagues.aspx';

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

   
}


$(document).ready(function ()
{
    var seasonandWeek = getCurrentSeasonAndWeek();
    $("#HeaderSeasonandWeek").text("Season: " + seasonandWeek[0].season + "Week: " + seasonandWeek[0].week);
 
    $("#checkName").click(function ()
    {
        checkBoxer();
    });

    $("#insertBoxer").click(function ()
    {
        insertBoxer();
    });
    $("#UpdateBoxer").click(function ()
    {
        updateBoxer();
    });
    $("#insertBout").click(function ()
    {
        insertBout();
    });
    $("#updateBout").click(function ()                                  
    {
        insertResultsForBout();
    });
    $("#GetBoxersBtn").click(function ()
    {
        getBoxersForBout();
    });

    $("#lastUpdatedScores").click(function () {

        checkLastDateScore();
    })

    $("#UpdateScores").click(function ()
    {
        updateScores();
    })

    $("#getCurrentSeasonAndWeek").click(function () {
        getCurrentSeasonAndWeek();
    })

    $("#SetSeasonandWeek").click(function () {
        
        var season = $("#adminSeason").val();
        var week = $("#adminWeek").val();

        UpdateSeasonAndWeek(season, week);
    })

    $("#SendSeasonPassEmails").click(function ()
    {
        sendSeasonPassEmails();
    })
});

function UpdateSeasonAndWeek(season,week) {

    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: season,
            week:week
        }),
        url: "/Views/Admin.aspx/updateSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {

            $("#seasonAndWeekUpdatedLabel").text("Season and Week Updated");
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
            $("#seasonAndWeekUpdatedLabel").text("Season and Week NOT Updated");
        }
    });
}

function getCurrentSeasonAndWeek() {

    var data;

    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Admin.aspx/getActiveSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;

            $("#CurrentSeasonAndWeekLabel").text("Season: " + data[0].season + " Week: " + data[0].week);
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function checkLastDateScore() {

    $.ajax({
        type: "post",
        data: JSON.stringify({            
        }),
        url: "/Views/Admin.aspx/checkLastUpdated",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
            $("#lastUpdatedScoresLabel").text(data);
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

function updateScores() {

    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Admin.aspx/UpdateScores",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            $("#scoreUpdatedLabel").text("Scores Updated");
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
            $("#scoreUpdatedLabel").text("ERROR **Scores Not Updated**");
        }
    });
}

function checkBoxer()
{
    var boxerName = $("#addName").val();
    var div = $("#addDiv").val();

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            boxer: boxerName, div:div
        }),
        url: "/Views/Admin.aspx/checkboxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            if (data >= 1)
            {                
                var BoxerData = populateAddBoxer(boxerName,div);
                $("#checknameLabel").text("Boxer already exists With ID -" + BoxerData[0].Id);

                var image = $("#addImage").val(BoxerData[0].image);
                var nationality = $("#addNationality").val(BoxerData[0].nationality);
                var nickname = $("#addNickname").val(BoxerData[0].nickname);
                var Stance = $("#addStance").val(BoxerData[0].stance);
                var height = $("#addheight").val(BoxerData[0].height);
                var reach = $("#addReach").val(BoxerData[0].reach);
                var residence = $("#addresidence").val(BoxerData[0].residence);
                var age = $("#addage").val(BoxerData[0].age);
                var division = $("#addDivision").val(BoxerData[0].DivId);
                var won = $("#addWon").val(BoxerData[0].won);
                var lost = $("#addLost").val(BoxerData[0].lost);
                var drawn = $("#addDrawn").val(BoxerData[0].drawn);
                var Ko = $("#addKo").val(BoxerData[0].ko);
                var IBF = $("#addIBF").val(BoxerData[0].IBF);
                var WBC = $("#addWBC").val(BoxerData[0].WBC);
                var IBO = $("#addIBO").val(BoxerData[0].IBO);
                var WBA = $("#addWBA").val(BoxerData[0].WBA);
                var WBO = $("#addWBO").val(BoxerData[0].WBO);
                var Last1 = $("#addlast1").val(BoxerData[0].last1);
                var Last2 = $("#addLast2").val(BoxerData[0].last2);
                var Last3 = $("#addLast3").val(BoxerData[0].last3);
                var Last4 = $("#addLast4").val(BoxerData[0].last4);
                var Last5 = $("#addLast5").val(BoxerData[0].last5);
                var Rounds = $("#addRounds").val(BoxerData[0].Rounds);
                var Bouts = $("#addBouts").val(BoxerData[0].Bouts);
            }
            else
            {
                $("#checknameLabel").text("Boxer is not in the database");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return count;
}

function sendSeasonPassEmails()
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getSeasonPassLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;

            for (var i = 0; i < data.length; i++)
            {
                emailNotification("forgotpassword@boxing5.com", "Boxing5", data[i].email, "Leaderboard Update", data[i].name + ",</br> Your Current Score is = " + data[i].score + "</br> Your Current Leaderboard Place is = " + data[i].position + nth(data[i].position));
            }

        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });    
}

function getBoxersForBout()
{
    var Season = $("#addResultSeason").val();
    Season = returnNull(Season);
    var Week = $("#addResultWeek").val();
    Week = returnNull(Week);
    var bout = $("#addResultBoutNumber").val();
    bout = returnNull(bout);

    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout
        }),
        url: "/Views/Selection.aspx/getBoxersForSWB",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            $("#getBoxerLabel").text("Boxer 1 ID = " + data[0].boxer1Id + " Name = " + data[0].boxerName + " | Boxer 2 Id = " + data[0].boxer2Id + "Name = " + data[0].boxer2Name);
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

function populateAddBoxer(Name,div)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            Name: Name, div:div
        }),
        url: "/Views/Selection.aspx/GetBoxerDetailsByName",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

function insertBoxer()
{
    var returnedValue = 0;

    var name = $("#addName").val();
    name = returnNull(name);
    var image = $("#addImage").val();
    image = returnNull(image);
    var nationality = $("#addNationality").val();
    nationality = returnNull(nationality);
    var nickname = $("#addNickname").val();
    nickname = returnNull(nickname);
    var Stance = $("#addStance").val();
    Stance = returnNull(Stance); 
    var height = $("#addheight").val();
    height = returnNull(height);
    var reach = $("#addReach").val();
    reach = returnNull(reach);
    var residence = $("#addresidence").val();
    residence = returnNull(residence);
    var age = $("#addage").val();
    age = returnNull(age);
    var division = $("#addDivision").val();
    division = returnNull(division);
    var won = $("#addWon").val();
    won = returnNull(won);
    var lost = $("#addLost").val();
    lost = returnNull(lost);
    var drawn = $("#addDrawn").val();
    drawn = returnNull(drawn);
    var Ko = $("#addKo").val();
    Ko = returnNull(Ko);
    var IBF = $("#addIBF").val();
    IBF = returnNull(IBF);
    var WBC = $("#addWBC").val();
    WBC = returnNull(WBC);
    var IBO = $("#addIBO").val();
    IBO = returnNull(IBO);
    var WBA = $("#addWBA").val();
    WBA = returnNull(WBA);
    var WBO = $("#addWBO").val();
    WBO = returnNull(WBO);
    var Last1 = $("#addlast1").val();
    Last1 = returnNull(Last1);
    var Last2 = $("#addLast2").val();
    Last2 = returnNull(Last2);
    var Last3 = $("#addLast3").val();
    Last3 = returnNull(Last3);
    var Last4 = $("#addLast4").val();
    Last4 = returnNull(Last4);
    var Last5 = $("#addLast5").val();
    Last5 = returnNull(Last5);
    var rounds = $("#addRounds").val();
    rounds = returnNull(rounds);
    var bouts = $("#addBouts").val();
    bouts = returnNull(bouts);

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            name: name,
            image: image,
            nationality: nationality,
            nickname: nickname,
            stance: Stance,
            height: height,
            reach: reach,
            residence: residence,
            age: age,
            division: division,
            won: won,
            lost: lost,
            drawn: drawn,
            ko: Ko,
            ibf: IBF,
            wbc: WBC,
            ibo: IBO,
            wba: WBA,
            wbo: WBO,
            last1: Last1,
            last2: Last2,
            last3: Last3,
            last4: Last4,
            last5: Last5,
            rounds: rounds,
            bouts:bouts,
        }),
        url: "/Views/Admin.aspx/insertBoxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
                $("#insertBoxerLabel").text("boxer not added");
            }
            else if (returnedValue > 0)
            {
                $("#insertBoxerLabel").text("boxer added");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    }); 

    return returnedValue;
}

function insertBout()
{
    var returnedValue = 0;

    var Season = $("#addSeason").val();
    Season = returnNull(Season);
    var Week = $("#addWeek").val();
    Week = returnNull(Week);
    var bout = $("#addBoutNumber").val();
    bout = returnNull(bout);
    var Boxer1 = $("#addBoxer1").val();
    Boxer1 = returnNull(Boxer1);
    var Boxer2 = $("#addBoxer2").val();
    Boxer2 = returnNull(Boxer2);
    var Rounds = $("#Rounds").val();
    Rounds = returnNull(Rounds);
    var Date = $("#addDate").val();
    Date = returnNull(Date);
    var Location = $("#addLocation").val();
    Location = returnNull(Location);
    var boutDivision = $("#addBoutDiv").val();
    boutDivision = returnNull(boutDivision);
    var televised = $("#addTelevised").val();
    televised = returnNull(televised);
    var title = $("#addTitle").val();
    title = returnNull(title);    

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout,
            boxer1: Boxer1,
            boxer2: Boxer2,
            rounds: Rounds,
            date: Date,
            location: Location,
            division: boutDivision,
            televised: televised,
            title: title,          
        }),
        url: "/Views/Admin.aspx/insertBout",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
               
            }
            else if (returnedValue > 0)
            {
                
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return returnedValue;
}

function insertResultsForBout()
{
    var returnedValue = 0;

    var Season = $("#addResultSeason").val();
    Season = returnNull(Season);
    var Week = $("#addResultWeek").val();
    Week = returnNull(Week);
    var bout = $("#addResultBoutNumber").val();
    bout = returnNull(bout);
    var Outcome = $("#addOutcome").val();
    Outcome = returnNull(Outcome);
    var Round = $("#addWinnerRound").val();
    Round = returnNull(Round);
    var BoxerWon = $("#addBoxerWon").val();
    BoxerWon = returnNull(BoxerWon);
    var BoxerLost = $("#addBoxerLost").val();
    BoxerLost = returnNull(BoxerLost);
    var TieBreaker = $("#addTieBreaker").val();
    TieBreaker = returnNull(TieBreaker); 

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout,
            Outcome: Outcome,
            Round: Round,
            BoxerLost: BoxerLost,
            BoxerWon: BoxerWon,
            TieBreaker: TieBreaker, 
        }),
        url: "/Views/Admin.aspx/insertResultsBout",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {

            }
            else if (returnedValue > 0)
            {

            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returnedValue;
}

function returnNull(string)
{
    if (string == "")
        return null;
    else
        return string;
}

function updateBoxer()
{
    var returnedValue = 0;

    var image = $("#addImage").val();
    image = returnNull(image);

    var Div = $("#addDiv").val();
    Div = returnNull(Div);   

    var name = $("#addName").val();
    name = returnNull(name);   
    var age = $("#addage").val();
    age = returnNull(age);   
    var won = $("#addWon").val();
    won = returnNull(won);
    var lost = $("#addLost").val();
    lost = returnNull(lost);
    var drawn = $("#addDrawn").val();
    drawn = returnNull(drawn);
    var Ko = $("#addKo").val();
    Ko = returnNull(Ko);
    var IBF = $("#addIBF").val();
    IBF = returnNull(IBF);
    var WBC = $("#addWBC").val();
    WBC = returnNull(WBC);
    var IBO = $("#addIBO").val();
    IBO = returnNull(IBO);
    var WBA = $("#addWBA").val();
    WBA = returnNull(WBA);
    var WBO = $("#addWBO").val();
    WBO = returnNull(WBO);
    var Last1 = $("#addlast1").val();
    Last1 = returnNull(Last1);
    var Last2 = $("#addLast2").val();
    Last2 = returnNull(Last2);
    var Last3 = $("#addLast3").val();
    Last3 = returnNull(Last3);
    var Last4 = $("#addLast4").val();
    Last4 = returnNull(Last4);
    var Last5 = $("#addLast5").val();
    Last5 = returnNull(Last5);
    var rounds = $("#addRounds").val();
    rounds = returnNull(rounds);
    var bouts = $("#addBouts").val();
    bouts = returnNull(bouts);

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            image:image,
            Div:Div,
            name: name,           
            age: age,           
            won: won,
            lost: lost,
            drawn: drawn,
            ko: Ko,
            ibf: IBF,
            wbc: WBC,
            ibo: IBO,
            wba: WBA,
            wbo: WBO,
            last1: Last1,
            last2: Last2,
            last3: Last3,
            last4: Last4,
            last5: Last5,
            rounds: rounds,
            bouts: bouts,
        }),
        url: "/Views/Admin.aspx/updateBoxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
                $("#insertBoxerLabel").text("boxer not added");
            }
            else if (returnedValue > 0)
            {
                $("#insertBoxerLabel").text("boxer added");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returnedValue;
}
(function ($)
{
    $(document).ready(function ()
    {
        $('#cssmenu').prepend('<div id="menu-button">Menu</div>');
        $('#cssmenu #menu-button').on('click', function ()
        {
            var menu = $(this).next('ul');
            if (menu.hasClass('open'))
            {
                menu.removeClass('open');
            }
            else
            {
                menu.addClass('open');
            }
        });
    });
})(jQuery);

$(document).ready(function ()
{    
    $(document).keypress(function (e)
    {
        if (e.which == 13)
        {
            if ($("#RegisterPasswordInput").is(":focus"))
            {
                LoginUser(null,null);}
            }
    });   

    fixtures(2);


    var user = parserId(); 

    if (user == 1)
    {
        $("#Admin").css("display", "block");
    }

    $("#Knives").kendoButton({
        click: function () {
            window.location.href = "/Views/swiftKnivesuk.aspx";
        }
    });

    $("#Feedback").click(function(){
       
            feedback();
        
    });

    $("#updateAccountDetails").click(function(){
        
            updateAccountDetails();
        
    });  
    
    $("#Terms").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Terms.aspx";
        }
    });

    $("#Privacy").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Privacy.aspx";
        }
    });

    $("#Unsubscribe").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Unsubscribe.aspx";
        }
    });
    
    $("#SeasonPassLeaderboard").click(function(){
        
            window.location.href = "/Views/SeasonPassLeaderboard.aspx";
        
    });

    $("#HomeRegisterBtn").click(function(){
      
            window.location.href = "Views/Register.aspx";
        
    });

    

    $("#RegisterSigninBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#LoginRegisterBtn").kendoButton({
        click: function () {
            window.location.href = "/Views/Register.aspx";
        }
    });

   
    $("#HomeLoginBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Home").click(function(){
       
            window.location.href = "/Index.aspx";
            var user = parserId();
       
    });

    $("#Admin").click(function ()
    {        
        window.location.href = "/Views/Admin.aspx?LC=b5105";  

    });

    $("#SeasonPass").click(function(){       
            window.location.href = "/Views/SeasonPass.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#rankings").click(function(){
       
            window.location.href = "/Views/rankings.aspx";
            var user = parserId();

            if (user === -1) {
                window.location.href = "/Login.aspx";
            }
        
    });

    $("#Selection").click(function(){       
            window.location.href = "/Views/Selection.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leaderboard").click(function(){       
            window.location.href = "/Views/Leaderboard.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leagues").click(function(){       
            window.location.href = "/Views/Leagues.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });


    $("#Register").click(function(){
       
            window.location.href = "/Views/Register.aspx";
        
    });

    $("#About").click(function () {

        window.location.href = "/Views/about.aspx";

    });

    $("#Login").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Results").click(function(){       
            window.location.href = "/Views/Results.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#logOut").click(function(){    

           
            window.location.href = "/Index.aspx";
            fbLogout();
        
    });

    var cookie = $.cookie('user');  

    $("#userData").html(cookie);

    if (cookie == undefined)
    {       
        try {
            $("#account")[0].style.display = "none";
            $("#userData").html("");
           
            
            $("#logOut")[0].style.display = "none";
            $("#updateAccountDetails")[0].style.display = "none";
           // $("#SeasonPass").css("display", "none");
          //  $("#SeasonPassLeaderboard").css("display", "none");
            $("#HomeRegisterBtn").css("display", "initial");
            $("#HomeLoginBtn").css("display", "initial");
             $("#Login")[0].style.display = "block"
            $("#Register")[0].style.display = "block";
        }
        catch (test) {}
    }
    else
    {
        $("#account")[0].style.display = "block";
      
        $("#logOut").css("display", "block");
        $("#updateAccountDetails").css("display", "block");
       // $("#SeasonPass").css("display", "block");
        $("#HomeRegisterBtn").css("display", "none");
        $("#HomeLoginBtn").css("display", "none");
       // $("#SeasonPassLeaderboard").css("display", "none");
          $("#Login").css("display", "none");
       $("#Register").css("display", "none");
    }
    var season = CheckSeasonForUSer();
    //if (season == true)
    //{
    //    $("#SeasonPass").css("display", "none");
    //    $("#seasonLabel")[0].innerText = "Season Pass Active";
    //    $("#SeasonPassLeaderboard").css("display", "block");

    //}


});

function parserId() {
    var userID1 = $.cookie("cookie");
    try {
       
        if (userID1 == undefined) {
            userID1 = -1;
        }
        else {
            var cookieDec = window.atob(userID1);
            userID1 = parseInt(cookieDec);
        }
    }
    catch (id) { }
    return userID1;
}

window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '1539533289441157', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    //// Check whether the user already logged in
    //FB.getLoginStatus(function (response) {
    //    if (response.status === 'connected') {
    //        //display user data
    //        getFbUserData();
    //    }
    //});
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function RegisterFb() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getregisterData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function getFbUserData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
        function (response)
        {
            try
            {
                LoginUser("Facebook", response.id);
            }
            catch(FB){}
        document.cookie = "user=" + '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img class=imageRadius src="' + response.picture.data.url + '"/></p>';
        //document.getElementById('userData').innerHTML = '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img src="' + response.picture.data.url + '"/></p>';
        $("#Login").css("display", "None");
        $("#Register").css("display", "None");      
    });
}

function getregisterData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
function (response) {
    RegisterUser(response.email, response.first_name, response.last_name, 'Facebook', response.id);
});
}

function fbLogout() {
    FB.logout(function () {
        document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="//login.create.net/images/icons/user/facebook_30x30.png"/>';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
        $("#Login").css("display", "inline");
        $("#Register").css("display", "inline");        
    });
    $.removeCookie('cookie', { path: '/' });
    $.removeCookie('user', { path: '/' });
    $.removeCookie('fbm_1539533289441157', { path: '/' });
    $.removeCookie('user', { path: '/Views' });
}

function popupMessage(size, title, message)
{
    bootbox.alert({
        size: size,
        title: title,
        message: message
    });
}

function updateAccountDetails()
{
    
        // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='example'><div class='demo-section k-content'><div style='height:400px;'><div><label class='centreText accountLabel'>First Name</label>\
                                 <input id='firstName' type='text' name='firstName' class='loginBox boxInput'/><br>\
                                <label class='centreText accountLabel'>Last Name</label><input id='lastName' type='text' name='Lastname' class='loginBox boxInput' /><br>\
                                <label class='centreText accountLabel'>User Name</label><input id='UserName' type='text' name='UserName' class='loginBox boxInput'/><label id='usernameError' class='errorTextHide' >UserName Unavailable</label><br>\
                                <label class='centreText accountLabel'>Email</label><input id='Email' type='text' name='Email' class='loginBox boxInput'/><label id='emailError' class='errorTextHide'>User already exists with that email</label><br>\
        <label class='centreText'>No matter if you opt in or opt out You will recieve emails for season Pass reciept,</label><label class='centreText'> change of password and details updated </label><br>\
        <label class='accountLabel' style= 'margin-left:37%; margin-top:3%; margin-bottom:3%;' for='eq1' > Recieve Emails</label > <input type='checkbox' id='eq1' class='k-checkbox' style='margin-left:5%; checked=' checked'><br>\
                                 <br> <input type='button' runat='server' id='UpdateDetails' value='UpdateDetails' onclick='updateDetails()' class='loginBtn k-button accountBtn' /> <br>\
                                 <br><label class='centreText accountLabelHeader'>Change Password</label><br><label class='centreText accountLabel'>New Password</label>\
                                 <input id='newPassword' type='text' name='newPassword' class='loginBox boxInput'/><br>\
                                 <label class='centreText accountLabel'>Confirm Password</label><input id='ConfirmPassword' type='text' name='ConfirmPassword' class='loginBox boxInput'/><br>\
                                <label class='errorTextHide' id='updatePasswordMessage'></label>\
                                 <br><input type='button' runat='server' id='ChangePassword' value='ChangePassword' onclick='changePassword()' class='loginBtn k-button accountBtn'/>\
                                </br></br></div></div>";

        kendoWindow = $("<div id='window'/>").kendoWindow({
            title: "Update Account Details",
            width: "90%",
            height: "90%",
            resizable: false,
            modal: true,
            content: {
                template: tabTemplate
            },
            deactivate: function (e)
            {
                e.sender.destroy();
            }

        }).data("kendoWindow");

        var tab = $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
        });       

        kendoWindow.open().center();

        populateUserDetails(true);
}

function feedback() {
    // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='FeedbackWin' class='redBackground'><div class='demo-section k-content redBackground'><div style='height:400px;' class='redBackground'><div class='redBackground'><label class='centreText accountLabel' style='color:white;'>Tell us about</label>\
                                 <input id='IssueDd' type='text' name='IssueDd' class='loginBox boxInput' style='border-color:white;'/><br>\
                                <label class='centreText accountLabel' style='color:white;'>Feedback</label> <textarea id='tx' rows='25' class='textAreaFeedback centreText' placeholder='Enter your message here .....' ></textarea><br>\
                                <label class='centreText accountLabel' style='color:white'>How would you rate us</label><input id='0to10' name='0to10' class='loginBox boxInput' style='border-color:white;'/></br></br>\
    <button type='button' id='SubmitFeedbackBtn' onclick='submitFeedback()' class='topMenu centreText k-button' style='width:95%;'>Submit</button></div ></div > ";

    kendoWindow = $("<div id='FeedbackWindow'/>").kendoWindow({
        title: "Feedback",
        width: "90%",
        height: "90%",
        resizable: false,
        modal: true,
        content: {
            template: tabTemplate
        }

    }).data("kendoWindow");

    kendoWindow.open().center();

    var IssueData = [
        { text: "Something I Like", value: "1" },
        { text: "Something that could be better", value: "2" },
        { text: "Technichal issue ", value: "3" },
        { text: "Something Different", value: "4" }
    ];

    $("#IssueDd").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: IssueData,
        index: 0,
        change: onchange

    });

    var zeroToTenData = [
        { text: "0", value: "1" },
        { text: "1", value: "2" },
        { text: "2", value: "3" },
        { text: "3", value: "4" },
        { text: "4", value: "5" },
        { text: "5", value: "6" },
        { text: "6", value: "7" },
        { text: "7", value: "8" },
        { text: "8", value: "9" },
        { text: "9", value: "10" },
        { text: "10", value: "11" }
        
    ];

    $("#0to10").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: zeroToTenData,
        index: 6,
        change: onchange

    });
}

function submitFeedback() {

    var type = $("#IssueDd").data("kendoDropDownList").text();
    var rating = $("#0to10").data("kendoDropDownList").text();
    var message = $("#tx").val().replace(/(\r\n|\n|\r)/gm, '<br>');

    emailNotification("feedback@boxing5.com", "Boxing5", "feedback@boxing5.com", "Feedback", "type:" +type + "<br> rating:" + rating + "<br>Message:" + message);

    popupMessage("large", "Feedback Left", "Thank you for your feedback. It is very much appreciated.");
    var mywindow = $("#FeedbackWindow").data("kendoWindow");
    mywindow.close();

}

function changePassword(){
    //var code = $("#ResetCode").val();
    var user = parserId();
    var pw = $("#newPassword").val();
    var pw1 = $("#ConfirmPassword").val();
if (pw != pw1)
{
    $("#updatePasswordMessage")[0].innerText = "Passwords Do Not Match";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
    
    return false;
}
if (pw.length < 8)
{
    $("#updatePasswordMessage")[0].innerText = "Password must be atleast 8 characters long.";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
  
    return false;
}
$.ajax({
    type: "post",
    data: JSON.stringify({
       // code: code,
        user:user,
        password: pw
    }),
    url: "/Views/results.aspx/UpdateUserpassword",
    dataType: "json",
    async: false,
    contentType: "application/json",
    success: function (object)
    {
        var data = populateUserDetails(false);
        popupMessage("large", "Password Changed", "Your Password has been changed");
        emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Password Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Password has been updated");

    },
    complete: function (object)
    {
        var mywindow = $("#window").data("kendoWindow");
        mywindow.close();
    },
    error: function (object)
    {
    }
    });
}
function updateDetails()
{   
    var user = parserId();
    var data;
    var firstName = $("#firstName").val();
    var LastName = $("#lastName").val();
    var userName = $("#UserName").val();
    var email = $("#Email").val();
    var Emailval = validateEmail(email);
    var checkbox = $("#eq1").is(':checked');
    
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    var returnData;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, f_name: firstName, l_name: LastName, userName: userName, email: email, emailSub: checkbox
        }),
        url: "/Views/results.aspx/updateUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {     
            returnData = object.d;
            if (returnData == 101)
            {
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");
          
            }
            if (returnData == 201)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");
        
            }
            if (returnData == 301)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");                
            }
            if (returnData == 0)
            {
                data = populateUserDetails(false);
                popupMessage("large", "details Changed", "Your Details have been changed");
                var email = CheckEmail(user);
                if (email == 1)
                {
                    emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Details Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Details have been updated");
                }
            }
        },
        complete: function ()
        {
            populateUserDetails(true);   
            var mywindow = $("#window").data("kendoWindow");
            mywindow.close();
            document.cookie = "user=" + '<p>' + data[0].FirstName + ' ' + data[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
            var cookie = $.cookie('user');
            $("#userData").html(cookie);
            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  
}

function populateUserDetails(pop)
{
    var user = parserId();
    
    var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                user: user
            }),
            url: "/Views/results.aspx/getUserDetails",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                data = object.d;
            },
            complete: function ()
            {
                if (pop == true)
                {
                    $("#firstName")[0].value = data[0].FirstName;
                    $("#lastName")[0].value = data[0].LastName;
                    $("#UserName")[0].value = data[0].UserName;
                    $("#Email")[0].value = data[0].Email;
                    $("#eq1").prop("checked",data[0].emailSub);
                }
            },
            error: function (object)
            {
                console.log("error" + object);
            }
    });

        return data;
       
}

function CheckEmail(userid)
{
    var returned;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            userId: userid
        }),
        url: "/Index.aspx/checkEmail",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returned = object.d;
        },
        complete: function ()
        {
           
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returned;
}




function CheckSeasonForUSer() {
    
    var user = parserId();
    if (user == -1)
    {
        return false;
    }
    var season;
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Views/results.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {
            try
            {
                season = data[0].season;
            }
            catch (season)
            {
                if (season == undefined)
                {
                    season = 0;
                }}
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return season;
}

function validateEmail(emailField)
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false)
    {
        return false;
    }
    return true;
}

function emailNotification(from,fromAlias,to,subject,mailBody)
{
    $.ajax({
        type: "post",
        data: JSON.stringify({
            from: from, fromAlias: fromAlias, to: to, subject: subject, mailBody: mailBody
        }),
        url: "/Index.aspx/mailNotification",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {           
        },
        complete: function ()
        {            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  

}

function checkSeasonPass(user)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Index.aspx/checkSeasonPass",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;

}

function getParameterByName(name, url)
{
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function nth(n) { return ["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th" }