﻿$(document).ready(function ()
{
    var seasonandWeek = getCurrentSeasonAndWeek();
    $("#HeaderSeasonandWeek").text("Season: " + seasonandWeek[0].season + "Week: " + seasonandWeek[0].week);
 
    $("#checkName").click(function ()
    {
        checkBoxer();
    });

    $("#insertBoxer").click(function ()
    {
        insertBoxer();
    });
    $("#UpdateBoxer").click(function ()
    {
        updateBoxer();
    });
    $("#insertBout").click(function ()
    {
        insertBout();
    });
    $("#updateBout").click(function ()                                  
    {
        insertResultsForBout();
    });
    $("#GetBoxersBtn").click(function ()
    {
        getBoxersForBout();
    });

    $("#lastUpdatedScores").click(function () {

        checkLastDateScore();
    })

    $("#UpdateScores").click(function ()
    {
        updateScores();
    })

    $("#getCurrentSeasonAndWeek").click(function () {
        getCurrentSeasonAndWeek();
    })

    $("#SetSeasonandWeek").click(function () {
        
        var season = $("#adminSeason").val();
        var week = $("#adminWeek").val();

        UpdateSeasonAndWeek(season, week);
    })

    $("#SendSeasonPassEmails").click(function ()
    {
        sendSeasonPassEmails();
    })
});

function UpdateSeasonAndWeek(season,week) {

    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: season,
            week:week
        }),
        url: "/Views/Admin.aspx/updateSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {

            $("#seasonAndWeekUpdatedLabel").text("Season and Week Updated");
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
            $("#seasonAndWeekUpdatedLabel").text("Season and Week NOT Updated");
        }
    });
}

function getCurrentSeasonAndWeek() {

    var data;

    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Admin.aspx/getActiveSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;

            $("#CurrentSeasonAndWeekLabel").text("Season: " + data[0].season + " Week: " + data[0].week);
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function checkLastDateScore() {

    $.ajax({
        type: "post",
        data: JSON.stringify({            
        }),
        url: "/Views/Admin.aspx/checkLastUpdated",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
            $("#lastUpdatedScoresLabel").text(data);
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

function updateScores() {

    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Admin.aspx/UpdateScores",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            $("#scoreUpdatedLabel").text("Scores Updated");
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
            $("#scoreUpdatedLabel").text("ERROR **Scores Not Updated**");
        }
    });
}

function checkBoxer()
{
    var boxerName = $("#addName").val();
    var div = $("#addDiv").val();

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            boxer: boxerName, div:div
        }),
        url: "/Views/Admin.aspx/checkboxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            if (data >= 1)
            {                
                var BoxerData = populateAddBoxer(boxerName,div);
                $("#checknameLabel").text("Boxer already exists With ID -" + BoxerData[0].Id);

                var image = $("#addImage").val(BoxerData[0].image);
                var nationality = $("#addNationality").val(BoxerData[0].nationality);
                var nickname = $("#addNickname").val(BoxerData[0].nickname);
                var Stance = $("#addStance").val(BoxerData[0].stance);
                var height = $("#addheight").val(BoxerData[0].height);
                var reach = $("#addReach").val(BoxerData[0].reach);
                var residence = $("#addresidence").val(BoxerData[0].residence);
                var age = $("#addage").val(BoxerData[0].age);
                var division = $("#addDivision").val(BoxerData[0].DivId);
                var won = $("#addWon").val(BoxerData[0].won);
                var lost = $("#addLost").val(BoxerData[0].lost);
                var drawn = $("#addDrawn").val(BoxerData[0].drawn);
                var Ko = $("#addKo").val(BoxerData[0].ko);
                var IBF = $("#addIBF").val(BoxerData[0].IBF);
                var WBC = $("#addWBC").val(BoxerData[0].WBC);
                var IBO = $("#addIBO").val(BoxerData[0].IBO);
                var WBA = $("#addWBA").val(BoxerData[0].WBA);
                var WBO = $("#addWBO").val(BoxerData[0].WBO);
                var Last1 = $("#addlast1").val(BoxerData[0].last1);
                var Last2 = $("#addLast2").val(BoxerData[0].last2);
                var Last3 = $("#addLast3").val(BoxerData[0].last3);
                var Last4 = $("#addLast4").val(BoxerData[0].last4);
                var Last5 = $("#addLast5").val(BoxerData[0].last5);
                var Rounds = $("#addRounds").val(BoxerData[0].Rounds);
                var Bouts = $("#addBouts").val(BoxerData[0].Bouts);
            }
            else
            {
                $("#checknameLabel").text("Boxer is not in the database");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return count;
}

function sendSeasonPassEmails()
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getSeasonPassLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;

            for (var i = 0; i < data.length; i++)
            {
                emailNotification("forgotpassword@boxing5.com", "Boxing5", data[i].email, "Leaderboard Update", data[i].name + ",</br> Your Current Score is = " + data[i].score + "</br> Your Current Leaderboard Place is = " + data[i].position + nth(data[i].position));
            }

        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });    
}

function getBoxersForBout()
{
    var Season = $("#addResultSeason").val();
    Season = returnNull(Season);
    var Week = $("#addResultWeek").val();
    Week = returnNull(Week);
    var bout = $("#addResultBoutNumber").val();
    bout = returnNull(bout);

    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout
        }),
        url: "/Views/Selection.aspx/getBoxersForSWB",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            $("#getBoxerLabel").text("Boxer 1 ID = " + data[0].boxer1Id + " Name = " + data[0].boxerName + " | Boxer 2 Id = " + data[0].boxer2Id + "Name = " + data[0].boxer2Name);
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

function populateAddBoxer(Name,div)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            Name: Name, div:div
        }),
        url: "/Views/Selection.aspx/GetBoxerDetailsByName",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
            
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;
}

function insertBoxer()
{
    var returnedValue = 0;

    var name = $("#addName").val();
    name = returnNull(name);
    var image = $("#addImage").val();
    image = returnNull(image);
    var nationality = $("#addNationality").val();
    nationality = returnNull(nationality);
    var nickname = $("#addNickname").val();
    nickname = returnNull(nickname);
    var Stance = $("#addStance").val();
    Stance = returnNull(Stance); 
    var height = $("#addheight").val();
    height = returnNull(height);
    var reach = $("#addReach").val();
    reach = returnNull(reach);
    var residence = $("#addresidence").val();
    residence = returnNull(residence);
    var age = $("#addage").val();
    age = returnNull(age);
    var division = $("#addDivision").val();
    division = returnNull(division);
    var won = $("#addWon").val();
    won = returnNull(won);
    var lost = $("#addLost").val();
    lost = returnNull(lost);
    var drawn = $("#addDrawn").val();
    drawn = returnNull(drawn);
    var Ko = $("#addKo").val();
    Ko = returnNull(Ko);
    var IBF = $("#addIBF").val();
    IBF = returnNull(IBF);
    var WBC = $("#addWBC").val();
    WBC = returnNull(WBC);
    var IBO = $("#addIBO").val();
    IBO = returnNull(IBO);
    var WBA = $("#addWBA").val();
    WBA = returnNull(WBA);
    var WBO = $("#addWBO").val();
    WBO = returnNull(WBO);
    var Last1 = $("#addlast1").val();
    Last1 = returnNull(Last1);
    var Last2 = $("#addLast2").val();
    Last2 = returnNull(Last2);
    var Last3 = $("#addLast3").val();
    Last3 = returnNull(Last3);
    var Last4 = $("#addLast4").val();
    Last4 = returnNull(Last4);
    var Last5 = $("#addLast5").val();
    Last5 = returnNull(Last5);
    var rounds = $("#addRounds").val();
    rounds = returnNull(rounds);
    var bouts = $("#addBouts").val();
    bouts = returnNull(bouts);

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            name: name,
            image: image,
            nationality: nationality,
            nickname: nickname,
            stance: Stance,
            height: height,
            reach: reach,
            residence: residence,
            age: age,
            division: division,
            won: won,
            lost: lost,
            drawn: drawn,
            ko: Ko,
            ibf: IBF,
            wbc: WBC,
            ibo: IBO,
            wba: WBA,
            wbo: WBO,
            last1: Last1,
            last2: Last2,
            last3: Last3,
            last4: Last4,
            last5: Last5,
            rounds: rounds,
            bouts:bouts,
        }),
        url: "/Views/Admin.aspx/insertBoxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
                $("#insertBoxerLabel").text("boxer not added");
            }
            else if (returnedValue > 0)
            {
                $("#insertBoxerLabel").text("boxer added");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    }); 

    return returnedValue;
}

function insertBout()
{
    var returnedValue = 0;

    var Season = $("#addSeason").val();
    Season = returnNull(Season);
    var Week = $("#addWeek").val();
    Week = returnNull(Week);
    var bout = $("#addBoutNumber").val();
    bout = returnNull(bout);
    var Boxer1 = $("#addBoxer1").val();
    Boxer1 = returnNull(Boxer1);
    var Boxer2 = $("#addBoxer2").val();
    Boxer2 = returnNull(Boxer2);
    var Rounds = $("#Rounds").val();
    Rounds = returnNull(Rounds);
    var Date = $("#addDate").val();
    Date = returnNull(Date);
    var Location = $("#addLocation").val();
    Location = returnNull(Location);
    var boutDivision = $("#addBoutDiv").val();
    boutDivision = returnNull(boutDivision);
    var televised = $("#addTelevised").val();
    televised = returnNull(televised);
    var title = $("#addTitle").val();
    title = returnNull(title);    

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout,
            boxer1: Boxer1,
            boxer2: Boxer2,
            rounds: Rounds,
            date: Date,
            location: Location,
            division: boutDivision,
            televised: televised,
            title: title,          
        }),
        url: "/Views/Admin.aspx/insertBout",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
               
            }
            else if (returnedValue > 0)
            {
                
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return returnedValue;
}

function insertResultsForBout()
{
    var returnedValue = 0;

    var Season = $("#addResultSeason").val();
    Season = returnNull(Season);
    var Week = $("#addResultWeek").val();
    Week = returnNull(Week);
    var bout = $("#addResultBoutNumber").val();
    bout = returnNull(bout);
    var Outcome = $("#addOutcome").val();
    Outcome = returnNull(Outcome);
    var Round = $("#addWinnerRound").val();
    Round = returnNull(Round);
    var BoxerWon = $("#addBoxerWon").val();
    BoxerWon = returnNull(BoxerWon);
    var BoxerLost = $("#addBoxerLost").val();
    BoxerLost = returnNull(BoxerLost);
    var TieBreaker = $("#addTieBreaker").val();
    TieBreaker = returnNull(TieBreaker); 

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: Season,
            week: Week,
            bout: bout,
            Outcome: Outcome,
            Round: Round,
            BoxerLost: BoxerLost,
            BoxerWon: BoxerWon,
            TieBreaker: TieBreaker, 
        }),
        url: "/Views/Admin.aspx/insertResultsBout",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {

            }
            else if (returnedValue > 0)
            {

            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returnedValue;
}

function returnNull(string)
{
    if (string == "")
        return null;
    else
        return string;
}

function updateBoxer()
{
    var returnedValue = 0;

    var image = $("#addImage").val();
    image = returnNull(image);

    var Div = $("#addDiv").val();
    Div = returnNull(Div);   

    var name = $("#addName").val();
    name = returnNull(name);   
    var age = $("#addage").val();
    age = returnNull(age);   
    var won = $("#addWon").val();
    won = returnNull(won);
    var lost = $("#addLost").val();
    lost = returnNull(lost);
    var drawn = $("#addDrawn").val();
    drawn = returnNull(drawn);
    var Ko = $("#addKo").val();
    Ko = returnNull(Ko);
    var IBF = $("#addIBF").val();
    IBF = returnNull(IBF);
    var WBC = $("#addWBC").val();
    WBC = returnNull(WBC);
    var IBO = $("#addIBO").val();
    IBO = returnNull(IBO);
    var WBA = $("#addWBA").val();
    WBA = returnNull(WBA);
    var WBO = $("#addWBO").val();
    WBO = returnNull(WBO);
    var Last1 = $("#addlast1").val();
    Last1 = returnNull(Last1);
    var Last2 = $("#addLast2").val();
    Last2 = returnNull(Last2);
    var Last3 = $("#addLast3").val();
    Last3 = returnNull(Last3);
    var Last4 = $("#addLast4").val();
    Last4 = returnNull(Last4);
    var Last5 = $("#addLast5").val();
    Last5 = returnNull(Last5);
    var rounds = $("#addRounds").val();
    rounds = returnNull(rounds);
    var bouts = $("#addBouts").val();
    bouts = returnNull(bouts);

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            image:image,
            Div:Div,
            name: name,           
            age: age,           
            won: won,
            lost: lost,
            drawn: drawn,
            ko: Ko,
            ibf: IBF,
            wbc: WBC,
            ibo: IBO,
            wba: WBA,
            wbo: WBO,
            last1: Last1,
            last2: Last2,
            last3: Last3,
            last4: Last4,
            last5: Last5,
            rounds: rounds,
            bouts: bouts,
        }),
        url: "/Views/Admin.aspx/updateBoxer",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returnedValue = object.d;
            if (returnedValue == 0)
            {
                $("#insertBoxerLabel").text("boxer not added");
            }
            else if (returnedValue > 0)
            {
                $("#insertBoxerLabel").text("boxer added");
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returnedValue;
}