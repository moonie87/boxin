﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Index.aspx.cs" Inherits="BoxingApp.Index" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
  <%--  <script src="/Global.js"></script>
    <script src="/Scripts/Login.js"></script>--%>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

    <div>
         <div class="headerTitleSelection">
            <h1 class="centreText" style="font-size:30px;">Home</h1>
        </div>
        <div class="bodySelection">
    <div class="centreText">
       <h2 class="centreText">Each season is 16 rounds long (one round a week),</h2>
            <h2 class="centreText">Each round has 5 bouts,</h2> 
            <h2 class="centreText">A possible of 90 points are up for grabs each round.</h2><br />
        <h3 class="centreText">SCORE BREAKDOWN</h3>
        <h4 class="centreText">A Correct Round = <b>10</b> points</h4><br />
        <h4 class="centreText">A Correct Outcome = <b>5</b> points</h4><br />
        <h4 class="centreText">A Correct Boxer = <b>3</b> points</h4><br />
       <p class="centreText" style="font-size:30px;">Play for fun or purchase a season pass for the chance to win Prizes.<br/>    

       </p>  
        <h3 class="centreText">JUST FOLLOW THESE 3 EASY STEPS.<br/>   </h3>
    </div>
    <div class="width100">
        <div class="centreText" style="margin-left: 28%;">
            <div >
            <div class="round-circle">1</div><h3 style="margin-top: 50px;">Register / Login</h3>

            </div>
             <div>
            <div class="round-circle">2</div><h3 style="margin-top: 50px;">Pick your selections</h3>
                 </div>
             <div>
            <div class="round-circle">3</div><h3 style="margin-top: 50px;">Share and invite your friends, family and work colleagues</h3>
                 </div>
            <br />
            <br />
           
        </div>
     
    </div>
              <input type="button" id="HomeRegisterBtn" value="Register"  class="width50 loginBtn topMenu k-button" />
             <input type="button" id="HomeLoginBtn" value="Login"  class="width50 loginBtn topMenu k-button" />
            </div>

      <%--  <h1 class="centreText" style="color:white;">Currently Down. We should be back up soon!!</h1>--%>

</div>

</asp:Content>