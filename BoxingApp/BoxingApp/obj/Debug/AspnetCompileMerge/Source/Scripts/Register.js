﻿$(document).ready(function () {

    $("#RegisterBtn").kendoButton({
        click: function ()
        {
            RegisterUser();
        }
    });
});



function RegisterUser(emailA,firstnameA,lastnameA,outhProvider,outhId) {
    var userName, email, firstName, lastName, password, password2,passwordCheck,emailVal;
    if (outhProvider != null && outhId != null)
    {
        email = emailA;
        firstName = firstnameA;
        lastName = lastnameA;
        password = null;
        userName = null;
    }
    else
    {
        userName = $("#RegisterUserNameInput").val();
        email = $("#RegisterEmailInput").val();
        firstName = $("#RegisterFirstNameInput").val();
        lastName = $("#RegisterLastNameInput").val();
        password = $("#RegisterPasswordInput").val();
        password2 = $("#RegisterPassword2Input").val();
        passwordCheck = true;       
       
            emailVal = validateEmail(email);
        
        if (password.length < 8)
        {
            popupMessage("large", "Invalid Password", "Password must be atleast 8 characters long.");
            return false;
        }
       
    }
  
    if (password != password2)
    {
        passwordCheck = false;
    }
    if (outhId != null)
    {       

        $.ajax({
            type: "post",
            data: JSON.stringify({
                userName: userName, Email: email, FName: firstName, LName: lastName, Password: password, outh_provider: outhProvider, outhId: outhId
            }),
            url: "/Views/Register.aspx/RegisterUser",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function () {                
            },
            complete: function () {
                popupMessage("small", "Registered", "You have registerd");
                emailNotification("forgotpassword@boxing5.com", "Boxing5", "mark.b.griffiths@hotmail.com", "help", "<h1>This is the first <b>Test</b></h1>");
                window.location.href = "/Login.aspx";
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
    }
    else
    {
        var passwordconf = checkPassword();
        if (passwordconf == false)
        {
            popupMessage("large", "Error", "Passwords Do not Match");
            return;
        }

        if (emailVal != true)
        {
            popupMessage("large", "Invalid email", "Email address Is not valid");
            return false;
        }

        if (passwordCheck == true && emailVal == true) {
            $.ajax({
                type: "post",
                data: JSON.stringify({
                    userName: userName, Email: email, FName: firstName, LName: lastName, Password: password, outh_provider: null, outhId: null
                }),
                url: "/Views/Register.aspx/RegisterUser",
                dataType: "json",
                async: false,
                contentType: "application/json",
                success: function () {
                },
                complete: function () {
                    popupMessage("small", "Registered", "You have registerd");
                    emailNotification("forgotpassword@boxing5.com", "Boxing5", email, "Welcome to Boxing5", "<div style='margin-left:65px;'><h1>Welcome to boxing5.com</h1></br>" + firstName + " " + lastName + ",</br> Boxing5 is a boxing prediction application." +
                        "Predict 5 bouts a week and grab a possible 90 points. </br> You can either play just for fun or grab your self a season pass and win prizes.</br></br> Dont forget to invite your friends, family and co-workers and create a mini league for bragging rights.</div>");
                    window.location.href = "/Login.aspx";
                },
                error: function (object) {
                    console.log("error" + object);
                }
            });
        }
        else if (passwordCheck == false) {
            $("#RegisterPasswordInput").addClass("userInputError");
            $("#RegisterPassword2Input").addClass("userInputError");
        }
        else if (emailVal == false) {
            $("#RegisterEmailInput").addClass("userInputError");
        }
    }
}
function checkPassword()
{
    var password = $("#RegisterPasswordInput").val();
    var passwordconfirm = $("#RegisterPassword2Input").val();
    var ok = true;
    if (password != passwordconfirm)
    {

        document.getElementById("RegisterPasswordInput").style.borderColor = "#E34234";
        document.getElementById("RegisterPassword2Input").style.borderColor = "#E34234";
        ok = false;
    }
    return ok;
} 