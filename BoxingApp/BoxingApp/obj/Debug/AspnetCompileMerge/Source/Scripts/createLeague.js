﻿$(document).ready(function () {

    $("#createLeagueBtn").kendoButton({
        click: function() {
            createLeague();
        }
    });
});

function createLeague() {
  
    var leagueName = $("#createLeagueInput").val();
    var user = parserId();

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, name:leagueName
        }),
        url: "/Views/Leagues.aspx/createLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function () {
            window.location.href = 'Leagues.aspx';
        },
        complete: function ()
        {
            var data = populateUserDetails(false);
            emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Created ", "<div style='margin-left:65px;'><h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
                "You created League - " + leagueName + "<br> Dont Forget to invite people to join your league. <br> Have Fun!</div > ");

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

