﻿$(document).ready(function ()
{



    var data = [
                 { text: "", value: "0" },
                 { text: "Minimumweight", value: "1" },
                 { text: "Light Flyweight", value: "2" },
                 { text: "Flyweight", value: "3" },
                 { text: "Super flyweight", value: "4" },
                 { text: "Bantamweight", value: "5" },
                 { text: "Super Bantamweight", value: "6" },
                 { text: "Lightweight", value: "7" },
                 { text: "Super lightweight", value: "8" },
                 { text: "Middleweight", value: "9" },
                 { text: "Super Middleweight", value: "10" },
                 { text: "Light Heavyweight", value: "11" },
                 { text: "Cruiserweight", value: "12" },
                 { text: "Heavyweight", value: "13" }

    ];

    // create DropDownList from input HTML element
    $("#IBO").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeIBO
    });

    $("#WBA").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeWBA
    });

    $("#IBF").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 1,
        change: onChangeIBF
    });

    $("#WBC").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeWBC
    });

    $("#WBO").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeWBO
    });

    $("#IBF").data("kendoDropDownList").select(1);
    onChangeIBF();
    

    function onChangeWBA() {
        var value = $("#WBA").val();

        $("#WBO").data("kendoDropDownList").select(0);
        $("#WBC").data("kendoDropDownList").select(0);
        $("#IBF").data("kendoDropDownList").select(0);
        $("#IBO").data("kendoDropDownList").select(0);

        if (value == '1') {
            var newTData = getRankings('wba', 1);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Minimumweight");
        } else if (value == '2') {
            var newTData = getRankings('wba', 2);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Light Flyweight");
        } else if (value == '3') {
            var newTData = getRankings('wba', 3);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Flyweight");
        } else if (value == '4') {
            var newTData = getRankings('wba', 4);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Super Flyweight");
        } else if (value == '5') {
            var newTData = getRankings('wba', 5);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Bantamweight ");
        } else if (value == '6') {
            var newTData = getRankings('wba', 6);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Super Bantamweight");
        } else if (value == '7') {
            var newTData = getRankings('wba', 7);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Lightweight");
        } else if (value == '8') {
            var newTData = getRankings('wba', 8);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Super Lightweight");
        } else if (value == '9') {
            var newTData = getRankings('wba', 9);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Middleweight");
        } else if (value == '10') {
            var newTData = getRankings('wba', 10);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Super Middleweight");
        } else if (value == '11') {
            var newTData = getRankings('wba', 11);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Light Heavyweight");
        } else if (value == '12') {
            var newTData = getRankings('wba', 12);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Cruiserweight ");
        } else if (value == '13') {
            var newTData = getRankings('wba', 13);
            populateTable(newTData);
            $("#rankingsName").text("WBA - Heavyweight");
        }
    }

    function onChangeIBF() {
        var value = $("#IBF").val();

        $("#WBO").data("kendoDropDownList").select(0);
        $("#WBC").data("kendoDropDownList").select(0);
        $("#WBA").data("kendoDropDownList").select(0);
        $("#IBO").data("kendoDropDownList").select(0);

        if (value == '1') {
            var newTData = getRankings('ibf', 1);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Minimumweight  ");
        } else if (value == '2') {
            var newTData = getRankings('ibf', 2);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Light Flyweight  ");
        } else if (value == '3') {
            var newTData = getRankings('ibf', 3);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Flyweight  ");
        } else if (value == '4') {
            var newTData = getRankings('ibf', 4);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Super Flyweight  ");
        } else if (value == '5') {
            var newTData = getRankings('ibf', 5);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Bantamweight  ");
        } else if (value == '6') {
            var newTData = getRankings('ibf', 6);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Super Bantamweight  ");
        } else if (value == '7') {
            var newTData = getRankings('ibf', 7);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Lightweight  ");
        } else if (value == '8') {
            var newTData = getRankings('ibf', 8);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Super Lightweight  ");
        } else if (value == '9') {
            var newTData = getRankings('ibf', 9);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Middleweight  ");
        } else if (value == '10') {
            var newTData = getRankings('ibf', 10);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Super Middleweight  ");
        } else if (value == '11') {
            var newTData = getRankings('ibf',11);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Light Heavyweight  ");
        } else if (value == '12') {
            var newTData = getRankings('ibf', 12);
            populateTable(newTData);
            $("#rankingsName").text("IBF - CruiserWeight  ");
        } else if (value == '13') {
            var newTData = getRankings('ibf', 13);
            populateTable(newTData);
            $("#rankingsName").text("IBF - Heavyweight  ");
        }
    }


    function onChangeIBO() {
        var value = $("#IBO").val();
       
        $("#WBO").data("kendoDropDownList").select(0);
        $("#WBC").data("kendoDropDownList").select(0);
        $("#IBF").data("kendoDropDownList").select(0);
        $("#WBA").data("kendoDropDownList").select(0);

            if (value == '1') {
                var newTData = getRankings('ibo', 1);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Minimumweight  ");
            } else if (value == '2') {
                var newTData = getRankings('ibo', 2);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Light Flyweight  ");
            } else if (value == '3') {
                var newTData = getRankings('ibo', 3);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Flyweight  ");
            } else if (value == '4') {
                var newTData = getRankings('ibo', 4);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Super Flyweight  ");
            } else if (value == '5') {
                var newTData = getRankings('ibo', 5);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Bantamweight  ");
            } else if (value == '6') {
                var newTData = getRankings('ibo', 6);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Super Bantamweight  ");
            } else if (value == '7') {
                var newTData = getRankings('ibo', 7);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Lightweight  ");
            } else if (value == '8') {
                var newTData = getRankings('ibo', 8);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Super Lightweight  ");
            } else if (value == '9') {
                var newTData = getRankings('ibo', 9);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Middleweight  ");
            } else if (value == '10') {
                var newTData = getRankings('ibo', 10);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Super Middleweight  ");
            } else if (value == '11') {
                var newTData = getRankings('ibo', 11);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Light Heavyweight  ");
            } else if (value == '12') {
                var newTData = getRankings('ibo', 12);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Cruiseweight  ");
            } else if (value == '13') {
                var newTData = getRankings('ibo', 13);
                populateTable(newTData);
                $("#rankingsName").text("IBO - Heavyweight  ");
            }
    }

    function onChangeWBO() {
        var value = $("#WBO").val();

        $("#WBA").data("kendoDropDownList").select(0);
        $("#WBC").data("kendoDropDownList").select(0);
        $("#IBF").data("kendoDropDownList").select(0);
        $("#IBO").data("kendoDropDownList").select(0);

                    if (value == '1') {
                var newTData = getRankings('wbo', 1);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Minimumweight  ");
            } else if (value == '2') {
                var newTData = getRankings('wbo', 2);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Light Flyweight  ");
            } else if (value == '3') {
                var newTData = getRankings('wbo', 3);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Flyweight  ");
            } else if (value == '4') {
                var newTData = getRankings('wbo', 4);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Super Flyweight  ");
            } else if (value == '5') {
                var newTData = getRankings('wbo', 5);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Bantamweight  ");
            } else if (value == '6') {
                var newTData = getRankings('wbo', 6);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Super Bantamweight  ");
            } else if (value == '7') {
                var newTData = getRankings('wbo', 7);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Lightweight  ");
            } else if (value == '8') {
                var newTData = getRankings('wbo', 8);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Super Lightweight  ");
            } else if (value == '9') {
                var newTData = getRankings('wbo', 9);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Middleweight  ");
            } else if (value == '10') {
                var newTData = getRankings('wbo', 10);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Super Middleweight  ");
            } else if (value == '11') {
                var newTData = getRankings('wbo', 11);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Light Heavyweight  ");
            } else if (value == '12') {
                var newTData = getRankings('wbo',12);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Cruiseweight  ");
            } else if (value == '13') {
                var newTData = getRankings('wbo', 13);
                populateTable(newTData);
                $("#rankingsName").text("WBO - Heavyweight  ");
            }
    }

    function onChangeWBC() {
        var value = $("#WBC").val();

        $("#WBO").data("kendoDropDownList").select(0);
        $("#WBA").data("kendoDropDownList").select(0);
        $("#IBF").data("kendoDropDownList").select(0);
        $("#IBO").data("kendoDropDownList").select(0);

        if (value == '1') {
            var newTData = getRankings('wbc', 1);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Minimumweight  ");
        } else if (value == '2') {
            var newTData = getRankings('wbc', 2);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Light Flyweight  ");
        } else if (value == '3') {
            var newTData = getRankings('wbc', 3);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Flyweight  ");
        } else if (value == '4') {
            var newTData = getRankings('wbc', 4);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Super Flyweight  ");
        } else if (value == '5') {
            var newTData = getRankings('wbc', 5);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Bantamweight  ");
        } else if (value == '6') {
            var newTData = getRankings('wbc', 6);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Super Bantamweight  ");
        } else if (value == '7') {
            var newTData = getRankings('wbc', 7);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Lightweight  ");
        } else if (value == '8') {
            var newTData = getRankings('wbc', 8);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Super Lightweight  ");
        } else if (value == '9') {
            var newTData = getRankings('wbc', 9);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Middleweight  ");
        } else if (value == '10') {
            var newTData = getRankings('wbc', 10);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Super Middleweight  ");
        } else if (value == '11') {
            var newTData = getRankings('wbc', 11);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Light Heavyweight  ");
        } else if (value == '12') {
            var newTData = getRankings('wbc', 12);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Cruiseweight  ");
        } else if (value == '13') {
            var newTData = getRankings('wbc', 13);
            populateTable(newTData);
            $("#rankingsName").text("WBC - Heavyweight  ");
        }
    }


   
});
    function populateTable(data){
        var a = 0;
        $('#rankingsTable').DataTable({

            destroy: true,
            "ordering": false,
            select:true,

            data: data,
            columns: [
            {data: 'id'},
                { data: 'PosId' },
                { data: 'name' },
                { data: 'won' },
                { data: 'lost' },
                { data: 'drawn' },

            ],
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData.PosId == "1") {
                    $('td', nRow).css('background-color', 'Gold');
                    $('td:eq(0)', nRow).html('<i class="fa fa-trophy" aria-hidden="true"></i>');
                }

                //if (a == 0)
                //{
                //    var table = $("#rankingsTable").DataTable();

                //    $('#rankingsTable tbody').off('click');

                //    $("#rankingsTable tbody").on('dblclick', 'tr', function ()
                //    {
                //        var att = $(this).attr('id');
                //        var tr = $(this).closest("tr");
                //        var data = table.rows(tr).data();
                //        alert('you clicked' + data[0].name);
                //    })
                //    a = 1;
                //}
            }
        });
    }

    function getRankings(federation,div) {

        var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                federation: federation, div:div
            }),
            url: "/Views/rankings.aspx/getRankings",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object) {
                data = object.d;
            },
            complete: function () {
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
        return data;
    }
