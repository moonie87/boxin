﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Login.aspx.cs" Inherits="BoxingApp.Login" %>


<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <script src="/Scripts/Login.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

     <div id="ForgotUserDiv" class="redBackground" style="display:none;">       
        <div class="resetClass";>
            <label class="centreText">Please enter the email address you used to sign up with.</label><br/>
            <label class="centreText">Email</label><br />
        <input id="ForgotUserEmail" type="text" class="loginBox centreText"  /><br /><br />
             <input type="button" id="sendUserNameButton" value="Send UserName" class="k-button topMenu centreText" />
            </div>
         </div>


         <div id="ResetPasswordDiv" class="redBackground" style="display:none;">
       
        <div class="resetClass";>
            <label class="centreText">Please enter the email address you used to sign up with.</label><br/>
            <label class="centreText">Email</label><br />
        <input id="resetEmail" type="text" class="loginBox centreText"  /><br /><br />
             <input type="button" id="getResetCode" value="Get a Code" class="k-button topMenu centreText"  /><br /><br />
            <div id="resetPwDiv" style="display:none;">
            <label class="centreText">Reset Code</label><br />
            <input id="ResetCode" type="text" class="loginBox centreText"/><br />
            <label class="centreText">new Password</label> <br />
         <input id="resetpassword" type="password" class="loginBox centreText"/> <br />
            <label class="centreText">repeat Password</label> <br />
             <input id="resetPassword2" type="password" class="loginBox centreText"/><br /><br />
             <input type="button" id="resetPasswordButton" value="Change Password" class="k-button topMenu centreText" /><br /><br/>
                </div>
            </div>
    </div>

    <div>
        <div class="headerTitleSelection">
            <h3 class="centreText">Login</h3>
        </div>
        <div class="bodySelection">
        <div class="width100">
            <div class="width100">              
                <div  id="loginDetails" class="centreText">                    
                    <br />
                    <br />
                    <input type="text" id="RegisterUserNameInput" placeholder="UserName" class="loginBox" /><br />
                    <br />
                    <input type="password" id="RegisterPasswordInput" placeholder="Password" class="loginBox"/><br />
                    <br />
                    <input id="LoginBtn" value="Login" class="width100 loginBtn" />  
                     <input id="LoginRegisterBtn" value="Create an Account" class="width100 loginBtn" /> 
                    <div class="padding5px">
                     <a href="javascript:void(0);" onclick="fbLogin()" id="fbLink" class="btn btn-block btn-social btn-facebook width100">
                        <span class="fa fa-facebook"></span>Login in with Facebook
                    </a>
                        </div>
                    <input id="ForgotPassword" value="Forgot Password" class="width100 loginBtn" /> 
                    <input id="ForgotUser" value="Forgot UserName" class="width100 loginBtn" /> 
                </div>
            </div>       
        </div>

        <label id="signedIn"></label>
            </div>
    </div>

</asp:Content>
