﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="joinLeague.aspx.cs" Inherits="BoxingApp.Views.joinLeague" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">

    <script src="/Scripts/joinLeague.js"></script>
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

     <div class="headerTitleSelection">
        <h3 class="centreText">Join a League</h3>
    </div>  

    <div class="leagueBorder">
         <div style="text-align:center;">                   
                    <p>Compete against your friends, family and co workers</p>                   
                </div>       
        <div >
            <div class="centreText">
                <label class="centreText">Enter the League Code</label>
                <input type="text" id="JoinLeagueInput" class="loginBox"/><br />
                <input id="JoinLeagueBtn" value="Join" class="topMenu centreText margintop5px"/>
            </div>           
        </div>
    </div>

    </asp:Content>