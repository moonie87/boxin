﻿<%@ Page Title="" Language="C#" MasterPageFile="~/application.Master" AutoEventWireup="true" CodeBehind="Selection.aspx.cs" Inherits="BoxingApp.Views.WebForm1" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">

    <link href="/Css/selection.css" rel="stylesheet" />
    <script src="/Scripts/Selection.js"></script>

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">
    <div class="headerTitleSelectionNew">
    <h3 class="centreText">Selections</h3>
    <div class="centreText">
        <label id="selSeason"></label> &nbsp
        <label id="selWeek"></label><br />

    </div>
        </div>
    <div class="bodySelectionNew" id="bodySelection">
        <div class="boutInfo">
            <label class="centreText"><b>Bout 1</b></label>
              <br />
            <label id="bout1Loc" class="centreText"></label>
           <b> <label id="bout1Boxer1"></label>&nbsp</b>
            <label>VS</label>&nbsp
           <b> <label id="bout1Boxer2"></label> </b>
            <br />
            <label id="bout1Date" class="centreText"></label>
              <br />
            <label id="bout1TV" class="centreText"></label>
              <br />
            <label id="bout1Div" class="centreText"></label>            
            <label id="Title1Div" class="centreText"></label>
        </div>
        <div id="bout1" class="boutDiv">
            <div class="boxerWrapper">
                <div style="position: relative">
                    <div class="boxerDetailsLeft">
                       
                        <label id="Boxer1Name" class="boxerName"></label>
                        <br />
                         <div class="borderSelection">
                        <label id="Boxer1Hometown"></label>
                        <br/><br/>
                         <label id="Boxer1Age" ></label>
                        <br />
                        <label id="Boxer1Height"></label>
                        <br />
                        <label id="Boxer1Reach"></label>
                        <br />
                         <label id="Boxer1Nation"></label>
                        <br />
                         <label id="Boxer1Stance"></label>                        
                        <br />
                         <label id="Boxer1Ko"></label>   
                                      <div>
                                  <label id="Boxer1L1"></label> 
                                  <label id="Boxer1L2"></label> 
                                  <label id="Boxer1L3"></label> 
                                  <label id="Boxer1L4"></label> 
                                  <label id="Boxer1L5"></label> 
                            </div>
                            </div>
                        <div>
                            <div class="selectionWLD">
                                <div class="winDiv"><label id="WonBout1Box11" class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout1Box1" class="wonT"></label></div>
                            </div>
                            <div class="selectionWLD">
                                <div class="LossDiv"><label id="LostBout1Box11" class="won">L</label></div>    
                                <div class="LossDivText"><label id="LostBout1Box1" class="wonT"></label></div>    
                            </div>
                            <div class="selectionWLD">
                                <div class="DrawDiv"><label id="DrawnBout1Box11" class="won">D</label></div>    
                                <div class="DrawDivText"><label id="DrawnBout1Box1" class="wonT"></label></div>    


                            </div>
                        </div>
                    </div>
                    <div class="boxerLeftImage">

                        <img src="" id="boxer1Src" class="boxerImage"/>
                    </div>
                </div>
            </div>
            <div class="middleBout">
                <div class="centreText">
                    <label class="centreText">Winner</label>
                    <input id="Bout1Winner" />
                </div>
                <br />
                 
                <div class="centreText">
                    <label class="centreText">How</label>
                    <input id="Bout1How" />
                </div>
                <br />
                <div class="centreText">
                    <label class="centreText">Round</label>
                    <input id="Bout1Round" />
                </div>
             
            </div>
            <div class="boxerWrapperR">
                <div style="position: relative">
                    <div class="boxerDetailsRight">
                         <label id="Boxer2Name" class="boxerNameRight"></label>
                        <br />
                        <div class="borderSelection">
                        <label id="Boxer2Hometown"></label>
                        <br/><br/>
                         <label id="Boxer2Age"></label>
                        <br />
                        <label id="Boxer2Height"></label>
                        <br />
                        <label id="Boxer2Reach" ></label>
                        <br />
                         <label id="Boxer2Nation"></label>
                        <br />
                         <label id="Boxer2Stance"></label>                       
                        <br />
                         <label id="Boxer2Ko"></label>    
                                <div>
                                  <label id="Boxer2L1"></label> 
                                  <label id="Boxer2L2"></label> 
                                  <label id="Boxer2L3"></label> 
                                  <label id="Boxer2L4"></label> 
                                  <label id="Boxer2L5"></label> 
                            </div>
                            </div>
                              <div>
                                    <div class="selectionRightWLD">
                                <div class="DrawDiv"><label class="won">D</label></div>
                                <div class="DrawDivText"><label id="DrawnBout1Box2" class="wonT"></label></div>    


                            </div>
                                     <div class="selectionRightWLD">
                             <div class="LossDiv"><label class="won">L</label></div>
                                <div class="LossDivText"><label id="LostBout1Box2" class="wonT"></label></div>    
                            </div>
                            <div class="selectionRightWLD">
                                <div class="winDiv"><label class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout1Box2" class="wonT"></label></div>
                            </div>
                         
                          
                        </div>

                    </div>
                    <div class="boxerRightImage">

                        <img src="" id="boxer2Src" class="boxerImage" />
                    </div>
                </div>
            </div>
        </div>

        <br style="clear: both;" />
        <hr>
         <div class="boutInfo">
            <label class="centreText"><b>Bout 2</b></label>
              <br />
            <label id="bout2Loc" class="centreText"></label><br />
           <b> <label id="bout2Boxer1"></label>&nbsp</b>
            <label>VS</label>&nbsp
           <b> <label id="bout2Boxer2"></label> </b>
            <br />
            <label id="bout2Date" class="centreText"></label>
              <br />
            <label id="bout2TV" class="centreText"></label>
              <br />
            <label id="bout2Div" class="centreText"></label>            
            <label id="Title2Div" class="centreText"></label>
        </div>
        <div id="bout2" class="boutDiv">
            <div class="boxerWrapper">
                <div style="position: relative">
                    <div class="boxerDetailsLeft">
                        <label id="Boxer3Name" class="boxerName"></label>
                        <br />
                        <div class="borderSelection">
                        <label id="Boxer3Hometown"></label>
                        <br /><br />
                         <label id="Boxer3Age"></label>
                        <br />
                        <label id="Boxer3Height"></label>
                        <br />
                        <label id="Boxer3Reach"></label>
                        <br />
                         <label id="Boxer3Nation"></label>
                        <br />
                         <label id="Boxer3Stance"></label>                       
                        <br />
                         <label id="Boxer3Ko"></label>   
                                <div>
                                  <label id="Boxer3L1"></label> 
                                  <label id="Boxer3L2"></label> 
                                  <label id="Boxer3L3"></label> 
                                  <label id="Boxer3L4"></label> 
                                  <label id="Boxer3L5"></label> 
                            </div>
                            </div>
                                              <div >
                            <div class="selectionWLD">
                                <div class="winDiv"><label id="WonBout2Box11" class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout2Box1" class="wonT"></label></div>
                            </div>
                            <div class="selectionWLD">
                             <div class="LossDiv"><label id="LostBout2Box11" class="won">L</label></div>
                                <div class="LossDivText"><label id="LostBout2Box1" class="wonT"></label></div>    
                            </div>
                            <div class="selectionWLD">
                                  <div class="DrawDiv"><label id="DrawnBout2Box11" class="won">D</label></div>
                                <div class="DrawDivText"><label id="DrawnBout2Box1" class="wonT"></label></div>    


                            </div>
                        </div>
                    </div>
                    <div class="boxerLeftImage">
                        <img src="" id="boxer3Src" class="boxerImage" />
                    </div>
                </div>
            </div>
                <div class="middleBout">
                <div class="centreText">
                    <label class="centreText">Winner</label>
                    <input id="Bout2Winner" />
                </div>
                     <br />
                <div class="centreText">
                    <label class="centreText">How</label>
                    <input id="Bout2How" />
                </div>
                <br />
                <div class="centreText">
                    <label class="centreText">Round</label>
                    <input id="Bout2Round" />
                </div>
               
            </div>
            <div class="boxerWrapperR">
                <div style="position: relative">
                    <div class="boxerDetailsRight">
                        <label id="Boxer4Name" class="boxerNameRight"></label>
                        <br />
                        <div class="borderSelection">
                        <label   id="Boxer4Hometown"></label>
                        <br /><br />
                         <label   id="Boxer4Age"></label>
                        <br />
                        <label   id="Boxer4Height"></label>
                        <br />
                        <label   id="Boxer4Reach"></label>
                        <br />
                         <label   id="Boxer4Nation"></label>
                        <br />
                         <label   id="Boxer4Stance"></label>                       
                        <br />
                         <label   id="Boxer4Ko"></label>
                                <div>
                                  <label id="Boxer4L1"></label> 
                                  <label id="Boxer4L2"></label> 
                                  <label id="Boxer4L3"></label> 
                                  <label id="Boxer4L4"></label> 
                                  <label id="Boxer4L5"></label> 
                            </div>
                            </div>
                        <div>
                            <div class="selectionRightWLD">
                                <div class="DrawDiv">
                                    <label id="DrawnBout2Box21" class="won">D</label></div>
                                <div class="DrawDivText">
                                    <label id="DrawnBout2Box2" class="wonT"></label>
                                </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="LossDiv">
                                        <label id="LostBout2Box21" class="won">L</label></div>
                                    <div class="LossDivText">
                                        <label id="LostBout2Box2" class="wonT"></label>
                                    </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="winDiv">
                                       <label id="WonBout2Box21" class="won">W</label></div>
                                    <div class="winDivText">
                                        <label id="WonBout2Box2" class="wonT"></label>
                                    </div>
                                </div>
                          



                        </div>
                    </div>
                    <div class="boxerRightImage">

                        <img src="" id="boxer4Src"  class="boxerImage" />
                    </div>
                </div>
            </div>
        </div>

        <br style="clear: both;" />
        <hr>
          <div class="boutInfo">
            <label class="centreText"><b>Bout 3</b></label>
               <br />
            <label id="bout3Loc" class="centreText"></label>
           <b> <label id="bout3Boxer1"></label>&nbsp</b>
            <label>VS</label>&nbsp
           <b> <label id="bout3Boxer2"></label> </b>
            <br />
            <label id="bout3Date" class="centreText"></label>
               <br />
            <label id="bou3TV" class="centreText"></label>
               <br />
            <label id="bout3Div" class="centreText"></label>             
            <label id="Title3Div" class="centreText"></label>
        </div>
        <div id="bout3" class="boutDiv">
            <div class="boxerWrapper">
                <div style="position: relative">
                    <div class="boxerDetailsLeft">
                       <label id="Boxer5Name" class="boxerName"></label>
                        <br />
                        <div class="borderSelection">
                        <label id="Boxer5Hometown"></label>
                        <br /><br />
                         <label id="Boxer5Age"></label>
                        <br />
                        <label id="Boxer5Height"></label>
                        <br />
                        <label id="Boxer5Reach"></label>
                        <br />
                         <label id="Boxer5Nation"></label>
                        <br />
                         <label id="Boxer5Stance"></label>                      
                        <br />
                         <label id="Boxer5Ko"></label>
                                <div>
                                  <label id="Boxer5L1"></label> 
                                  <label id="Boxer5L2"></label> 
                                  <label id="Boxer5L3"></label> 
                                  <label id="Boxer5L4"></label> 
                                  <label id="Boxer5L5"></label> 
                            </div>
                            </div>
                                              <div>
                            <div class="selectionWLD">
                                <div class="winDiv"><label id="WonBout3Box11" class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout3Box1" class="wonT"></label></div>
                            </div>
                            <div class="selectionWLD">
                             <div class="LossDiv"><label id="LostBout3Box11" class="won">L</label></div>
                                <div class="LossDivText"><label id="LostBout3Box1" class="wonT"></label></div>    
                            </div>
                            <div class="selectionWLD">
                                                             <div class="DrawDiv"><label id="DrawnBout3Box11" class="won">D</label></div>
                                <div class="DrawDivText"><label id="DrawnBout3Box1" class="wonT"></label></div>    


                            </div>
                        </div>
                    </div>
                    <div class="boxerLeftImage">

                        <img src="" id="boxer5Src"  class="boxerImage" />
                    </div>
                </div>
            </div>
                <div class="middleBout">
                <div class="centreText">
                    <label class="centreText">Winner</label>
                    <input id="Bout3Winner" />
                </div>
                    <br />
                <div class="centreText">
                    <label class="centreText">How</label>
                    <input id="Bout3How" />
                </div>
                <br />
                <div class="centreText">
                    <label class="centreText">Round</label>
                    <input id="Bout3Round" />
                </div>
                
            </div>
            <div class="boxerWrapperR">
                <div style="position: relative">
                    <div class="boxerDetailsRight">
                        <label id="Boxer6Name" class="boxerNameRight"></label>
                        <br />
                        <div class="borderSelection">
                        <label   id="Boxer6Hometown"></label>
                        <br /><br />
                         <label   id="Boxer6Age"></label>
                        <br />
                        <label   id="Boxer6Height"></label>
                        <br />
                        <label   id="Boxer6Reach"></label>
                        <br />
                         <label   id="Boxer6Nation"></label>
                        <br />
                         <label   id="Boxer6Stance"></label>                       
                        <br />
                         <label   id="Boxer6Ko"></label> 
                                <div>
                                  <label id="Boxer6L1"></label> 
                                  <label id="Boxer6L2"></label> 
                                  <label id="Boxer6L3"></label> 
                                  <label id="Boxer6L4"></label> 
                                  <label id="Boxer6L5"></label> 
                            </div>
                            </div>
                         <div>
                            <div class="selectionRightWLD">
                                <div class="DrawDiv">
                                    <label id="DrawnBout3Box21" class="won">D</label></div>
                                <div class="DrawDivText">
                                    <label id="DrawnBout3Box2" class="wonT"></label>
                                </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="LossDiv">
                                        <label id="LostBout3Box21" class="won">L</label></div>
                                    <div class="LossDivText">
                                        <label id="LostBout3Box2" class="wonT"></label>
                                    </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="winDiv">
                                          <label id="WonBout3Box21" class="won">W</label></div>
                                    <div class="winDivText">
                                        <label id="WonBout3Box2" class="wonT"></label>
                                    </div>
                                </div>
                          



                        </div>
                    </div>
                    <div class="boxerRightImage">

                        <img src="" id="boxer6Src"  class="boxerImage" />
                    </div>
                </div>
            </div>
        </div>

        <br style="clear: both;" />
        <hr>
         <div class="boutInfo">
            <label class="centreText"><b>Bout 4</b></label>
              <br />
            <label id="bout4Loc" class="centreText"></label>
           <b> <label id="bout4Boxer1"></label>&nbsp</b>
            <label>VS</label>&nbsp
           <b> <label id="bout4Boxer2"></label> </b>
            <br />
            <label id="bout4Date" class="centreText"></label>
              <br />
            <label id="bout4TV" class="centreText"></label>
              <br />
            <label id="bout4Div" class="centreText"></label>            
            <label id="Title4Div" class="centreText"></label>
        </div>
        <div id="bout4" class="boutDiv">
            <div class="boxerWrapper">
                <div style="position: relative">
                    <div class="boxerDetailsLeft">
                        <label id="Boxer7Name" class="boxerName"></label>
                        <br />
                        <div class="borderSelection">
                        <label id="Boxer7Hometown"></label>
                        <br /><br />
                         <label id="Boxer7Age"></label>
                        <br />
                        <label id="Boxer7Height"></label>
                        <br />
                        <label id="Boxer7Reach"></label>
                        <br />
                         <label id="Boxer7Nation"></label>
                        <br />
                         <label id="Boxer7Stance"></label>
                       
                        <br />
                         <label id="Boxer7Ko"></label>  
                                <div>
                                  <label id="Boxer7L1"></label> 
                                  <label id="Boxer7L2"></label> 
                                  <label id="Boxer7L3"></label> 
                                  <label id="Boxer7L4"></label> 
                                  <label id="Boxer7L5"></label> 
                            </div>
                   
                            </div>
                                              <div>
                            <div class="selectionWLD">
                                <div class="winDiv"><label id="WonBout4Box11" class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout4Box1" class="wonT"></label></div>
                            </div>
                            <div class="selectionWLD">
                             <div class="LossDiv"><label id="LostBout4Box11" class="won">L</label></div>
                                <div class="LossDivText"><label id="LostBout4Box1" class="wonT"></label></div>    
                            </div>
                            <div class="selectionWLD">
                                                             <div class="DrawDiv"><label id="DrawnBout4Box11" class="won">D</label></div>
                                <div class="DrawDivText"><label id="DrawnBout4Box1" class="wonT"></label></div>    


                            </div>
                        </div>
                    </div>
                    <div class="boxerLeftImage">

                        <img src="" id="boxer7Src" class="boxerImage" />
                    </div>
                </div>
            </div>
                <div class="middleBout">
                <div class="centreText">
                    <label class="centreText">Winner</label>
                    <input id="Bout4Winner" />
                </div>
                      <br />
                <div class="centreText">
                    <label class="centreText">How</label>
                    <input id="Bout4How" />
                </div>
                <br />
                <div class="centreText">
                    <label class="centreText">Round</label>
                    <input id="Bout4Round" />
                </div>
              
            </div>
            <div class="boxerWrapperR">
                <div style="position: relative">
                    <div class="boxerDetailsRight">
                       <label id="Boxer8Name" class="boxerNameRight"></label>
                        <br />
                        <div class="borderSelection">
                        <label   id="Boxer8Hometown"></label>
                        <br /><br />
                         <label   id="Boxer8Age"></label>
                        <br />
                        <label   id="Boxer8Height"></label>
                        <br />
                        <label   id="Boxer8Reach"></label>
                        <br />
                         <label   id="Boxer8Nation"></label>
                        <br />
                         <label   id="Boxer8Stance"></label>
                       
                        <br />
                         <label   id="Boxer8Ko"></label>   
                                <div>
                                  <label id="Boxer8L1"></label> 
                                  <label id="Boxer8L2"></label> 
                                  <label id="Boxer8L3"></label> 
                                  <label id="Boxer8L4"></label> 
                                  <label id="Boxer8L5"></label> 
                            </div>
                            </div>
                                                 <div>
                            <div class="selectionRightWLD">
                                <div class="DrawDiv">
                                     <label id="DrawnBout4Box21" class="won">D</label></div>
                                <div class="DrawDivText">
                                    <label id="DrawnBout4Box2" class="wonT"></label>
                                </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="LossDiv">
                                        <label id="LostBout4Box21" class="won">L</label></div>
                                    <div class="LossDivText">
                                        <label id="LostBout4Box2" class="wonT"></label>
                                    </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="winDiv">
                                       <label id="WonBout4Box21" class="won">W</label></div>
                                    <div class="winDivText">
                                        <label id="WonBout4Box2" class="wonT"></label>
                                    </div>
                                </div>
                          



                        </div>
                    </div>
                    <div class="boxerRightImage">

                        <img src="" id="boxer8Src" class="boxerImage" />
                    </div>
                </div>
            </div>
        </div>
         <br style="clear: both;" />
        <hr>
         <div class="boutInfo">
            <label class="centreText"><b>Bout 5</b></label>
              <br />
            <label id="bout5Loc" class="centreText"></label>
           <b> <label id="bout5Boxer1"></label>&nbsp</b>
            <label>VS</label>&nbsp
           <b> <label id="bout5Boxer2"></label> </b>
            <br />
            <label id="bout5Date" class="centreText"></label>
              <br />
            <label id="bout5TV" class="centreText"></label>
              <br />
            <label id="bout5Div" class="centreText"></label>            
            <label id="Title5Div" class="centreText"></label>
        </div>

                <div id="bout5" class="boutDiv">
            <div class="boxerWrapper">
                <div style="position: relative">
                    <div class="boxerDetailsLeft">
                        <label id="Boxer9Name" class="boxerName"></label>
                        <br />
                        <div class="borderSelection">
                        <label id="Boxer9Hometown"></label>
                        <br /><br />
                         <label id="Boxer9Age" ></label>
                        <br />
                        <label id="Boxer9Height"></label>
                        <br />
                        <label id="Boxer9Reach"></label>
                        <br />
                         <label id="Boxer9Nation"></label>
                        <br />
                         <label id="Boxer9Stance"></label>
                       
                        <br />
                         <label id="Boxer9Ko"></label> 
                                <div>
                                  <label id="Boxer9L1"></label> 
                                  <label id="Boxer9L2"></label> 
                                  <label id="Boxer9L3"></label> 
                                  <label id="Boxer9L4"></label> 
                                  <label id="Boxer9L5"></label> 
                            </div>
                            </div>
                        <div>
                            <div class="selectionWLD">
                                <div class="winDiv"><label id="WonBout5Box11" class="won">W</label></div>
                                <div class="winDivText"><label id="WonBout5Box1" class="wonT"></label></div>
                            </div>
                            <div class="selectionWLD">
                             <div class="LossDiv"><label id="LostBout5Box11" class="won">L</label></div>
                                <div class="LossDivText"><label id="LostBout5Box1" class="wonT"></label></div>    
                            </div>
                            <div class="selectionWLD">
                                                             <div class="DrawDiv"><label id="DrawnBout5Box11" class="won">D</label></div>
                                <div class="DrawDivText"><label id="DrawnBout5Box1" class="wonT"></label></div>    


                            </div>
                        </div>
                    </div>
                    <div class="boxerLeftImage">

                        <img src="" id="boxer9Src" class="boxerImage" />
                    </div>
                </div>
            </div>
            <div class="middleBout">
                <div class="centreText">
                    <label class="centreText">Winner</label>
                    <input id="Bout5Winner" />
                </div>
                <br />
                 
                <div class="centreText">
                    <label class="centreText">How</label>
                    <input id="Bout5How" />
                </div>
                <br />
                <div class="centreText">
                    <label class="centreText">Round</label>
                    <input id="Bout5Round" />
                </div>
             
            </div>
            <div class="boxerWrapperR">
                <div style="position: relative">
                    <div class="boxerDetailsRight">
                         <label id="Boxer10Name" class="boxerNameRight"></label>
                        <br />
                        <div class="borderSelection">
                        <label   id="Boxer10Hometown"></label>
                        <br /><br />
                         <label   id="Boxer10Age" class="boxerRightDeets"></label>
                        <br />
                        <label   id="Boxer10Height" class="boxerRightDeets"></label>
                        <br />
                        <label   id="Boxer10Reach" class="boxerRightDeets"></label>
                        <br />
                         <label   id="Boxer10Nation" class="boxerRightDeets"></label>
                        <br />
                         <label   id="Boxer10Stance"></label>
                       
                        <br />
                         <label   id="Boxer10Ko"></label>    
                                <div>
                                  <label id="Boxer10L1"></label> 
                                  <label id="Boxer10L2"></label> 
                                  <label id="Boxer10L3"></label> 
                                  <label id="Boxer10L4"></label> 
                                  <label id="Boxer10L5"></label> 
                            </div>
                            </div>
                                  <div>
                            <div class="selectionRightWLD">
                                <div class="DrawDiv">
                                     <label id="DrawnBout5Box21" class="won">D</label></div>
                                <div class="DrawDivText">
                                    <label id="DrawnBout5Box2" class="wonT"></label>
                                </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="LossDiv">
                                        <label id="LostBout5Box21" class="won">L</label></div>
                                    <div class="LossDivText">
                                        <label id="LostBout5Box2" class="wonT"></label>
                                    </div>
                                </div>
                                <div class="selectionRightWLD">
                                    <div class="winDiv">
                                         <label id="WonBout5Box21" class="won">W</label></div>
                                    <div class="winDivText">
                                        <label id="WonBout5Box2" class="wonT"></label>
                                    </div>
                                </div>
                          



                        </div>

                    </div>
                    <div class="boxerRightImage">

                        <img src="" id="boxer10Src" class="boxerImage" />
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div>
             <label class='centreText accountLabel' style='color:white'>Second(s) of first Winner Prediction</label><input id='WinnerSeconds' type="number" value="1" class='loginBox boxInput' style='border-color:white;'/>
            <br /><p class="centreText">The Seconds of the first Winner in any of the six Bouts. This will be used if a tie breaker is needed.<br /> Example : 200 would be 3 minutes and 33 seconds  </p>
        </div>

        <br style="clear: both;" />

    </div>
    <div id="SelectionSaveBtn" class="centreText">
        <input type="button" id="SaveSelectionBtn" value="Save Selection" class= "topMenu saveSelectionBtn k-button"; />
    </div> 
    <br />
    <br />
    <br />
    <br />

</asp:Content>
