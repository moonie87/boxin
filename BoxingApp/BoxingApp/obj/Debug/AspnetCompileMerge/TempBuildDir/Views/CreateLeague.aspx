﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="CreateLeague.aspx.cs" Inherits="BoxingApp.Views.CreateLeague" %>


<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">

    <script src="/Scripts/createLeague.js"></script>
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">
     <div class="headerTitleSelection">
        <h3 class="centreText">Create a League</h3>
    </div>  

    <div class="leagueBorder">
     <div style="text-align:center;">                  
                    <p>Compete against your friends, family and co workers</p>                   
                </div>       
        <div >
            <div class="centreText">
                <label class="centreText">Enater a League Name</label>
                <input type="text" id="createLeagueInput" class="loginBox" />
                <input id="createLeagueBtn" value="Create" class="topMenu centreText margintop5px"/>
            </div>
          
        </div>
    </div>

    </asp:Content>