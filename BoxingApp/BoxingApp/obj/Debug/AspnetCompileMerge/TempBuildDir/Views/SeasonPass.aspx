﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="SeasonPass.aspx.cs" Inherits="BoxingApp.Views.SeasonPass" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script src="/Scripts/paypal.js"></script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

    <div>
         <div class="headerTitleSelection">
            <h3 class="centreText">Purchase Season Pass</h3>
        </div>
        <div class="bodySelection">
    <div class="centreText" style="color:white;">
        <h3 class="centreText">A season lasts for 16 rounds, Each week there will be 5 bouts. </h3>
        <h3 class="centreText">The person(s) who correctly guesses the results will win prizes</h3><br />
        <h3 class="centreText">The person(s) who is at the top of the leaderboard will win a prize</h3><br />
        <h3 class="centreText">(The more people signed up for a season pass the better the prize's)</h3><br />
        <h3 class="centreText">What you get with a Season Pass</h3><br />        
        <h4 class="centreText" >A Chance of winning prizes (Prizes To be announced) </h4><br />
          <h4 class="centreText" >(Winners will be emailed for address to forward prizes to)</h4><br />
          <h4 class="centreText" >unlimited custom Leagues </h4><br />
        <h4 class="centreText">.......</h4><br />
        <h4 class="centreText">ALL FOR</h4>
        <h2 class="centreText">£2.99 per Season</h2><br />
        <%--<h3 class="centreText">Prizes</h3>
        <p>1st - 15% of total</p> 
        <p>2nd - 10% of total</p> 
        <p>3rd - 5% of total</p> 
        <p>**in a chance there is a draw at the end of the season the winnings will be split **</p><br />  --%>   
           <div id="paypal-button" style="display:table; margin:auto;"></div>
    </div>

  
            </div>
   </div>

</asp:Content>