﻿$(document).ready(function () {
    $("#LoginBtn").kendoButton({
        click: function() {
            LoginUser(null, null);
        }
    }); 
});

function LoginUser(outhProvider,outhId) {
    var userName;
    var password;
    if (outhProvider == null && outhId == null) {
         userName = $("#RegisterUserNameInput").val();
         password = $("#RegisterPasswordInput").val();
    }
    else {
        userName = null;
        password = null;
    }
   
        $.ajax({
            type: "post",
            data: JSON.stringify({
                userName: userName, Password: password,outhProvider:outhProvider,outhId
            }),
            url: "/Login.aspx/LoginUser",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object) {
                $.cookie("cookie",window.btoa(object.d),{path: '/'});
               

                var userD = getUserDetails(object.d);

                document.cookie = "user=" + '<p>' + userD[0].FirstName + ' ' + userD[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
            },
            complete: function ()
            {      
                window.location.href = "/Views/Selection.aspx";
                $("#signedIn").text("You are signed in");
                $("#loginDetails").css('display', "none");
                $("#socialLoginFB").css('display', 'none');

            },
            error: function (object) {
                console.log("error" + object);
            }
        });  
       
}

function getUserDetails(user)
{
    var Data;

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Login.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            Data = object.d;
        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return Data;

}