﻿$(document).ready(function () {

    $("#JoinLeagueBtn").kendoButton({
        click: function() {
            joinLeague();
        }
    });
});

function joinLeague() {

    var leagueName = $("#JoinLeagueInput").val();
    var user = parserId();
    var returnData;

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user,code: leagueName
        }),
        url: "/Views/Leagues.aspx/joinLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            returnData = object.d;
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    // added to league
    if (returnData == 1)
    {
        popupMessage("large", "Joined League", "You have joined the league");
        window.location.href = 'Leagues.aspx';  

        var data = populateUserDetails(false);
        emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Joined ", "<div style='margin-left:65px;'><h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
            "You joined a League <br> <br> Have Fun!</div > ");

    }
    // already belongs to league
    if (returnData == 2)
    {
        popupMessage("large", "Error Joining League", "You Already belong to this league!!");
    }

    // Code does not belong to a league
    if (returnData == 3) {
        popupMessage("large", "Error Joining League", "Invalid Code");
    }


}

