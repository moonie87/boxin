﻿$(document).ready(function () {
    var user = parserId();

    var day = moment().weekday();

    if (day == 6 || day == 0) {
        $("#bodySelection")[0].innerHTML = "<br><br><h2 class='centreText'>This weeks selections are Closed it Reopens Monday</h2><br><h2 class='centreText'>Selections close every Friday at 23:59 and reopen Monday</h2><br><h2 class='centreText'>To view your selections go to RESULTS</h2><br><br>";
        $("#SelectionSaveBtn")[0].innerHTML = "";
    }
    else {

        getboxerDetails('left');
        getboxerDetails('right');

        populateboxerDropdown(1);
        populateRounds(1);
        populateboxerDropdown(2);
        populateRounds(2);
        populateboxerDropdown(3);
        populateRounds(3);
        populateboxerDropdown(4);
        populateRounds(4);
        populateboxerDropdown(5);
        populateRounds(5);
        populateHowDropdowns();

       

        $("#SaveSelectionBtn").kendoButton({
            click: function () {
                SaveSelection(user);
            }
        });

        if (user == undefined) {
        }
        else {
            populateSelections(user);
        }
        populateSeasonAndWeek();
    }

});

function populateHowDropdowns() {
    var data = [
        { text: "Points", value: 1 },
        { text: "KO/TKO", value: 2 },
        { text: "Draw", value: 3 }
    ];

    $("#Bout1How").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onchange

    });
    $("#Bout2How").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onchange

    });
    $("#Bout3How").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onchange

    });
    $("#Bout4How").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onchange

    });
    $("#Bout5How").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onchange

    });
}

function populateSeasonAndWeek()
{
    $.ajax({
        type: "post",
        data: JSON.stringify({           
        }),
        url: "/Views/Selection.aspx/getActiveSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            $("#selSeason").text("Season: " + object.d[0].season);
            $("#selWeek").text("Week: " + object.d[0].week);
        },
        complete: function ()
        {

        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
}

function populateRounds(bout)
{
    try
    {
        $.ajax({
            type: "post",
            data: JSON.stringify({
                bout: bout
            }),
            url: "/Views/Selection.aspx/getRounds",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                var data = [];
                for (var i = 0; i < object.d.length; i++)
                {
                    data.push({ text: object.d[i].round, value: object.d[i].id });
                }

                if (bout == 1)
                {
                    $("#Bout1Round").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 2)
                {
                    $("#Bout2Round").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }

                if (bout == 3)
                {
                    $("#Bout3Round").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 4)
                {
                    $("#Bout4Round").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 5)
                {
                    $("#Bout5Round").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
            },
            complete: function ()
            {

            },
            error: function (object)
            {
                console.log("error" + object);
            }
        });
    }
    catch(populateRounds){}
}

function populateboxerDropdown(bout)
{
    try
    {
        $.ajax({
            type: "post",
            data: JSON.stringify({
                bout: bout
            }),
            url: "/Views/Selection.aspx/getBoxers",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                var data = [
                    { text: object.d[0].boxerName, value: object.d[0].boxer1Id },
                    { text: object.d[0].boxer2Name, value: object.d[0].boxer2Id },
                    { text: "Draw", value: 3 }

                ];
                if (bout == 1)
                {
                    $("#Bout1Winner").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 2)
                {
                    $("#Bout2Winner").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 3)
                {
                    $("#Bout3Winner").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 4)
                {
                    $("#Bout4Winner").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
                if (bout == 5)
                {
                    $("#Bout5Winner").kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: data,
                        index: 0,
                        change: onchange

                    });
                }
            },
            complete: function ()
            {

            },
            error: function (object)
            {
                console.log("error" + object);
            }
        });
    }
    catch(populateBoxers){}
}

function onchange() {
    // if points is selected
    try
    {
        var bout1How = $("#Bout1How").val();
        if (bout1How == 3 || bout1How == 1)

        var bout1How = $("#Bout1How").val();
        if (bout1How == 3 || bout1How == 1)
        {
            var rounds = $("#Bout1Round").data("kendoDropDownList").dataSource._data.length;
            $("#Bout1Round").data("kendoDropDownList").value(rounds);
            $("#Bout1Round").data("kendoDropDownList").enable(false);
        }
        else
        {
            $("#Bout1Round").data("kendoDropDownList").enable(true);
        }
        var bout2How = $("#Bout2How").val();
        if (bout2How == 3 || bout2How == 1)
        {
            var rounds = $("#Bout2Round").data("kendoDropDownList").dataSource._data.length;
            $("#Bout2Round").data("kendoDropDownList").value(rounds);
            $("#Bout2Round").data("kendoDropDownList").enable(false);
        }
        else
        {
            $("#Bout2Round").data("kendoDropDownList").enable(true);
        }
        var bout3How = $("#Bout3How").val();
        if (bout3How == 3 || bout3How == 1)
        {
            var rounds = $("#Bout3Round").data("kendoDropDownList").dataSource._data.length;
            $("#Bout3Round").data("kendoDropDownList").value(rounds);
            $("#Bout3Round").data("kendoDropDownList").enable(false);
        }
        else
        {
            $("#Bout3Round").data("kendoDropDownList").enable(true);
        }
        var bout4How = $("#Bout4How").val();
        if (bout4How == 3 || bout4How == 1)
        {
            var rounds = $("#Bout4Round").data("kendoDropDownList").dataSource._data.length;
            $("#Bout4Round").data("kendoDropDownList").value(rounds);
            $("#Bout4Round").data("kendoDropDownList").enable(false);
        }
        else
        {
            $("#Bout4Round").data("kendoDropDownList").enable(true);
        }
        var bout5How = $("#Bout5How").val();
        if (bout5How == 3 || bout5How == 1)
        {
            var rounds = $("#Bout5Round").data("kendoDropDownList").dataSource._data.length;
            $("#Bout5Round").data("kendoDropDownList").value(rounds);
            $("#Bout5Round").data("kendoDropDownList").enable(false);
        }
        else
        {
            $("#Bout5Round").data("kendoDropDownList").enable(true);
        }
    }
    catch(onChangeDropdown){}

  }

function getboxerDetails(leftorRight) {
    $.ajax({
        type: "post",
        data: JSON.stringify({
            leftorRight: leftorRight
        }),
        url: "/Views/Selection.aspx/GetBoutDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            var data = object.d;
            for (var i = 0; i < data.length; i++) {
                if (leftorRight == 'left') {
                    if (i == 0) {
                        var date = moment(data[0].Date).format('ddd-Do-MMM-YYYY');
                        $("#bout1Boxer1").text(data[i].Name);
                        $("#bout1Date").text(date);
                        $("#Boxer1Name").text(data[i].Name);
                        $("#bout1Div").text(data[i].division);
                        $("#Boxer1Hometown").text(data[i].residence);
                        $("#Boxer1Height").text("Height - " + data[i].height);
                        $("#Boxer1Reach").text("Reach - " + data[i].reach);
                        $("#boxer1Src").attr('src', (data[i].image));
                        $("#WonBout1Box1").text(data[i].won);
                        $("#LostBout1Box1").text(data[i].lost);
                        $("#DrawnBout1Box1").text(data[i].drawn);
                        $("#Boxer1Age").text("Age - " + data[i].age);
                        $("#Boxer1Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer1Stance").text("Stance - " + data[i].stance);
                        $("#Boxer1Ko").text("KO% - " + data[i].ko + "%");   
                        $("#bout1Loc").text( data[i].location);
                        $("#bout1TV").text("Where to watch: " + data[i].televised);
                        $("#Title1Div").text("Title: " + data[i].title);

                        $("#Boxer1L1").text(data[i].last1);
                        var bl1 = returnclassonValue(data[i].last1);
                        $("#Boxer1L1").addClass(bl1);

                        $("#Boxer1L2").text(data[i].last2);
                        var bl2 = returnclassonValue(data[i].last2);
                        $("#Boxer1L2").addClass(bl2);

                        $("#Boxer1L3").text(data[i].last3);
                        var bl3 = returnclassonValue(data[i].last3);
                        $("#Boxer1L3").addClass(bl3);

                        $("#Boxer1L4").text(data[i].last4);
                        var bl4 = returnclassonValue(data[i].last4);
                        $("#Boxer1L4").addClass(bl4);

                        $("#Boxer1L5").text(data[i].last5);
                        var bl5 = returnclassonValue(data[i].last5);
                        $("#Boxer1L5").addClass(bl5);
                    }
                    if (i == 1) {
                        var date = moment(data[1].Date).format('ddd-Do-MMM-YYYY');
                        $("#bout2Date").text(date);
                        $("#bout2Boxer1").text(data[i].Name);
                        $("#bout2Div").text(data[i].division);
                        $("#Boxer3Name").text(data[i].Name);
                        $("#Boxer3Hometown").text(data[i].residence);
                        $("#Boxer3Height").text("Height - " + data[i].height);
                        $("#Boxer3Reach").text("Reach - " + data[i].reach);
                        $("#boxer3Src").attr('src', (data[i].image));
                        $("#WonBout2Box1").text(data[i].won);
                        $("#LostBout2Box1").text(data[i].lost);
                        $("#DrawnBout2Box1").text(data[i].drawn);
                        $("#Boxer3Age").text("Age - " + data[i].age);
                        $("#Boxer3Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer3Stance").text("Stance - " + data[i].stance);
                        $("#Boxer3Ko").text("KO% - " + data[i].ko + "%"); 
                        $("#bout2Loc").text(data[i].location);
                        $("#bout2TV").text("Where to watch: " + data[i].televised);
                        $("#Title2Div").text("Title: " + data[i].title);
                        $("#Boxer3L1").text(data[i].last1);
                        var b31 = returnclassonValue(data[i].last1);
                        $("#Boxer3L1").addClass(b31);

                        $("#Boxer3L2").text(data[i].last2);
                        var b32 = returnclassonValue(data[i].last2);
                        $("#Boxer3L2").addClass(b32);

                        $("#Boxer3L3").text(data[i].last3);
                        var b33 = returnclassonValue(data[i].last3);
                        $("#Boxer3L3").addClass(b33);

                        $("#Boxer3L4").text(data[i].last4);
                        var b34 = returnclassonValue(data[i].last4);
                        $("#Boxer3L4").addClass(b34);

                        $("#Boxer3L5").text(data[i].last5);
                        var b35 = returnclassonValue(data[i].last5);
                        $("#Boxer3L5").addClass(b35);
                        
                    }

                    if (i == 2) {
                        var date = moment(data[2].Date).format('ddd-Do-MMM-YYYY');
                        $("#bout3Date").text(date);
                        $("#bout3Boxer1").text(data[i].Name);
                        $("#bout3Div").text(data[i].division);
                        $("#Boxer5Name").text(data[i].Name);
                        $("#Boxer5Hometown").text(data[i].residence);
                        $("#Boxer5Height").text("Height - " + data[i].height);
                        $("#Boxer5Reach").text("Reach - " + data[i].reach);
                        $("#boxer5Src").attr('src', (data[i].image));
                        $("#WonBout3Box1").text(data[i].won);
                        $("#LostBout3Box1").text(data[i].lost);
                        $("#DrawnBout3Box1").text(data[i].drawn);
                        $("#Boxer5Age").text("Age - " + data[i].age);
                        $("#Boxer5Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer5Stance").text("Stance - " + data[i].stance);
                        $("#Boxer5Ko").text("KO% - " + data[i].ko + "%");   
                        $("#bout3Loc").text(data[i].location);
                        $("#bout3TV").text("Where to watch: " + data[i].televised);
                        $("#Title3Div").text("Title: " + data[i].title);

                        $("#Boxer5L1").text(data[i].last1);
                        var b51 = returnclassonValue(data[i].last1);
                        $("#Boxer5L1").addClass(b31);

                        $("#Boxer5L2").text(data[i].last2);
                        var b52 = returnclassonValue(data[i].last2);
                        $("#Boxer5L2").addClass(b52);

                        $("#Boxer5L3").text(data[i].last3);
                        var b53 = returnclassonValue(data[i].last3);
                        $("#Boxer5L3").addClass(b53);

                        $("#Boxer5L4").text(data[i].last4);
                        var b54 = returnclassonValue(data[i].last4);
                        $("#Boxer5L4").addClass(b54);

                        $("#Boxer5L5").text(data[i].last5);
                        var b55 = returnclassonValue(data[i].last5);
                        $("#Boxer5L5").addClass(b55);
                    }
                    if (i == 3) {
                        var date = moment(data[3].Date).format('ddd-Do-MMM-YYYY');
                        $("#bout4Date").text(date);
                        $("#bout4Boxer1").text(data[i].Name);
                        $("#bout4Div").text(data[i].division);
                        $("#Boxer7Name").text(data[i].Name);
                        $("#Boxer7Hometown").text(data[i].residence);
                        $("#Boxer7Height").text("Height - " + data[i].height);
                        $("#Boxer7Reach").text("Reach - " + data[i].reach);
                        $("#boxer7Src").attr('src', (data[i].image));
                        $("#WonBout4Box1").text(data[i].won);
                        $("#LostBout4Box1").text(data[i].lost);
                        $("#DrawnBout4Box1").text(data[i].drawn);
                        $("#Boxer7Age").text("Age - " + data[i].age);
                        $("#Boxer7Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer7Stance").text("Stance - " + data[i].stance);
                        $("#Boxer7Ko").text("KO% - " + data[i].ko + "%"); 
                        $("#bout4Loc").text(data[i].location);
                        $("#bout4TV").text("Where to watch: " + data[i].televised);
                        $("#Title4Div").text("Title: " + data[i].title);
                        $("#Boxer7L1").text(data[i].last1);
                        var b71 = returnclassonValue(data[i].last1);
                        $("#Boxer7L1").addClass(b71);

                        $("#Boxer7L2").text(data[i].last2);
                        var b72 = returnclassonValue(data[i].last2);
                        $("#Boxer7L2").addClass(b72);

                        $("#Boxer7L3").text(data[i].last3);
                        var b73 = returnclassonValue(data[i].last3);
                        $("#Boxer7L3").addClass(b73);

                        $("#Boxer7L4").text(data[i].last4);
                        var b74 = returnclassonValue(data[i].last4);
                        $("#Boxer7L4").addClass(b74);

                        $("#Boxer7L5").text(data[i].last5);
                        var b75 = returnclassonValue(data[i].last5);
                        $("#Boxer7L5").addClass(b75);
                      
                    }
                    if (i == 4)
                    {
                        var date = moment(data[4].Date).format('ddd-Do-MMM-YYYY');
                        $("#bout5Date").text(date);
                        $("#bout5Boxer1").text(data[i].Name);
                        $("#bout5Div").text(data[i].division);
                        $("#Boxer9Name").text(data[i].Name);
                        $("#Boxer9Hometown").text(data[i].residence);
                        $("#Boxer9Height").text("Height - " + data[i].height);
                        $("#Boxer9Reach").text("Reach - " + data[i].reach);
                        $("#boxer9Src").attr('src', (data[i].image));
                        $("#WonBout5Box1").text(data[i].won);
                        $("#LostBout5Box1").text(data[i].lost);
                        $("#DrawnBout5Box1").text(data[i].drawn);
                        $("#Boxer9Age").text("Age - " + data[i].age);
                        $("#Boxer9Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer9Stance").text("Stance - " + data[i].stance);
                        $("#Boxer9Ko").text("KO% - " + data[i].ko + "%");    
                        $("#bout5Loc").text(data[i].location);
                        $("#bout5TV").text("Where to watch: " + data[i].televised);
                        $("#Title5Div").text("Title: " + data[i].title);
                        $("#Boxer9L1").text(data[i].last1);
                        var b91 = returnclassonValue(data[i].last1);
                        $("#Boxer9L1").addClass(b91);

                        $("#Boxer9L2").text(data[i].last2);
                        var b92 = returnclassonValue(data[i].last2);
                        $("#Boxer9L2").addClass(b92);

                        $("#Boxer9L3").text(data[i].last3);
                        var b93 = returnclassonValue(data[i].last3);
                        $("#Boxer9L3").addClass(b93);

                        $("#Boxer9L4").text(data[i].last4);
                        var b94 = returnclassonValue(data[i].last4);
                        $("#Boxer9L4").addClass(b94);

                        $("#Boxer9L5").text(data[i].last5);
                        var b95 = returnclassonValue(data[i].last5);
                        $("#Boxer9L5").addClass(b95);
                    }
                }
                if(leftorRight == 'right')
                {
                    if (i == 0) {
                        $("#bout1Boxer2").text(data[i].Name);
                        $("#Boxer2Name").text(data[i].Name);
                        $("#Boxer2Hometown").text(data[i].residence);
                        $("#Boxer2Height").text("Height - " + data[i].height);
                        $("#Boxer2Reach").text("Reach - " + data[i].reach);
                        $("#boxer2Src").attr('src', (data[i].image));
                        $("#WonBout1Box2").text(data[i].won);
                        $("#LostBout1Box2").text(data[i].lost);
                        $("#DrawnBout1Box2").text(data[i].drawn);
                        $("#Boxer2Age").text("Age - " + data[i].age);
                        $("#Boxer2Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer2Stance").text("Stance - " + data[i].stance);
                        $("#Boxer2Ko").text("KO% - " + data[i].ko + "%");   
                        $("#Boxer2L1").text(data[i].last1);
                        var b21 = returnclassonValue(data[i].last1);
                        $("#Boxer2L1").addClass(b21);

                        $("#Boxer2L2").text(data[i].last2);
                        var b22 = returnclassonValue(data[i].last2);
                        $("#Boxer2L2").addClass(b22);

                        $("#Boxer2L3").text(data[i].last3);
                        var b23 = returnclassonValue(data[i].last3);
                        $("#Boxer2L3").addClass(b23);

                        $("#Boxer2L4").text(data[i].last4);
                        var b24 = returnclassonValue(data[i].last4);
                        $("#Boxer2L4").addClass(b24);

                        $("#Boxer2L5").text(data[i].last5);
                        var b25 = returnclassonValue(data[i].last5);
                        $("#Boxer2L5").addClass(b25);
                    }
                    if (i == 1) {
                        $("#bout2Boxer2").text(data[i].Name);
                        $("#Boxer4Name").text(data[i].Name);
                        $("#Boxer4Hometown").text(data[i].residence);
                        $("#Boxer4Height").text("Height - " + data[i].height);
                        $("#Boxer4Reach").text("Reach - " + data[i].reach);
                        $("#boxer4Src").attr('src', (data[i].image));
                        $("#WonBout2Box2").text(data[i].won);
                        $("#LostBout2Box2").text(data[i].lost);
                        $("#DrawnBout2Box2").text(data[i].drawn);
                        $("#Boxer4Age").text("Age - " + data[i].age);
                        $("#Boxer4Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer4Stance").text("Stance - " + data[i].stance);
                        $("#Boxer4Ko").text("KO% - " + data[i].ko + "%");      
                        $("#Boxer4L1").text(data[i].last1);
                        var b41 = returnclassonValue(data[i].last1);
                        $("#Boxer4L1").addClass(b41);

                        $("#Boxer4L2").text(data[i].last2);
                        var b42 = returnclassonValue(data[i].last2);
                        $("#Boxer4L2").addClass(b42);

                        $("#Boxer4L3").text(data[i].last3);
                        var b43 = returnclassonValue(data[i].last3);
                        $("#Boxer4L3").addClass(b43);

                        $("#Boxer4L4").text(data[i].last4);
                        var b44 = returnclassonValue(data[i].last4);
                        $("#Boxer4L4").addClass(b44);

                        $("#Boxer4L5").text(data[i].last5);
                        var b45 = returnclassonValue(data[i].last5);
                        $("#Boxer4L5").addClass(b45);
                    }

                    if (i == 2) {
                        $("#bout3Boxer2").text(data[i].Name);
                        $("#Boxer6Name").text(data[i].Name);
                        $("#Boxer6Hometown").text(data[i].residence);
                        $("#Boxer6Height").text("Height - " + data[i].height);
                        $("#Boxer6Reach").text("Reach - " + data[i].reach);
                        $("#boxer6Src").attr('src', (data[i].image));
                        $("#WonBout3Box2").text(data[i].won);
                        $("#LostBout3Box2").text(data[i].lost);
                        $("#DrawnBout3Box2").text(data[i].drawn);
                        $("#Boxer6Age").text("Age - " + data[i].age);
                        $("#Boxer6Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer6Stance").text("Stance - " + data[i].stance);
                        $("#Boxer6Ko").text("KO% - " + data[i].ko + "%"); 
                        $("#Boxer6L1").text(data[i].last1);
                        var b61 = returnclassonValue(data[i].last1);
                        $("#Boxer6L1").addClass(b61);

                        $("#Boxer6L2").text(data[i].last2);
                        var b62 = returnclassonValue(data[i].last2);
                        $("#Boxer6L2").addClass(b62);

                        $("#Boxer6L3").text(data[i].last3);
                        var b63 = returnclassonValue(data[i].last3);
                        $("#Boxer6L3").addClass(b63);

                        $("#Boxer6L4").text(data[i].last4);
                        var b64 = returnclassonValue(data[i].last4);
                        $("#Boxer6L4").addClass(b64);

                        $("#Boxer6L5").text(data[i].last5);
                        var b65 = returnclassonValue(data[i].last5);
                        $("#Boxer6L5").addClass(b65);
                    }
                    if (i == 3) {
                        $("#bout4Boxer2").text(data[i].Name);
                        $("#Boxer8Name").text(data[i].Name);
                        $("#Boxer8Hometown").text(data[i].residence);
                        $("#Boxer8Height").text("Height - " + data[i].height);
                        $("#Boxer8Reach").text("Reach - " + data[i].reach);
                        $("#boxer8Src").attr('src', (data[i].image));
                        $("#WonBout4Box2").text(data[i].won);
                        $("#LostBout4Box2").text(data[i].lost);
                        $("#DrawnBout4Box2").text(data[i].drawn);
                        $("#Boxer8Age").text("Age - " + data[i].age);
                        $("#Boxer8Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer8Stance").text("Stance - " + data[i].stance);
                        $("#Boxer8Ko").text("KO% - " + data[i].ko + "%");   
                        $("#Boxer8L1").text(data[i].last1);
                        var b81 = returnclassonValue(data[i].last1);
                        $("#Boxer8L1").addClass(b81);

                        $("#Boxer8L2").text(data[i].last2);
                        var b82 = returnclassonValue(data[i].last2);
                        $("#Boxer8L2").addClass(b82);

                        $("#Boxer8L3").text(data[i].last3);
                        var b83 = returnclassonValue(data[i].last3);
                        $("#Boxer8L3").addClass(b83);

                        $("#Boxer8L4").text(data[i].last4);
                        var b84 = returnclassonValue(data[i].last4);
                        $("#Boxer8L4").addClass(b84);

                        $("#Boxer8L5").text(data[i].last5);
                        var b85 = returnclassonValue(data[i].last5);
                        $("#Boxer8L5").addClass(b85);
                    }
                    if (i == 4)
                    {              
                        $("#bout5Boxer2").text(data[i].Name);
                        $("#Boxer10Name").text(data[i].Name);
                        $("#Boxer10Hometown").text(data[i].residence);
                        $("#Boxer10Height").text("Height - " + data[i].height);
                        $("#Boxer10Reach").text("Reach - " + data[i].reach);
                        $("#boxer10Src").attr('src', (data[i].image));
                        $("#WonBout5Box2").text(data[i].won);
                        $("#LostBout5Box2").text(data[i].lost);
                        $("#DrawnBout5Box2").text(data[i].drawn);
                        $("#Boxer10Age").text("Age - " + data[i].age);
                        $("#Boxer10Nation").text("Nationality - " + data[i].nationality);
                        $("#Boxer10Stance").text("Stance - " + data[i].stance);
                        $("#Boxer10Ko").text("KO% - " + data[i].ko + "%");   
                        $("#Boxer10L1").text(data[i].last1);
                        var b101 = returnclassonValue(data[i].last1);
                        $("#Boxer10L1").addClass(b101);

                        $("#Boxer10L2").text(data[i].last2);
                        var b102 = returnclassonValue(data[i].last2);
                        $("#Boxer10L2").addClass(b102);

                        $("#Boxer10L3").text(data[i].last3);
                        var b103 = returnclassonValue(data[i].last3);
                        $("#Boxer10L3").addClass(b103);

                        $("#Boxer10L4").text(data[i].last4);
                        var b104 = returnclassonValue(data[i].last4);
                        $("#Boxer10L4").addClass(b104);

                        $("#Boxer10L5").text(data[i].last5);
                        var b105 = returnclassonValue(data[i].last5);
                        $("#Boxer10L5").addClass(b105);
                    }
                }             
            }
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

function SaveSelection(user)
{
    var anyErrors = false;
    var error = "";

    var counter = 1;
    //bout1
    var boxer1 = $("#Bout1Winner").val();
    var round1 = $("#Bout1Round").val();
    var how1 = $("#Bout1How").val();

    if (boxer1 == 3 & how1 != 3)
    {
        anyErrors = true;
        error += "Bout 1 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br>";
    }
    if (boxer1 != 3 & how1 == 3)
    {
        anyErrors = true;
        error += "Bout 1 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW</br> ";
    }

    //bout2
    var boxer2 = $("#Bout2Winner").val();
    var round2 = $("#Bout2Round").val();
    var how2 = $("#Bout2How").val();
    if (boxer2 == 3 & how2 != 3)
    {
        anyErrors = true;
        error += "Bout 2 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br>";
    }
    if (boxer2 != 3 & how2 == 3)
    {
        anyErrors = true;
        error += "Bout 2 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br>";
    }
    //bout3
    var boxer3 = $("#Bout3Winner").val();
    var round3 = $("#Bout3Round").val();
    var how3 = $("#Bout3How").val();
    if (boxer3 == 3 & how3 != 3)
    {
        anyErrors = true;
        error += "Bout 3 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br> ";
    }
    if (boxer3 != 3 & how3 == 3)
    {
        anyErrors = true;
        error += "Bout 2 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br> ";
    }
    //bout4
    var boxer4 = $("#Bout4Winner").val();
    var round4 = $("#Bout4Round").val();
    var how4 = $("#Bout4How").val();
    if (boxer4 == 3 & how4 != 3)
    {
        anyErrors = true;
        error += "Bout 4 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br>";
    }
    if (boxer4 != 3 & how4 == 3)
    {
        anyErrors = true;
        error += "Bout 4 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br> ";
    }
    //bout5
    var boxer5 = $("#Bout5Winner").val();
    var round5 = $("#Bout5Round").val();
    var how5 = $("#Bout5How").val();
    if (boxer5 == 3 & how5 != 3)
    {
        anyErrors = true;
        error += "Bout 5 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br> ";
    }
    if (boxer5 != 3 & how5 == 3)
    {
        anyErrors = true;
        error += "Bout 5 Error: Draw Selected - Both 'Winner' and 'How' dropdowns need to be set to DRAW </br> ";
    }

   var tieBreaker = $("#WinnerSeconds").val();

    if (anyErrors == true)
    {
        bootbox.alert({
            message: error + "</br> Please change these and re-submit",
            size: 'large'
        });
    }
    else
    {
        while (counter <= 5)
        {
            if (counter == 1)
            {
                saveprocess(user, 1, round1, how1, boxer1, tieBreaker);
            }
            if (counter == 2)
            {
                saveprocess(user, 2, round2, how2, boxer2, tieBreaker);
            }
            if (counter == 3)
            {
                saveprocess(user, 3, round3, how3, boxer3, tieBreaker);
            }
            if (counter == 4)
            {
                saveprocess(user, 4, round4, how4, boxer4, tieBreaker);
            }
            if (counter == 5)
            {
                saveprocess(user, 5, round5, how5, boxer5, tieBreaker);
            }

            counter++;
        }
        {
            bootbox.alert({
                message: "Selections Saved",
                size: 'large'
            });
        }
    }
}

function saveprocess(user,bout,round,outcome,boxer,tieBreaker) {
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user,
            bout: bout,
            round: round,
            outcome: outcome,
            boxer: boxer,
            tieBreaker: tieBreaker
        }),
        url: "/Views/Selection.aspx/saveSelection",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function () {
            console.log("selection saved");
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

function populateSelections(user) {
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user            
        }),
        url: "/Views/Selection.aspx/getuserSelctions",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            try
            {
                // rounds
                var bout1Round = $("#Bout1Round").data("kendoDropDownList");
                bout1Round.value(object.d[0].round);
                var bout2Round = $("#Bout2Round").data("kendoDropDownList");
                bout2Round.value(object.d[1].round);
                var bout3Round = $("#Bout3Round").data("kendoDropDownList");
                bout3Round.value(object.d[2].round);
                var bout4Round = $("#Bout4Round").data("kendoDropDownList");
                bout4Round.value(object.d[3].round);
                var bout5Round = $("#Bout5Round").data("kendoDropDownList");
                bout5Round.value(object.d[4].round);
                //boxers
                var bout1Boxers = $("#Bout1Winner").data("kendoDropDownList");
                bout1Boxers.value(object.d[0].boxer);
                var bout2Boxers = $("#Bout2Winner").data("kendoDropDownList");
                bout2Boxers.value(object.d[1].boxer);
                var bout3Boxers = $("#Bout3Winner").data("kendoDropDownList");
                bout3Boxers.value(object.d[2].boxer);
                var bout4Boxers = $("#Bout4Winner").data("kendoDropDownList");
                bout4Boxers.value(object.d[3].boxer);
                var bout5Boxers = $("#Bout5Winner").data("kendoDropDownList");
                bout5Boxers.value(object.d[4].boxer);
                // outcome
                var bout1out = $("#Bout1How").data("kendoDropDownList");
                bout1out.value(object.d[0].outcome);
                var bout2out = $("#Bout2How").data("kendoDropDownList");
                bout2out.value(object.d[1].outcome);
                var bout3out = $("#Bout3How").data("kendoDropDownList");
                bout3out.value(object.d[2].outcome);
                var bout4out = $("#Bout4How").data("kendoDropDownList");
                bout4out.value(object.d[3].outcome);
                var bout5out = $("#Bout5How").data("kendoDropDownList");
                bout5out.value(object.d[4].outcome);

                $("#WinnerSeconds").val(object.d[9].tieBreaker);

            }
            catch (selections) { }
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });
}

function returnclassonValue(value)
{
    if (value == "W")
    {
        return "Wins";
    }
    if (value == "L")
    {
        return "loss";
    }
    if (value == "D")
    {
        return "draw";
    }
}