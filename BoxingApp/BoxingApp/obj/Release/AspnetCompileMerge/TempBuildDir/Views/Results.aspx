﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Results.aspx.cs" Inherits="BoxingApp.Views.Results" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server"> 
      
    <script src="/Scripts/Results.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
</asp:Content>
   
<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

    
    <div class="headerTitleSelection">
        <h3 class="centreText">Results</h3>
    </div>
    <div class="LeaderboardBorder">
    <div>       
       
  <div id="weeks">

            </div>
    </div>
    <div class="resultDiv">
    <div class="resultDivDiv">
        <div class="headerTitleSelection">
            <label class="centreText">Actual Results</label>
            </div>
    <div id="results" >
    
            </div>
        </div>
       
    <div class="left50">
         <div class="headerTitleSelection">
            <label class="centreText">Your Predicted Results</label>
            </div>
      <div id="predictResults" >
         
            </div>
   </div>
     
        </div>
        </div>

           <div class="headerTitleSelection">
            <h3 class="centreText">Weeks Points Breakdown</h3>
        </div>
    <div class="LeaderboardBorder">
        <h4 class="centreText">Total Points</h4>
        <label id="pointsValue" class="centreText"></label>
        <br />
        <h4 class="centreText">Correct Boxers</h4>
          <label id="boxersValue" class="centreText"></label>
        <br />
        <h4 class="centreText">Correct Outcomes</h4>
          <label id="outcomesValue" class="centreText"></label>
        <br />
        <h4 class="centreText">Correct Rounds</h4>
          <label id="roundsValue" class="centreText"></label>
    </div>
    <%-- <img class="sticky" src="../Content/logoresized.png" alt="Avatar"/>--%>
    
    </asp:Content>