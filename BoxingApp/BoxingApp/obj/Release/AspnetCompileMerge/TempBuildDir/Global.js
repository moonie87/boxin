﻿(function ($)
{
    $(document).ready(function ()
    {
        $('#cssmenu').prepend('<div id="menu-button">Menu</div>');
        $('#cssmenu #menu-button').on('click', function ()
        {
            var menu = $(this).next('ul');
            if (menu.hasClass('open'))
            {
                menu.removeClass('open');
            }
            else
            {
                menu.addClass('open');
            }
        });
    });
})(jQuery);

$(document).ready(function ()
{   

    $(document).keypress(function (e)
    {
        if (e.which == 13)
        {
            if ($("#RegisterPasswordInput").is(":focus"))
            {
                LoginUser(null,null);}
            }
    });   




    var user = parserId(); 

    if (user == 1)
    {
        $("#Admin").css("display", "block");
    }

    $("#Knives").kendoButton({
        click: function () {
            window.location.href = "/Views/swiftKnivesuk.aspx";
        }
    });

    $("#Feedback").click(function(){
       
            feedback();
        
    });

    $("#updateAccountDetails").click(function(){
        
            updateAccountDetails();
        
    });  
    
    $("#Terms").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Terms.aspx";
        }
    });

    $("#Privacy").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Privacy.aspx";
        }
    });

    $("#Unsubscribe").kendoButton({
        click: function ()
        {
            window.location.href = "/Views/Unsubscribe.aspx";
        }
    });
    
    $("#SeasonPassLeaderboard").click(function(){
        
            window.location.href = "/Views/SeasonPassLeaderboard.aspx";
        
    });

    $("#HomeRegisterBtn").click(function(){
      
            window.location.href = "Views/Register.aspx";
        
    });

    

    $("#RegisterSigninBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#LoginRegisterBtn").kendoButton({
        click: function () {
            window.location.href = "/Views/Register.aspx";
        }
    });

   
    $("#HomeLoginBtn").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Home").click(function(){
       
            window.location.href = "/Index.aspx";
            var user = parserId();
       
    });

    $("#Admin").click(function ()
    {        
        window.location.href = "/Views/Admin.aspx?LC=b5105";  

    });

    $("#SeasonPass").click(function(){       
            window.location.href = "/Views/SeasonPass.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#rankings").click(function(){
       
            window.location.href = "/Views/rankings.aspx";
            var user = parserId();

            if (user === -1) {
                window.location.href = "/Login.aspx";
            }
        
    });

    $("#Selection").click(function(){       
            window.location.href = "/Views/Selection.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leaderboard").click(function(){       
            window.location.href = "/Views/Leaderboard.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#Leagues").click(function(){       
            window.location.href = "/Views/Leagues.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }
        
    });


    $("#Register").click(function(){
       
            window.location.href = "/Views/Register.aspx";
        
    });

    $("#About").click(function () {

        window.location.href = "/Views/about.aspx";

    });

    $("#Login").click(function(){
       
            window.location.href = "/Login.aspx";
        
    });

    $("#Results").click(function(){       
            window.location.href = "/Views/Results.aspx";
            var user = parserId();

            if (user == -1) {
                window.location.href = "/Login.aspx";
            }        
    });

    $("#logOut").click(function(){    

           
            window.location.href = "/Index.aspx";
            fbLogout();
        
    });

    var cookie = $.cookie('user');  

    $("#userData").html(cookie);

    if (cookie == undefined)
    {       
        try {
            $("#account")[0].style.display = "none";
            $("#userData").html("");
           
            
            $("#logOut")[0].style.display = "none";
            $("#updateAccountDetails")[0].style.display = "none";
           // $("#SeasonPass").css("display", "none");
          //  $("#SeasonPassLeaderboard").css("display", "none");
            $("#HomeRegisterBtn").css("display", "initial");
            $("#HomeLoginBtn").css("display", "initial");
             $("#Login")[0].style.display = "block"
            $("#Register")[0].style.display = "block";
        }
        catch (test) {}
    }
    else
    {
        $("#account")[0].style.display = "block";
      
        $("#logOut").css("display", "block");
        $("#updateAccountDetails").css("display", "block");
       // $("#SeasonPass").css("display", "block");
        $("#HomeRegisterBtn").css("display", "none");
        $("#HomeLoginBtn").css("display", "none");
       // $("#SeasonPassLeaderboard").css("display", "none");
          $("#Login").css("display", "none");
       $("#Register").css("display", "none");
    }
    var season = CheckSeasonForUSer();
    //if (season == true)
    //{
    //    $("#SeasonPass").css("display", "none");
    //    $("#seasonLabel")[0].innerText = "Season Pass Active";
    //    $("#SeasonPassLeaderboard").css("display", "block");

    //}


});

function parserId() {
    var userID1 = $.cookie("cookie");
    try {
       
        if (userID1 == undefined) {
            userID1 = -1;
        }
        else {
            var cookieDec = window.atob(userID1);
            userID1 = parseInt(cookieDec);
        }
    }
    catch (id) { }
    return userID1;
}

window.fbAsyncInit = function () {
    // FB JavaScript SDK configuration and setup
    FB.init({
        appId: '1539533289441157', // FB App ID
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });

    //// Check whether the user already logged in
    //FB.getLoginStatus(function (response) {
    //    if (response.status === 'connected') {
    //        //display user data
    //        getFbUserData();
    //    }
    //});
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function RegisterFb() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getregisterData();
        } else {
            document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, { scope: 'email' });
}

function getFbUserData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
        function (response)
        {
            try
            {
                LoginUser("Facebook", response.id);
            }
            catch(FB){}
        document.cookie = "user=" + '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img class=imageRadius src="' + response.picture.data.url + '"/></p>';
        //document.getElementById('userData').innerHTML = '<p>' + response.first_name + ' ' + response.last_name + '&nbsp<img src="' + response.picture.data.url + '"/></p>';
        $("#Login").css("display", "None");
        $("#Register").css("display", "None");      
    });
}

function getregisterData() {
    FB.api('/me', { locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture' },
function (response) {
    RegisterUser(response.email, response.first_name, response.last_name, 'Facebook', response.id);
});
}

function fbLogout() {
    FB.logout(function () {
        document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
        document.getElementById('fbLink').innerHTML = '<img src="//login.create.net/images/icons/user/facebook_30x30.png"/>';
        document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
        $("#Login").css("display", "inline");
        $("#Register").css("display", "inline");        
    });
    $.removeCookie('cookie', { path: '/' });
    $.removeCookie('user', { path: '/' });
    $.removeCookie('fbm_1539533289441157', { path: '/' });
    $.removeCookie('user', { path: '/Views' });
}

function popupMessage(size, title, message)
{
    bootbox.alert({
        size: size,
        title: title,
        message: message
    });
}

function updateAccountDetails()
{
    
        // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='example'><div class='demo-section k-content'><div style='height:400px;'><div><label class='centreText accountLabel'>First Name</label>\
                                 <input id='firstName' type='text' name='firstName' class='loginBox boxInput'/><br>\
                                <label class='centreText accountLabel'>Last Name</label><input id='lastName' type='text' name='Lastname' class='loginBox boxInput' /><br>\
                                <label class='centreText accountLabel'>User Name</label><input id='UserName' type='text' name='UserName' class='loginBox boxInput'/><label id='usernameError' class='errorTextHide' >UserName Unavailable</label><br>\
                                <label class='centreText accountLabel'>Email</label><input id='Email' type='text' name='Email' class='loginBox boxInput'/><label id='emailError' class='errorTextHide'>User already exists with that email</label><br>\
        <label class='centreText'>No matter if you opt in or opt out You will recieve emails for season Pass reciept,</label><label class='centreText'> change of password and details updated </label><br>\
        <label class='accountLabel' style= 'margin-left:37%; margin-top:3%; margin-bottom:3%;' for='eq1' > Recieve Emails</label > <input type='checkbox' id='eq1' class='k-checkbox' style='margin-left:5%; checked=' checked'><br>\
                                 <br> <input type='button' runat='server' id='UpdateDetails' value='UpdateDetails' onclick='updateDetails()' class='loginBtn k-button accountBtn' /> <br>\
                                 <br><label class='centreText accountLabelHeader'>Change Password</label><br><label class='centreText accountLabel'>New Password</label>\
                                 <input id='newPassword' type='text' name='newPassword' class='loginBox boxInput'/><br>\
                                 <label class='centreText accountLabel'>Confirm Password</label><input id='ConfirmPassword' type='text' name='ConfirmPassword' class='loginBox boxInput'/><br>\
                                <label class='errorTextHide' id='updatePasswordMessage'></label>\
                                 <br><input type='button' runat='server' id='ChangePassword' value='ChangePassword' onclick='changePassword()' class='loginBtn k-button accountBtn'/>\
                                </br></br></div></div>";

        kendoWindow = $("<div id='window'/>").kendoWindow({
            title: "Update Account Details",
            width: "90%",
            height: "90%",
            resizable: false,
            modal: true,
            content: {
                template: tabTemplate
            },
            deactivate: function (e)
            {
                e.sender.destroy();
            }

        }).data("kendoWindow");

        var tab = $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            },
        });       

        kendoWindow.open().center();

        populateUserDetails(true);
}

function feedback() {
    // removed <li>\upgrade</li> for version 1
    var tabTemplate = "<div id='FeedbackWin' class='redBackground'><div class='demo-section k-content redBackground'><div style='height:400px;' class='redBackground'><div class='redBackground'><label class='centreText accountLabel' style='color:white;'>Tell us about</label>\
                                 <input id='IssueDd' type='text' name='IssueDd' class='loginBox boxInput' style='border-color:white;'/><br>\
                                <label class='centreText accountLabel' style='color:white;'>Feedback</label> <textarea id='tx' rows='25' class='textAreaFeedback centreText' placeholder='Enter your message here .....' ></textarea><br>\
                                <label class='centreText accountLabel' style='color:white'>How would you rate us</label><input id='0to10' name='0to10' class='loginBox boxInput' style='border-color:white;'/></br></br>\
    <button type='button' id='SubmitFeedbackBtn' onclick='submitFeedback()' class='topMenu centreText k-button' style='width:95%;'>Submit</button></div ></div > ";

    kendoWindow = $("<div id='FeedbackWindow'/>").kendoWindow({
        title: "Feedback",
        width: "90%",
        height: "90%",
        resizable: false,
        modal: true,
        content: {
            template: tabTemplate
        }

    }).data("kendoWindow");

    kendoWindow.open().center();

    var IssueData = [
        { text: "Something I Like", value: "1" },
        { text: "Something that could be better", value: "2" },
        { text: "Technichal issue ", value: "3" },
        { text: "Something Different", value: "4" }
    ];

    $("#IssueDd").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: IssueData,
        index: 0,
        change: onchange

    });

    var zeroToTenData = [
        { text: "0", value: "1" },
        { text: "1", value: "2" },
        { text: "2", value: "3" },
        { text: "3", value: "4" },
        { text: "4", value: "5" },
        { text: "5", value: "6" },
        { text: "6", value: "7" },
        { text: "7", value: "8" },
        { text: "8", value: "9" },
        { text: "9", value: "10" },
        { text: "10", value: "11" }
        
    ];

    $("#0to10").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: zeroToTenData,
        index: 6,
        change: onchange

    });
}

function submitFeedback() {

    var type = $("#IssueDd").data("kendoDropDownList").text();
    var rating = $("#0to10").data("kendoDropDownList").text();
    var message = $("#tx").val().replace(/(\r\n|\n|\r)/gm, '<br>');

    emailNotification("feedback@boxing5.com", "Boxing5", "feedback@boxing5.com", "Feedback", "type:" +type + "<br> rating:" + rating + "<br>Message:" + message);

    popupMessage("large", "Feedback Left", "Thank you for your feedback. It is very much appreciated.");
    var mywindow = $("#FeedbackWindow").data("kendoWindow");
    mywindow.close();

}

function changePassword(){
    //var code = $("#ResetCode").val();
    var user = parserId();
    var pw = $("#newPassword").val();
    var pw1 = $("#ConfirmPassword").val();
if (pw != pw1)
{
    $("#updatePasswordMessage")[0].innerText = "Passwords Do Not Match";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
    
    return false;
}
if (pw.length < 8)
{
    $("#updatePasswordMessage")[0].innerText = "Password must be atleast 8 characters long.";
    $("#updatePasswordMessage").removeClass("errorTextHide");
    $("#updatePasswordMessage").addClass("errorText centreText");
  
    return false;
}
$.ajax({
    type: "post",
    data: JSON.stringify({
       // code: code,
        user:user,
        password: pw
    }),
    url: "/Views/results.aspx/UpdateUserpassword",
    dataType: "json",
    async: false,
    contentType: "application/json",
    success: function (object)
    {
        var data = populateUserDetails(false);
        popupMessage("large", "Password Changed", "Your Password has been changed");
        emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Password Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Password has been updated");

    },
    complete: function (object)
    {
        var mywindow = $("#window").data("kendoWindow");
        mywindow.close();
    },
    error: function (object)
    {
    }
    });
 }

function updateDetails()
{   
    var user = parserId();
    var data;
    var firstName = $("#firstName").val();
    var LastName = $("#lastName").val();
    var userName = $("#UserName").val();
    var email = $("#Email").val();
    var Emailval = validateEmail(email);
    var checkbox = $("#eq1").is(':checked');
    
    if (Emailval != true)
    {
        popupMessage("large", "Invalid email", "Email address Is not valid");
        return false;
    }
    var returnData;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, f_name: firstName, l_name: LastName, userName: userName, email: email, emailSub: checkbox
        }),
        url: "/Views/results.aspx/updateUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {     
            returnData = object.d;
            if (returnData == 101)
            {
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");
          
            }
            if (returnData == 201)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");
        
            }
            if (returnData == 301)
            {
                $("#emailError").removeClass("errorTextHide");
                $("#emailError").addClass("errorText centreText");
                $("#usernameError").removeClass("errorTextHide");
                $("#usernameError").addClass("errorText centreText");                
            }
            if (returnData == 0)
            {
                data = populateUserDetails(false);
                popupMessage("large", "details Changed", "Your Details have been changed");
                var email = CheckEmail(user);
                if (email == 1)
                {
                    emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Details Changed", data[0].FirstName + " " + data[0].LastName + ",</br> Your Details have been updated");
                }
            }
        },
        complete: function ()
        {
            populateUserDetails(true);   
            var mywindow = $("#window").data("kendoWindow");
            mywindow.close();
            document.cookie = "user=" + '<p>' + data[0].FirstName + ' ' + data[0].LastName + '&nbsp<img src="' + "" + '"/></p>';
            var cookie = $.cookie('user');
            $("#userData").html(cookie);
            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  
}

function populateUserDetails(pop)
{
    var user = parserId();
    
    var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                user: user
            }),
            url: "/Views/results.aspx/getUserDetails",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object)
            {
                data = object.d;
            },
            complete: function ()
            {
                if (pop == true)
                {
                    $("#firstName")[0].value = data[0].FirstName;
                    $("#lastName")[0].value = data[0].LastName;
                    $("#UserName")[0].value = data[0].UserName;
                    $("#Email")[0].value = data[0].Email;
                    $("#eq1").prop("checked",data[0].emailSub);
                }
            },
            error: function (object)
            {
                console.log("error" + object);
            }
    });

        return data;
       
}

function CheckEmail(userid)
{
    var returned;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            userId: userid
        }),
        url: "/Index.aspx/checkEmail",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            returned = object.d;
        },
        complete: function ()
        {
           
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return returned;
}


function CheckSeasonForUSer() {
    
    var user = parserId();
    if (user == -1)
    {
        return false;
    }
    var season;
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Views/results.aspx/getUserDetails",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {
            try
            {
                season = data[0].season;
            }
            catch (season)
            {
                if (season == undefined)
                {
                    season = 0;
                }}
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return season;
}

function validateEmail(emailField)
{
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField) == false)
    {
        return false;
    }
    return true;
}

function emailNotification(from,fromAlias,to,subject,mailBody)
{
    $.ajax({
        type: "post",
        data: JSON.stringify({
            from: from, fromAlias: fromAlias, to: to, subject: subject, mailBody: mailBody
        }),
        url: "/Index.aspx/mailNotification",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {           
        },
        complete: function ()
        {            
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });  

}

function checkSeasonPass(user)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Index.aspx/checkSeasonPass",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            data = object.d;
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });

    return data;

}

function getParameterByName(name, url)
{
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function nth(n) { return ["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th" }