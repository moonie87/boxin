﻿$(document).ready(function ()
{
    var weeks = getWeeksForSeason(1);
    getResultsforWeek(weeks);
    var id = parserId();
    getpredictedresults(id, weeks);

    $("#left").kendoButton({
        click: function ()
        {
            var val = parseInt($("#weekLabel").text());
            if (val == 1)
            {
            } else
            {
                $("#weekLabel").text(val - 1);
                $("#results").empty();
                getResultsforWeek(val - 1);
                $("#predictResults").empty();
                getpredictedresults(id, val - 1);
                calculatePoints();
                checkresults();
            }
        }
    });

    $("#right").kendoButton({
        click: function ()
        {
            var val = parseInt($("#weekLabel").text());
            if (val == weeks)
            {
            } else
            {
                $("#weekLabel").text(val + 1);
                $("#results").empty();
                getResultsforWeek(val + 1);
                $("#predictResults").empty();
                getpredictedresults(id, val + 1);
                calculatePoints();
                checkresults();
            }
        }
    });

    calculatePoints();
    checkresults();

});

function getWeeksForSeason(season)
{
    var length;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            season: season
        }),
        url: "/Views/Results.aspx/getWeeksforSeason",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");
            div.style = "overflow-y: auto; display:table; margin:auto;";

            var div1 = document.createElement("div");
            div1.style = "float:left; width:75%;";

            var div2 = document.createElement("div");
            div2.style = "float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-left: 1%;margin-right: 1%; margin-top:1%;";
            div2.className = "ActualText";

            var buttonDiv = document.createElement("div");

            var button = document.createElement("button");
            button.id = "left";
            button.type = "button";
            button.dataset;
            button.dataset.id = data[length - 1].week;
            button.style = "width: 45%; margin-left: 5%;";

            var leftButton = document.createElement("i");
            leftButton.className = "fa fa-arrow-left fa-lg";

            button.appendChild(leftButton);

            var weekLab = document.createElement("h3");
            weekLab.innerHTML = "Week";
            weekLab.style = "display:table; margin:auto; margin-left: 50px; margin-right: 50px;";

            var lab = document.createElement("h3");
            lab.id = "weekLabel";
            lab.innerHTML = data[length - 1].week;
            lab.style = "width:3%; font-weight: bolder;";
            lab.classList = "centreText";

            var break1 = document.createElement("br");

            var buttonr = document.createElement("button");
            buttonr.id = "right";
            buttonr.type = "button";
            buttonr.dataset;
            buttonr.dataset.id = data[length - 1].week;
            buttonr.style = "width:45%; margin-right:5%;";

            var rightButton = document.createElement("i");
            rightButton.className = "fa fa-arrow-right fa-lg";

            buttonr.appendChild(rightButton);

            div2.appendChild(weekLab);
            div2.appendChild(lab);
            div2.appendChild(break1);
            buttonDiv.appendChild(button);
            buttonDiv.appendChild(buttonr);

            div2.appendChild(buttonDiv);

            div.appendChild(div1);
            div.appendChild(div2);

            $("#weeks").append(div);
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function getResultsforWeek(week)
{

    $.ajax({
        type: "post",
        data: JSON.stringify({
            week: week
        }),
        url: "/Views/Results.aspx/getResultsforweek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");

            var tieArray = [];
            for (var i = 0; i < data.length; i++)
            {
                var bouttext;

                if (i == 0) {
                    bouttext = "Main Event"
                }
                else {
                    bouttext = "Undercard " + i;
                }

                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.className = "actualTextDiv";
                //div2.style = "min-width:100%; float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-right: 1%; margin-top:1%; font-size: xx-large;";


                var boutTextDiv = document.createElement("div");
                boutTextDiv.className = "actualTextDiv1";


                var boutText = document.createElement("label");
                boutText.innerHTML = "<h2>" + bouttext + "</h2>";
                boutText.className = "ActualText";


                var imageDiv = document.createElement("div");
                imageDiv.className = "imageDiv";
                //imageDiv.style = "float: left; width:100%;";

                if (data[i].outcomeText == "Draw")
                {
                    var image = document.createElement("img");
                    image.src = "/Content/draw3.png";
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                else
                {
                    var image = document.createElement("img");
                    image.src = data[i].link;
                    image.className = "actualImage"
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                imageDiv.appendChild(image);

                var LabelDiv = document.createElement("div");
                LabelDiv.className = "ActualText"
                //LabelDiv.style = "float: right; width: 60%;";

                if (data[i].outcomeText == "Draw")
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + i + ">Draw<h2/> " + "<h2 id=outcome" + i + "> " + data[i].outcomeText + "<h2/><h2 id=round" + i + "> Round: " + data[i].round + "<h2/>";
                    lab.style = "float:left; width:100%;";
                }
                else
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + i + ">" + data[i].boxerName + "<h2/> " + "<h2 id=outcome" + i + "> " + data[i].outcomeText + "<h2/><h2 id=round" + i + "> Round: " + data[i].round + "<h2/>";
                    lab.style = "float:left; width:100%;";
                }
                LabelDiv.appendChild(lab);

                var breaker = document.createElement("br");

                boutTextDiv.appendChild(boutText);

                div2.appendChild(boutTextDiv)
                div2.appendChild(imageDiv);
                div2.appendChild(LabelDiv);
                div2.appendChild(breaker);

                div.appendChild(div1);
                div.appendChild(div2);

                $("#results").append(div);

                tieArray.push(data[i].tieBreaker);

                if (i == data.length - 1)
                {
                    var min = Math.min.apply(Math, tieArray)

                    var tieDiv = document.createElement("div");
                    tieDiv.style = "float: right; width: 75%;";

                    var tieLab = document.createElement("label");
                    tieLab.id = "resultsLabel" + i;
                    tieLab.innerHTML = "<h1 id=TieResult" + i + ">Tie Breaker: &nbsp" + min + "</h1>";
                    tieLab.style = "float:left; width:100%;";

                    tieDiv.appendChild(tieLab);

                    $("#results").append(tieDiv);
                }
            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function getpredictedresults(user, week)
{
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, week: week
        }),
        url: "/Views/Results.aspx/getpredictedforweek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            var data = object.d;
            length = object.d.length;

            var div = document.createElement("div");
            var inc = 6;
            for (var i = 0; i < data.length; i++)
            {

                var bouttext;

                if (i == 0) {
                    bouttext = "Main Event"
                }
                else {
                    bouttext = "Undercard " + i;
                }

                var boutTextDiv = document.createElement("div");
                boutTextDiv.className = "actualTextDiv1";


                var boutText = document.createElement("label");
                boutText.innerHTML = "<h2>" + bouttext + "</h2>";
                boutText.className = "ActualText";

                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.id = "predictedBoxer" + i
                div2.className = "actualTextDiv";
                //div2.style = "min-width:100%; float:left;border-color: #e1e1e1; border-style:groove; border-radius: 15px; border-width: 5px; padding:10px;margin-right: 1%; margin-top:1%; font-size: xx-large;";

                var imageDiv = document.createElement("div");
                imageDiv.className = "imageDiv";
                //imageDiv.style = "float: left; width:35%;";

                if (data[i].outcomeText == "Draw")
                {
                    var image = document.createElement("img");
                    image.src = "/Content/draw3.png";
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                else
                {
                    var image = document.createElement("img");
                    image.src = data[i].link;
                    image.className = "actualImage";
                    //image.style = "float:left; width: 150px; height: 200px; border-style: solid; border-radius: 15px; border-width: 5px;";
                }
                imageDiv.appendChild(image);

                var LabelDiv = document.createElement("div");
                LabelDiv.className = "ActualText"
                //LabelDiv.style = "float: right; width: 60%;";
                if (data[i].outcomeText == "Draw")
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + inc + ">Draw<h2/> " + "<h2 id=outcome" + inc + "> " + data[i].outcomeText + "<h2/><h2 id=round" + inc + "> Round: " + data[i].round + "<h2/>";
                    inc++;
                    lab.style = "float:left; width:100%;";
                }
                else
                {
                    var lab = document.createElement("label");
                    lab.id = "resultsLabel" + i;
                    lab.innerHTML = "<h2 id=boxer" + inc + ">" + data[i].boxerName + "<h2/> " + "<h2 id=outcome" + inc + "> " + data[i].outcomeText + "<h2/><h2 id=round" + inc + "> Round: " + data[i].round + "<h2/>";
                    inc++;
                    lab.style = "float:left; width:100%;";
                }


                LabelDiv.appendChild(lab);

                var breaker = document.createElement("br");

                boutTextDiv.appendChild(boutText);

                div2.appendChild(boutTextDiv);
                div2.appendChild(imageDiv);
                div2.appendChild(LabelDiv);
                div2.appendChild(breaker);

                div.appendChild(div1);
                div.appendChild(div2);

                $("#predictResults").append(div);

                if (i == data.length - 1)
                {
                    var tieDiv = document.createElement("div");
                    tieDiv.style = "float: right; width: 75%;";

                    var tieLab = document.createElement("label");
                    tieLab.id = "resultsLabel" + i;
                    tieLab.innerHTML = "<h1 id=Tie" + inc + ">Tie Breaker: &nbsp" + data[i].tieBreaker + "</h1>";
                    inc++;
                    tieLab.style = "float:left; width:100%;";

                    tieDiv.appendChild(tieLab);

                    $("#predictResults").append(tieDiv);

                }

                // boxers corresponds
                // 1 > 6
                // 2 > 7
                // 3 > 8
                // 4 > 9
                // 5 > 10

            }
        },
        complete: function ()
        {
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });
    return length;
}

function calculatePoints()
{
    try
    {
        var Totpoints = 0;
        var correctBoxers = 0;
        var correctOutcomes = 0;
        var correctRounds = 0;

        // bout1
        var b0 = $("#boxer0")[0].innerText;
        var b6 = $("#boxer6")[0].innerText;
        if (b0 == b6)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O0 = $("#outcome0")[0].innerText;
        var O6 = $("#outcome6")[0].innerText;
        if (O0 == O6)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R0 = $("#round0")[0].innerText;
        var R6 = $("#round6")[0].innerText;
        if (R0 == R6)
        {
            Totpoints += 10;
            correctRounds += 1;
        }
        //bout2
        var b1 = $("#boxer1")[0].innerText;
        var b7 = $("#boxer7")[0].innerText;
        if (b1 == b7)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O1 = $("#outcome1")[0].innerText;
        var O7 = $("#outcome7")[0].innerText;
        if (O1 == O7)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R1 = $("#round1")[0].innerText;
        var R7 = $("#round7")[0].innerText;
        if (R1 == R7)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 3
        var b2 = $("#boxer2")[0].innerText;
        var b8 = $("#boxer8")[0].innerText;
        if (b2 == b8)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O2 = $("#outcome2")[0].innerText;
        var O8 = $("#outcome8")[0].innerText;
        if (O2 == O8)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R2 = $("#round2")[0].innerText;
        var R8 = $("#round8")[0].innerText;
        if (R2 == R8)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 4
        var b3 = $("#boxer3")[0].innerText;
        var b9 = $("#boxer9")[0].innerText;
        if (b3 == b9)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O3 = $("#outcome3")[0].innerText;
        var O9 = $("#outcome9")[0].innerText;
        if (O3 == O9)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R3 = $("#round3")[0].innerText;
        var R9 = $("#round9")[0].innerText;
        if (R3 == R9)
        {
            Totpoints += 10;
            correctRounds += 1;
        }

        // bout 5
        var b4 = $("#boxer4")[0].innerText;
        var b10 = $("#boxer10")[0].innerText;
        if (b4 == b10)
        {
            Totpoints += 3;
            correctBoxers += 1;
        }

        var O4 = $("#outcome4")[0].innerText;
        var O10 = $("#outcome10")[0].innerText;
        if (O4 == O10)
        {
            Totpoints += 7;
            correctOutcomes += 1;
        }

        var R4 = $("#round4")[0].innerText;
        var R10 = $("#round10")[0].innerText;
        if (R4 == R10)
        {
            Totpoints += 10;
            correctRounds += 1;
        }
    }
    catch (score) { }
    try
    {
        $("#boxersValue")[0].innerText = correctBoxers.toString();
        $("#outcomesValue")[0].innerText = correctOutcomes.toString();
        $("#roundsValue")[0].innerText = correctRounds.toString();
        $("#pointsValue")[0].innerText = Totpoints.toString();
    }
    catch (showPointValues) { }
}

function checkresults()
{
    var bout1 = 0;
    var bout2 = 0;
    var bout3 = 0;
    var bout4 = 0;
    var bout5 = 0;

    //bout1
    var boxer0 = $("#boxer0");
    var boxer6 = $("#boxer6");
    try {
        if (boxer0[0].innerText == boxer6[0].innerText) {
            boxer6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            boxer6[0].style.color = "Red";
        }
        var outcome0 = $("#outcome0");
        var outcome6 = $("#outcome6");
        if (outcome0[0].innerText == outcome6[0].innerText) {
            outcome6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            outcome6[0].style.color = "Red";
        }
        var round0 = $("#round0");
        var round6 = $("#round6");
        if (round0[0].innerText == round6[0].innerText) {
            round6[0].style.color = "Green";
            bout1 += 1;
        }
        else {
            round6[0].style.color = "Red";
        }
        if (bout1 == 3) {
            var div = $("#predictedBoxer0")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer0")[0].style.borderColor = "red";
        }

        //bout2
        var boxer1 = $("#boxer1");
        var boxer7 = $("#boxer7");
        if (boxer1[0].innerText == boxer7[0].innerText) {
            boxer7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            boxer7[0].style.color = "Red";
        }
        var outcome1 = $("#outcome1");
        var outcome7 = $("#outcome7");
        if (outcome1[0].innerText == outcome7[0].innerText) {
            outcome7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            outcome7[0].style.color = "Red";
        }
        var round1 = $("#round1");
        var round7 = $("#round7");
        if (round1[0].innerText == round7[0].innerText) {
            round7[0].style.color = "Green";
            bout2 += 1;
        }
        else {
            round7[0].style.color = "Red";
        }
        if (bout2 == 3) {
            var div = $("#predictedBoxer1")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer1")[0].style.borderColor = "red";
        }

        //bout3
        var boxer2 = $("#boxer2");
        var boxer8 = $("#boxer8");
        if (boxer2[0].innerText == boxer8[0].innerText) {
            boxer8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            boxer8[0].style.color = "Red";
        }
        var outcome2 = $("#outcome2");
        var outcome8 = $("#outcome8");
        if (outcome2[0].innerText == outcome8[0].innerText) {
            outcome8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            outcome8[0].style.color = "Red";
        }
        var round2 = $("#round2");
        var round8 = $("#round8");
        if (round2[0].innerText == round8[0].innerText) {
            round8[0].style.color = "Green";
            bout3 += 1;
        }
        else {
            round8[0].style.color = "Red";
        }
        if (bout3 == 3) {
            var div = $("#predictedBoxer2")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer2")[0].style.borderColor = "red";
        }

        //bout4
        var boxer3 = $("#boxer3");
        var boxer9 = $("#boxer9");
        if (boxer3[0].innerText == boxer9[0].innerText) {
            boxer9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            boxer9[0].style.color = "Red";
        }
        var outcome3 = $("#outcome3");
        var outcome9 = $("#outcome9");
        if (outcome3[0].innerText == outcome9[0].innerText) {
            outcome9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            outcome9[0].style.color = "Red";
        }
        var round3 = $("#round3");
        var round9 = $("#round9");
        if (round3[0].innerText == round9[0].innerText) {
            round9[0].style.color = "Green";
            bout4 += 1;
        }
        else {
            round9[0].style.color = "Red";
        }
        if (bout4 == 3) {
            var div = $("#predictedBoxer3")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer3")[0].style.borderColor = "red";
        }

        //bout5
        var boxer4 = $("#boxer4");
        var boxer10 = $("#boxer10");
        if (boxer4[0].innerText == boxer10[0].innerText) {
            boxer10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            boxer10[0].style.color = "Red";
        }
        var outcome4 = $("#outcome4");
        var outcome10 = $("#outcome10");
        if (outcome4[0].innerText == outcome10[0].innerText) {
            outcome10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            outcome10[0].style.color = "Red";
        }
        var round4 = $("#round4");
        var round10 = $("#round10");
        if (round4[0].innerText == round10[0].innerText) {
            round10[0].style.color = "Green";
            bout5 += 1;
        }
        else {
            round10[0].style.color = "Red";
        }
        if (bout5 == 3) {
            var div = $("#predictedBoxer4")[0].style.borderColor = "#06ff06";
        }
        else {
            var div = $("#predictedBoxer4")[0].style.borderColor = "red";
        }
        var tie4 = $("#TieResult4");
        var tie11 = $("#Tie11");
        if (tie4[0].innerText == tie11[0].innerText) {
            tie11[0].style.color = "Green";
        }
        else {
            tie11[0].style.color = "Red";
        }
    }
    catch (innerTextWarning) { }
}