﻿
    paypal.Button.render({

        env: 'sandbox', // Or 'sandbox',

        locale: 'en_GB',

        style:{
            size: 'large',
            color:'blue',
            shape: 'pill',
            label: 'buynow',
            branding:true,
        },

        client: {
            sandbox: 'AXcUgnH3yEqnqeX_Gc26di_GnkAgeRx25CuQEOjOAPZyz9XbknsbCM9XxQwi96sTl1Ii0x9rUvH408jB',
            production: 'ATgldSOyHnviIJskZC7FJ0eaRKRjbKGG_YiJ9_RX5XMowyfvS5m6SfgwsbZK9llBlVw90D2JnYBE_HC5'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function (data, actions) {
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '2.99', currency: 'GBP' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function (data, actions) {
            return actions.payment.execute().then(function (payment) {

                var t = payment.transactions["0"].related_resources["0"].sale;

                var p_id = payment.id;
                var id = t.id;
                var state = t.state;
                var user = parserId();

                var payer = payment.payer.payer_info;
                var email = payer.email;
                var fname = payer.first_name;
                var lname = payer.last_name;
                var payerId = payer.payer_id;

                $.ajax({
                    type: "post",
                    data: JSON.stringify({
                        userId: user,
                        Reciept: id,
                        parentPayId: p_id,
                        state: state,
                        email: email,
                        fname: fname,
                        lname: lname,
                        payerId:payerId
                    }),
                    url: "/Views/SeasonPass.aspx/AddReceipt",
                    dataType: "json",
                    async: false,
                    contentType: "application/json",
                    success: function () {
                        popupMessage("large", "Season Pass", "Thank you for purchasing a season pass. Good Luck!!");
                    },
                    complete: function ()
                    {
                        data = populateUserDetails(false);                       
                        emailNotification("forgotpassword@boxing5.com", "Boxing5", data[0].Email, "Season Pass Purchase", data[0].FirstName + " " + data[0].LastName + ",<br> This is your recipt for purchasing a season pass<br>"
                            + "your recipt id is :" + id + "<br> Name : " + fname + " " + lname + "<br> Email :" + email + "<br><br> <h2>GOOD LUCK THIS SEASON</h2>");
                        $("#seasonLabel")[0].innerText = "Season Pass Active";
                    },
                    error: function (object) {
                        console.log("error" + object);
                    }
                });             
            });
        },

        onError: function () {
            bootbox.alert({
                message: "Error purchasing",
                size: 'large'
            });
        }

    }, '#paypal-button');
