﻿var codes = [];

$(document).ready(function () {
    $("#createleague").kendoButton({
        click: function() {
            window.location.href = 'CreateLeague.aspx';
        }
    });

    $("#joinLeague").kendoButton({
        click: function() {
            window.location.href = 'joinLeague.aspx';
        }
    });
    var user = parserId();
    var leagues = getLeaguesForUser(user);

    for (var i = 0; i < leagues.length; i++)
    {  
        $("#FB" + codes[i]).click(function (e) {
            data = this.dataset.code;
            e.preventDefault();
            FB.ui(
            {
                method: 'share',
                href: "https://boxing5.com", 
                quote: "join my league on Boxing5.com. The code to enter on the leagues screen is: " + data,              
            });
        });

        $("#" + codes[i]).kendoButton({
            click: function ()
            {
                var league = this;
                var check = checkLeague(league);

                if (check == 0)
                {
                    bootbox.alert({
                        message: "You dont own this league, You can not delete it.",
                        size: 'large'
                    });
                }
                else if (check > 0)
                {
                    bootbox.confirm({
                        title: "Delete League?",
                        message: "Are you sure you want to delete this league? This cannot be undone.",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> No'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Yes'
                            }
                        },
                        callback: function (result)
                        {
                            if (result == true)
                            {
                                DeleteLeague(league);                                
                            }
                            window.location.href = 'Leagues.aspx';
                        }
                    });
                }
            }
        });
        $("#" + leagues[i] + i).kendoButton({
            click: function() {
                data = this.element[0].dataset.id;
                var newTData = getLeagues(data);

                $("#headerLeague").text(this.element[0].dataset.name);

                $('#leaguesTable').DataTable({
                    destroy: true,
                    "ordering": false,
                    responsive: true,
                    data: newTData,
                    columns: [
                        { data: 'position' },
                        { data: 'name' },
                        { data: 'correctResult' },
                        { data: 'correctRounds' },
                        { data: 'correctBoxers' },
                        { data: 'score' }
                    ],
                    "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                        if (aData.position == "1") {
                            $('td', nRow).css('background-color', 'Gold');
                        } else if (aData.position == "2") {
                            $('td', nRow).css('background-color', 'silver');
                        } else if (aData.position == "3") {
                            $('td', nRow).css('background-color', 'orange');
                        }
                    }
                });

                document.getElementById("leaguesTable_wrapper").scrollIntoView();
            }
        });
    }  
});

function getLeaguesForUser(user) {
    var leagues = [];
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user
        }),
        url: "/Views/Leagues.aspx/getLeaguesForUser",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
          
            data = object.d;

            var div = document.createElement("div");            
            div.style = "overflow-y: auto; margin-bottom: 5px;";

            for(var i = 0; i < data.length; i++)
            {
                var div1 = document.createElement("div");
                div1.style = "float:left; width:50%;";

                var div2 = document.createElement("div");
                div2.classList = "leagueContent";

                var label = document.createElement("label");
                label.innerHTML = data[i].Name;
                label.style = "display: table; margin: auto;";

                var button = document.createElement("input");
                button.id = data[i].Name + i;
                button.value = "View League";
                button.type = "button";
                button.title = "View League";
                button.dataset;
                button.dataset.id = data[i].parentId;
                button.dataset.name = data[i].Name;
                button.style = "margin-top: 10px !important; margin-bottom: 10px !important;display: table; margin: auto;";
                button.classList = "topMenu";

                var buttonDelete = document.createElement("button");
                buttonDelete.id = data[i].Code;
                buttonDelete.type = "button";
                buttonDelete.title = "Delete league";
                buttonDelete.dataset;
                buttonDelete.dataset.id = data[i].Code;                         

                var deleteIcon = document.createElement("i");
                deleteIcon.className = "fa fa-trash fa-lg";

                var codeLabel = document.createElement("label");
                codeLabel.id = "codeLabel";
                codeLabel.innerHTML = "League Code &nbsp";
                codeLabel.style = "display:table; margin:auto;";

                var code = document.createElement("Label");
                code.innerHTML = data[i].Code;
                code.style = "display:table; margin:auto; border-style:groove; border-radius: 10px; border-spacing:3px; margin-top:5px;";

                var codeshareFB = document.createElement("img");
                codeshareFB.src = "//login.create.net/images/icons/user/facebook_30x30.png";
                codeshareFB.id = "FB" + data[i].Code;
                codeshareFB.dataset;
                codeshareFB.dataset.code = data[i].Code;
                codeshareFB.className = "facebookShare";

                var codeshareT = document.createElement("a");
                codeshareT.href = "https://twitter.com/intent/tweet?text=I just created a league. Join the league: '"+ data[i].Name + "' head to boxing5.co.uk and join the league with this code: '"+ data[i].Code + "'#boxingApp";              
                codeshareT.id = "T" + data[i].Code;
                codeshareT.dataset;
                codeshareT.dataset.code = data[i].Code;

                var tPic = document.createElement("img");
                tPic.src = "//login.create.net/images/icons/user/twitter-b_30x30.png";

                var socialDiv = document.createElement("div");
                socialDiv.style = "float:right;";

                var socialLabel = document.createElement("label");
                socialLabel.innerHTML = "Share on&nbsp&nbsp";

                div2.appendChild(label);
                div2.appendChild(button);               
                div2.appendChild(codeLabel);
                div2.appendChild(code);
                // social media 
                buttonDelete.appendChild(deleteIcon);
                div2.appendChild(buttonDelete);

                codeshareT.appendChild(tPic);
                socialDiv.appendChild(socialLabel);
                socialDiv.appendChild(codeshareFB);
                socialDiv.appendChild(codeshareT);

                div2.appendChild(socialDiv);

                div.appendChild(div1);
                div.appendChild(div2);                
               
                $("#myLeagues").append(div);         
                
                leagues.push(data[i].Name);
                codes.push(data[i].Code);              
            }
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return leagues;
}

function getLeagues(leagueId)
{
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            leagueId:leagueId
        }),
        url: "/Views/Leagues.aspx/getLeagueforUser",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return data;
}

function DeleteLeague(league)
{
    var code = league.element[0].id;
    var user = parserId();
    
    $.ajax({
        type: "post",
        data: JSON.stringify({
            lCode: code, user:user
        }),
        url: "/Views/Leagues.aspx/DeleteLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function ()
        {            
        },
        complete: function ()
        {

            toastr.warning('Selections saved', 'Selection ');

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            //bootbox.alert({
            //    message: "League Deleted",
            //    size: 'large'
            //});
        },
        error: function (object)
        {
            console.log("error" + object);
        }
    });   
}

function checkLeague(league)
{
    var code = league.element[0].id;
    var user = parserId();

    var count;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            lCode: code, user: user
        }),
        url: "/Views/Leagues.aspx/checkLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object)
        {
            count = object.d;
        },
        complete: function ()
        {
        },
        error: function ()
        {            
        }
    });  
    return count;
}

function onChange() {
    alert("change");
};