﻿$(document).ready(function ()
{
    var data = [       
        { text: "Minimumweight", value: "1" },
        { text: "Light Flyweight", value: "2" },
        { text: "Flyweight", value: "3" },
        { text: "Super flyweight", value: "4" },
        { text: "Bantamweight", value: "5" },
        { text: "Super Bantamweight", value: "6" },
        { text: "Featherweight", value: "19" },
        { text: "Super Featherweight", value: "18" },
        { text: "Lightweight", value: "7" },
        { text: "Super lightweight", value: "8" },
        { text: "Welterweight", value: "16" },
        { text: "Super Welterweight", value: "17" },
        { text: "Middleweight", value: "9" },
        { text: "Super Middleweight", value: "10" },
        { text: "Light Heavyweight", value: "11" },
        { text: "Cruiserweight", value: "12" },
        { text: "Heavyweight", value: "13" },
    ];

    var Federationdata = [       
        { text: "WBA", value: "1" },
        { text: "IBF", value: "2" },
        { text: "IBO", value: "3" },
        { text: "WBO", value: "4" },
        { text: "WBC", value: "5" }

    ]

    // create DropDownList from input HTML element
    $("#Division").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onChangeFederation
    });

    // create DropDownList from input HTML element
    $("#Federation").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: Federationdata,
        index: 0,
        change: onChangeFederation
    });

    onChangeFederation();

    function onChangeFederation()
    {
        var value = $("#Federation").val();
        var text = $("#Federation").data("kendoDropDownList").text();

        var division = $("#Division").val();
        var DivText = $("#Division").data("kendoDropDownList").text();
       
        var newTData = getRankings(text, division);
        populateTable(newTData);
        $("#rankingsName").text(text + " " + DivText);
    }  
   
});
    function populateTable(data){
        var a = 0;
        var table = $('#rankingsTable').DataTable({

            destroy: true,
            "ordering": false,
            "paging": false,
            select: {
                style: 'os',
                blurable: true
            },

            data: data,
            columns: [

                { data: 'id' },
                { data: 'Boxing5Id'},
                { data: 'name' },
                { data: 'won' },
                { data: 'lost' },
                { data: 'drawn' },

            ],
           "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull)
           {
               if (aData.PosId == "1")
               {
                   $('td', nRow).css('background-color', 'Gold');
                   $('td:eq(0)', nRow).html('<i class="fa fa-trophy" aria-hidden="true"></i>');
               }

           
                    var table = $("#rankingsTable").DataTable();

                    $('#rankingsTable tbody').off('click');

                    $("#rankingsTable tbody").on('dblclick', 'tr', function ()
                    {
                        var rowData = table.rows(this).data().toArray();
                        console.log(rowData);
                        //var att = $(this).attr('id');
                        //var tr = $(this).closest("tr");
                        //var data = table.rows(tr).data();
                        //alert('you clicked' + data[0].name);
                    })                    
                }
            
        });      
    }

    function getRankings(federation,div) {

        var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                federation: federation, div:div
            }),
            url: "/Views/rankings.aspx/getRankings",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object) {
                data = object.d;
            },
            complete: function () {
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
        return data;
    }
