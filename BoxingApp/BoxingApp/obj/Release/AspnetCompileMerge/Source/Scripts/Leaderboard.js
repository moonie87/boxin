﻿
$(document).ready(function () {
   var data = getleaderboard();
    $('#example').DataTable({

        responsive: true,
        "ordering": false,
      
        data: data,
        columns: [
            { data: 'position' },
            { data: 'name' },
            { data: 'correctResult' },
            { data: 'correctRounds' },
            { data: 'correctBoxers' },
            { data: 'score' }

        ],
        "fnRowCallback": function (nRow, aData) {
            if (aData.position == "1") {
                $('td', nRow).css('background-color', 'Gold');
            }
            else if (aData.position == "2") {
                $('td', nRow).css('background-color', 'silver');
            }
            else if (aData.position == "3") {
                $('td', nRow).css('background-color', 'orange');
            }
        }
        
   });
    var seasonPassData = getSeasonPassleaderboard();
    $('#SeasonPassTable').DataTable({
        responsive: true,
        "ordering": false,
        

        data: seasonPassData,
        columns: [
            { data: 'position' },
            { data: 'name' },
            { data: 'correctResult' },
            { data: 'correctRounds' },
            { data: 'correctBoxers' },
            { data: 'score' }

        ],
        "fnRowCallback": function (nRow, aData) {
            if (aData.position == "1") {
                $('td', nRow).css('background-color', 'Gold');
            }
            else if (aData.position == "2") {
                $('td', nRow).css('background-color', 'silver');
            }
            else if (aData.position == "3") {
                $('td', nRow).css('background-color', 'orange');
            }
        }

    });

    getActiveSeasonandWeek();
});

function getleaderboard() {
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({          
        }),
        url: "/Views/Leaderboard.aspx/getLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function getSeasonPassleaderboard() {
    var data;
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getSeasonPassLeaderboard",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            data = object.d;
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return data;
}

function getActiveSeasonandWeek() {
   
    $.ajax({
        type: "post",
        data: JSON.stringify({
        }),
        url: "/Views/Leaderboard.aspx/getActiveSeasonAndWeek",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            $("#season").text('Season: ' + object.d[0].Season);
            $("#week").text('Week: ' + object.d[0].week);
        },
        complete: function () {

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

}