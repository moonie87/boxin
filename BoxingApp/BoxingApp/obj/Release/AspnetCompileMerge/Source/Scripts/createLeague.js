﻿$(document).ready(function () {

    $("#createLeagueBtn").kendoButton({
        click: function() {
            createLeague();
        }
    });
});

function createLeague() {
  
    var leagueName = $("#createLeagueInput").val();
    var user = parserId();

    $.ajax({
        type: "post",
        data: JSON.stringify({
            user: user, name:leagueName
        }),
        url: "/Views/Leagues.aspx/createLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function () {
           
        },
        complete: function ()
        {
            var data = populateUserDetails(false);
            var email = CheckEmail(user);
            if (email == 1)
            {
                emailNotification("feedback@boxing5.com", "Boxing5", data[0].Email, "League Created ", "<h3>" + data[0].FirstName + " " + data[0].LastName + "</h3><br> " +
                    "You created League - " + leagueName + "<br> Dont Forget to invite people to join your league. <br> Have Fun!");
            }
            window.location.href = 'Leagues.aspx';

        },
        error: function (object) {
            console.log("error" + object);
        }
    });

   
}

