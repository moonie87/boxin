﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Index.aspx.cs" Inherits="BoxingApp.Index" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <%--  <script src="/Global.js"></script>
    <script src="/Scripts/Login.js"></script>--%>
   
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">
     <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
    <div>
       
       <%--  <h1 class="centreText" style="color:white;">This site is currently in testing. please do not purchase a Season Pass!!</h1>--%>
         <div class="headerTitleSelection">
            <h1 class="centreText" style="font-size:30px;">Home</h1>
        </div>
        <div class="bodySelection row">
    <div class="centreText col-xl">
        <p class="centreText h1" style="font-size:3vw;">Predict the Winner, Outcome and Round of 5 bouts</p>
       <p class="centreText h2" style="font-size:3vw;">5 new bouts a week with a total of 16 rounds which makes up a season,</p>             
            <h2 class="centreText"  style="font-size:3vw;">A possible of 100 points are up for grabs each round.</h2><br />
        <h3 class="centreText"  style="font-size:4vw;">SCORE BREAKDOWN</h3>
        <h4 class="centreText"  style="font-size:3vw;">A Correct Round = <b>10</b> points</h4><br />
        <h4 class="centreText"  style="font-size:3vw;">A Correct Outcome = <b>7</b> points</h4><br />
        <h4 class="centreText"  style="font-size:3vw;">A Correct Boxer = <b>3</b> points</h4><br />
        <p>Selections close every Friday at 23:59 (UK TIME) and reopen Monday</p>
    <%--   <p class="centreText" style="font-size:5vw;">Play for fun or purchase a season pass for the chance to win Prizes.<br/>    --%>

       <%--</p>  --%>
        <h3 class="centreText" style="font-size:3vw;">JUST FOLLOW THESE 3 EASY STEPS TO PLAY<br/>   </h3>
    </div>
            <br />
    <div class="width100 col-xl">
        <div class="centreText row">
            <div class="col-md-4">
            <div class="round-circle">1</div><br /><h3>Register / Login</h3>
            </div>
             <div class="col-md-4"">
            <div class="round-circle">2</div><br /><h3>Pick your selections</h3>
                 </div>
             <div class="col-md-4">
            <div class="round-circle">3</div><br /><h3>Share and Invite</h3>
                 </div>
            <br />
            <br />           
        </div>     
    </div>
       <div class="container">
           <div class="row">
             <div class="col-md-4 col-sm-offset-2"> <input type="button" id="HomeRegisterBtn" value="Register"  class="width100 loginBtn topMenu k-button " /></div>
             <div class="col-md-4"><input type="button" id="HomeLoginBtn" value="Login"  class="width100 loginBtn topMenu k-button" /></div>
               </div>
           </div>
            </div>

     

</div>

</asp:Content>