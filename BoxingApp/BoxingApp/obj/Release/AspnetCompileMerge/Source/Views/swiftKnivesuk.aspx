﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="swiftKnivesuk.aspx.cs" Inherits="BoxingApp.Views.swiftKnivesuk" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>



    <!-- JQuery -->
     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->


        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/css/mdb.min.css" rel="stylesheet"/>

     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../knives/knivesStyle.css" rel="stylesheet" />
    <script src="../knives/knives.js"></script>
    <script src="../knives/bpenDrawer-master/js/bpen.drawer.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@4.0.0/dist/simpleParallax.min.js"></script>
     <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
</head>
<body>
    <form id="form1" runat="server">
   <div class="center-block">
              <img class="center-block" src="../knives/images/Swift%20logo%20White.png" style="width: 30%;padding: 1%;" />
                </div>

         <div id="mainMenu"></div>

        <div class="container">
      
        <%-- banner --%>
            <div class="row">
        <div style="float: none; margin: 0 auto;">
            
            <img src="../knives/images/49785038_515627008963165_4654600425471704450_n.jpg" class="banner" />

        </div>
                </div>

        <%-- parralax images --%>
        <div class="row">
            <div class="">
                
                <h3 class="textFont3">Every knife I make is made from scratch. This allows me to choose the steel for the blade, the wood for the handles and the materials for the pins. The only part of the knife making process that I do not do in house is the heat treatment and tempering. I outsource this important part of the knife making process because the company I use can produce extremely accurate and consistent results, that I would not currently be able to achieve in my current workshop. All knives are heat treated to 59 HRC.</h3>
            </div>           
                </div>
           
            <div class="row">
                <div style="float: none; margin: 0 auto;">
            <img class="thumbnail1" src="../knives/images/49848987_2155745411309565_8192936648900518853_n.jpg" alt="image" style='width:100%';/>
                 <div class="">
                <h3 class="textFont3"><b>Blade</b></h3><br/>
<h4 class="textFont3">All my knives have hi-carbon blades, using O1 / 52100 tool steel. Hi-carbon steel has a finer grain structure than stainless, which makes them much easier to sharpen, and as I make my knives to be used, keeping them sharp is a high priority.</h4>
               
            </div>
                </div>
                </div>
           
            <div class="row">
                <div  style="float: none; margin: 0 auto;">
            <img class="thumbnail2" src="../knives/images/52626395_126428941802021_5072567642770252786_n.jpg" alt="image" style='width:100%'; />
                 <div class="">
                <h3 class="textFont3" ><b>HANDLE</b></h3><br />
<h4 class="textFont3">Because I only need a small amount of wood for each knife, it allows me to choose the very best available. All the wood I use is hardwood, from Black African to stabilised Maple burls allowing me to compliment by blades beautifully.</h4>
                
            </div>
                </div>
                </div>
                      <div class="row">
                <div  style="float: none; margin: 0 auto;">
            <img class="thumbnail3" src="../knives/images/52351168_160509664945193_7912982316738951930_n.jpg" alt="image" style='width:100%'; />
                 <div class="">
                <h3 class="textFont3" ><b>PINS</b></h3><br />
<h4 class="textFont3">Each knife is finished with different pins that I feel best suit the blade and handle combination. I use many materials including mosaic pins, brass and silver steel.</h4>
                
            </div>
                </div>
                </div>          
        </div> 
        
         <!-- Footer -->
<footer class="page-footer font-large stylish-color">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Grid row-->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center">

          <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>          
          <!--Instagram-->
          <a href="https://www.instagram.com/swiftknivesuk/" class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"></i>
          </a>           
        </div>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
  <!-- Footer Elements -->

 <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://www.swiftknivesuk.com"> swiftknivesuk.com</a>
  </div>

</footer>
<!-- Footer -->

    </form> 
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/js/mdb.min.js"></script>

</body>
</html>
