﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="Register.aspx.cs" Inherits="BoxingApp.Views.Register" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <link href="/Css/Register.css" rel="stylesheet" />
    <script src="/Scripts/Register.js"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">

    <div>
          <div class="headerTitleSelection">
            <h3 class="centreText">Register</h3>
        </div>
         <div class="bodySelection">
        <div style="width: 100%;">
            <div class="width100">
                <div class="centreText">                   
                    <br />
                    <br />
                    <input type="text" id="RegisterUserNameInput" placeholder="UserName"  class="loginBox" /><br />
                    <br />
                    <input type="text" id="RegisterEmailInput" placeholder="Email"  class="loginBox"/><br />
                    <br />
                    <input type="text" id="RegisterFirstNameInput" placeholder="First Name"  class="loginBox" /><br />
                    <br />
                    <input type="text" id="RegisterLastNameInput" placeholder="Last Name"  class="loginBox" /><br />
                    <br />
                    <input type="password" id="RegisterPasswordInput" placeholder="Password" title="Password must be atleast 8 Charcters long"  class="loginBox" /><br />
                    <br />
                    <input type="password" id="RegisterPassword2Input" placeholder="Repeat Password" title="Password must be atleast 8 Charcters long"  class="loginBox" /><br />
                    <br />
                    <h3>By clicking Register you are agreeing to the Terms and Conditions.</h3>
                    <input type="button" id="RegisterBtn" value="Register"  class="width100 loginBtn" />
                     <div class="padding5px">
                     <a href="javascript:void(0);" onclick="RegisterFb()" id="fbRegLink" class="btn btn-block btn-social btn-facebook width100">
                        <span class="fa fa-facebook"></span>Register with Facebook
                    </a>
                         </div>
                     <input type="button" id="RegisterSigninBtn" value="Already Registered - Sign In"  class="width100 loginBtn" />
                </div>
            </div>        
        </div>
             </div>
    </div>
   

</asp:Content>
