﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/application.Master" CodeBehind="rankings.aspx.cs" Inherits="BoxingApp.Views.rankings" %>

<asp:Content ContentPlaceHolderID="extraStylesAndScripts" runat="server">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>  
    <script src="/Scripts/Rankings.js"></script>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContentPlaceHolder1" runat="server">  
     <div class="headerTitlesRanking">
         <h1 class="centreText">Top 10 Rankings</h1>
     </div>
    <div class="rankings">
    <div class="centreText">
        <label id="season"></label>
        &nbsp
       <label id="week"></label>
    </div>
    <div class="rankingsDiv">
        <label>Organisation</label>
        <input id="Federation" value="1" class="width100" />
    </div>
    <div class="rankingsDiv">
        <label>Division</label>
        <input id="Division" value="1" class="width100" style="width: 100% !important;" />
    </div> 
         <h1 id="rankingsName" class="centreText"></h1>
    <br />
    <br />
    <div >       
        <table id="rankingsTable" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Boxing5Id</th>
                    <th>Name</th>
                    <th>Won</th>
                    <th>Lost</th>
                    <th>Drawn</th>
                </tr>
            </thead>
        </table>
    </div>
        </div>
</asp:Content>
