﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contactKnives.aspx.cs" Inherits="BoxingApp.Views.contactKnives" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


        <!-- JQuery -->
     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


        <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/css/mdb.min.css" rel="stylesheet"/>

        
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
     <link href="../knives/knivesStyle.css" rel="stylesheet" />   
      <script src="../knives/bpenDrawer-master/js/bpen.drawer.js"></script>  
    <link href="../knives/form.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.js"></script>
    <script src="../knives/contact-form-script.js"></script>
    <script src="../knives/knives.js"></script>
     <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1"/>
</head>
<body>
   <div class="row">
        <div class="center-block">
              <img class="center-block" src="../knives/images/Swift%20logo%20White.png" style="width: 30%;padding: 1%;" />
                </div>

   
       </div>
<!-- Start Contact Form -->
<form role="form" id="contactForm" class="contact-form shake center-block" data-toggle="validator" style="width:70%;">
  <div class="form-group">
    <div class="controls">
      <input type="text" id="name" class="form-control" placeholder="Name" required data-error="Please enter your name"/>
      <div class="help-block with-errors"></div>
    </div>
  </div>
  <div class="form-group">
    <div class="controls">
      <input type="email" class="email form-control" id="email" placeholder="Email" required data-error="Please enter your email"/>
      <div class="help-block with-errors"></div>
    </div>
  </div>
  <div class="form-group">
    <div class="controls">
      <input type="text" id="msg_subject" class="form-control" placeholder="Subject" required data-error="Please enter your message subject"/>
      <div class="help-block with-errors"></div>
    </div>
  </div>
  <div class="form-group">
    <div class="controls">
      <textarea id="message" rows="7" placeholder="Massage" class="form-control" required data-error="Write your message"></textarea>
      <div class="help-block with-errors"></div>
    </div>  
  </div>

  <button type="submit" id="submit" class="btn btn-effect" style="color: white; border-color: white;"><i class="fa fa-check"></i> Send Message</button>
  <div id="msgSubmit" class="h3 text-center hidden"></div> 
  <div class="clearfix"></div>   
    
             <!-- Footer -->
<footer class="page-footer font-large stylish-color fixed-bottom">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Grid row-->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-12 py-5">
        <div class="mb-5 flex-center">

          <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"></i>
          </a>         
          <!--Instagram-->
          <a href="https://www.instagram.com/swiftknivesuk/" class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"></i>
          </a>         
        </div>
      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row-->

  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://www.swiftknivesuk.com"> swiftknivesuk.com</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->

     <div id="mainMenu"></div>

</form>     
<!-- End Contact Form -->

   <!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/js/mdb.min.js"></script> 
   
</body>
</html>
