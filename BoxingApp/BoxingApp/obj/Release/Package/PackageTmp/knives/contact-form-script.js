﻿$(document).ready(function () {


    $("#contactForm").validator().on("submit", function (event) {
       
        
        
            // everything looks good!
            event.preventDefault();
            submitForm();
        
    });
});

function submitForm() {
    // Initiate Variables With Form Content
    var name = $("#name").val();
    var email = $("#email").val();
    var msg_subject = $("#msg_subject").val();
    var message = $("#message").val();

    emailNotification("forgotpassword@boxing5.com", "Knives", "mark.b.griffiths@hotmail.com", msg_subject, message, email);
    formSuccess();

     $("#name").val("");
     $("#email").val("");
     $("#msg_subject").val("");
     $("#message").val("");

  
}

function formSuccess() {
    $("#contactForm")[0].reset();
    submitMSG(true, "Message Submitted!")
}

function formError() {
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $(this).removeClass();
    });
}

function submitMSG(valid, msg) {
    if (valid) {
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}