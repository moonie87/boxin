﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FixtureIndex.aspx.cs" Inherits="BoxingApp.Fixtures.Views.FixtureIndex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fixtures
    </title>

      <%--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--%>
     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/jquery.min.js"></script>
     <script src="https://kendo.cdn.telerik.com/2014.2.716/js/kendo.ui.core.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
     
    <link href="https://kendo.cdn.telerik.com/2014.2.716/styles/kendo.common.min.css" rel="stylesheet" />
    <link href="https://kendo.cdn.telerik.com/2014.2.716/styles/kendo.default.min.css" rel="stylesheet" /> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css" />
    <link rel="shortcut icon" type="image/png" href="Content/logo2.png" />    
    <script src="../Scripts/Fixtures.js"></script>
    <link href="../CSS/GlobalFixtures.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
    <div id="main">
        <div id="controls">
            <label>League Name</label> <input id="leagueName" type="text" title="Leauge" style="width: 20%;" class="k-textbox" />
           <label># Players</label> <input id="numeric" type="number" title="numeric" min="0" max="100" step="2" placeholder="Choose how many players Must be an EVEN number" style="width: 20%;" />
            <label># Legs</label> <input id="Legs" type="number" title="legs" min="0" max="2" step="1"  placeholder="Choose how many legs" style="width: 20%;" />
           <label>League Password</label> <input type="password" id="LeaguePasswordInput" placeholder="Password" class="k-textbox"/><br />
             <div class="container">
    <div id="players" class="span12 text.center"></div>
             </div>  

        </div>
        <div class="container">
    <div id="fixtures" class="span12 text.center"></div>
             </div>   
    </div>
    </form>
</body>
</html>
