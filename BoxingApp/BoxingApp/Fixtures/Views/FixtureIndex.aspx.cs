﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BoxingApp.Fixtures.Views
{
    public partial class FixtureIndex : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public class FixturesConnection
        {
            private static string connection = string.Empty;
            public static string Connection
            {
                get
                {
                    if (connection.Length == 0)
                    {
                         connection = ConfigurationManager.ConnectionStrings["fixturesConnection"].ConnectionString;

                        //connection = "server=db728600826.db.1and1.com;Initial Catalog=db728600826; uid=dbo728600826; pwd=Moonie1987!;";
                    }
                    return connection;
                }
            }
        }
        [WebMethod]
        public static int addFixture(int HomeId, int AwayId, int LeagueId, int Round)
        {
            var returnedVal = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(FixturesConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.AddFixture]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@Homeid", HomeId));
                    stmt.Parameters.Add(new SqlParameter("@awayid", AwayId));
                    stmt.Parameters.Add(new SqlParameter("@leagueid", LeagueId));
                    stmt.Parameters.Add(new SqlParameter("@round", Round));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    returnedVal = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return returnedVal;
        }

        [WebMethod]
        public static int GetUserId(string name, int league_Id)
        {
            var returnedVal = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(FixturesConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.findUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@name", name));
                    stmt.Parameters.Add(new SqlParameter("@league_id", league_Id));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    returnedVal = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return returnedVal;
        }

        [WebMethod]
        public static int CreateLeague(string name, int players, string code, string password,int rounds )
        {
            var returnedVal = 0;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(FixturesConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.CreateLeague]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@name", name));
                    stmt.Parameters.Add(new SqlParameter("@players", players));
                    stmt.Parameters.Add(new SqlParameter("@code", code));
                    stmt.Parameters.Add(new SqlParameter("@password", password));
                    stmt.Parameters.Add(new SqlParameter("@rounds", rounds));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    returnedVal = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
               
            }
            return returnedVal;
        }

        [WebMethod]
        public static int CreatePlayerForLeague(string name, int league_Id)
        {
            var returnedVal = 0;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(FixturesConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Leagues.CreatePlayerForLeague]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@name", name));
                    stmt.Parameters.Add(new SqlParameter("@league_id", league_Id));                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    returnedVal = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {

            }
            return returnedVal;
        }

        public class fixture
        {
            public Int64 team1 { set; get; }
            public Int64 team2 { set; get; }

        }

        [WebMethod]
        public static ArrayList gfixture(int teams, int legs)
        {
            ArrayList lst1 = new ArrayList();
            ArrayList lst2 = new ArrayList();
            ArrayList lst3 = new ArrayList();
            int roundsnum = legs;
            int totalRounds = teams - 1;
            int matchesPerRound = teams / 2;
            String[][] rounds = new String[totalRounds][];           
            for (int i = 0; i < totalRounds; i++)
            {
                rounds[i] = new String[matchesPerRound];               
            }

            for (int round = 0; round < totalRounds; round++)
            {
                for (int match = 0; match < matchesPerRound; match++)
                {
                    int home = (round + match) % (teams - 1);
                    int away = (teams - 1 - match + round) % (teams - 1);
                    if (match == 0)
                    {
                        away = teams - 1;
                    }
                   rounds[round][match] = "Team:" + (home + 1) + " * Team:" + (away + 1);
                   lst2.Add("Team:" + (home + 1) + " * Team:" + (away + 1));
                }
            }
            // Interleave so that home and away games are fairly evenly dispersed.
            String[][] interleaved = new String[totalRounds][];
            for (int i2 = 0; i2 < totalRounds; i2++)
            {
                interleaved[i2] = new String[matchesPerRound];
            }
            int evn = 0;
            int odd = (teams / 2);
            for (int i = 0; i < rounds.Length; i++)
            {
                if (i % 2 == 0)
                {
                    interleaved[i] = rounds[evn++];
                }
                else
                {
                    interleaved[i] = rounds[odd++];
                }
            }
            rounds = interleaved;
            for (int round = 0; round < rounds.Length; round++)
            {
                if (round % 2 == 1)
                {
                    rounds[round][0] = flip(rounds[round][0]);
                }
            }
            
           // lst1.Add(rounds);

            {
                if (roundsnum == 2)
                {
                    for (int round = 0; round < rounds.Length; round++)
                        for (int j = 0; j < matchesPerRound; j++)
                           lst3.Add(flip(rounds[round][j]));

                    //               for (int i = 0; i < rounds.Length; i++) 
                    //{					
                    //	for(int j= 0;j< matchesPerRound ;j++)
                    //		lst3.Add(rounds[i][j]);
                    //}
                   
                    lst1.Add(lst3);
                }
                lst1.Add(lst2);
            }
            return lst1;
        }

        private static String flip(String match)
        {
            String[] components = match.Split('*');
            return components[1] + " * " + components[0];
        }
    }
}