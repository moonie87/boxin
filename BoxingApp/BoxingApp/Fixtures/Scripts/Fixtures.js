﻿var totRounds;
var matchesperRound;
var Totplayers;

$(document).ready(function () {  

    // create NumericTextBox from input HTML element using custom format
    $("#numeric").kendoNumericTextBox({
        change: onChange,
        spin: onSpin
    });

    function onChange() {
        console.log("Change :: " + this.value());
    }

    function onSpin() {
        var players = $("#numeric").data("kendoNumericTextBox");
        var number = players.value();
        $("#players").html('');
        $("#players").append('<label class="centreText">Enter Player Names</label>')
        for (var i = 1; i <= number; i++)
        {
            $("#players").append('<label class="centreText">player' + i + '</label></br> <input class="centreText" id="team' + i + '" type="text" title="players" style="width: 20%;" class="k-textbox" /><br>');
        }
        $("#players").append('<button id="submit" class="k-primary centreText">Create</button>');

        $("#submit").kendoButton();

        $("#submit").click(function (e) {
            e.preventDefault();
            $("#fixtures").html('');
            var leagueName = $("#leagueName").val();
            var password = $("#LeaguePasswordInput").val();
            var players = $("#numeric").data("kendoNumericTextBox");
            var legs = $("#Legs").data("kendoNumericTextBox");
            Totplayers = players.value();
            matchesperRound = Totplayers / 2;
            totRounds = (Totplayers - 1) * legs.value();

           var leagueId = createLeague(leagueName, Totplayers, password, totRounds);
           var FixturesCount = 0;
           var roundCount = 0;
            var results = fixtures(players.value(), legs.value())
            for (var p = 0; p <= legs.value() - 1; p++) {
                var text = p + 1;
                $("#fixtures").append('<label class="centreText">Leg  ' + text + '</label></br>');
                for (var x = 0; x <= results[p].length - 1; x++) {
                    roundCount++;
                    $("#fixtures").append('<label class="centreText" id="round' + roundCount + '">Round: ' + roundCount + '</label></br>');
                    for (var w = 0; w <= results[p][x].length - 1; w++) {
                        FixturesCount++;
                        $("#fixtures").append('<label class="centreTextNoFont" id="fixture' + FixturesCount + '">' + results[p][x][w] + "*"+ roundCount+ '</label></br>');
                    }
                }
            }
            for (var p = 1; p <= Totplayers; p++)
            {                
                var reg = new RegExp("Team:" + p, "g");
                var text1 = $("#team" + p).val();
                $("#fixtures").html($("#fixtures").html().replace(reg, text1));
                var userID = addUser(text1, leagueId);
            }

           
                for (var v = 1; v <= FixturesCount; v++) {
                    var fix1 = $("#fixture" + v)[0].innerHTML;                   
                    var Splitplayers = splitString(fix1, "*");
                    var player1 = Splitplayers[0].trim();
                    var player2 = Splitplayers[1].trim();
                    var round = Splitplayers[2].trim();
                    var player1Id = getUserId(player1, leagueId);
                    var player2Id = getUserId(player2, leagueId);
                    addFixture(player1Id, player2Id, leagueId, round);
                }            
        })
    }
    $("#Legs").kendoNumericTextBox();
});

function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function addUser(name, League) {

    var userId;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            name: name, league_Id: League
        }),
        url: "/Fixtures/Views/FixtureIndex.aspx/CreatePlayerForLeague",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            userId = object.d;
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return userId;
}

function createLeague(name, players, password, rounds) {  

    var code = makeid(6);
        var data;
        $.ajax({
            type: "post",
            data: JSON.stringify({
                name: name, players: players, code:code, password:password,rounds:rounds
            }),
            url: "/Fixtures/Views/FixtureIndex.aspx/CreateLeague",
            dataType: "json",
            async: false,
            contentType: "application/json",
            success: function (object) {
                data = object.d;
            },
            complete: function () {
            },
            error: function (object) {
                console.log("error" + object);
            }
        });
        return data;
    }

function getUserId(name, League) {

    var userId;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            name: name, league_Id:League
        }),
        url: "/Fixtures/Views/FixtureIndex.aspx/GetUserId",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            userId = object.d;
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return userId;
}

function addFixture(homeId,awayId, leagueId, round) {

    var FixtureId;
    $.ajax({
        type: "post",
        data: JSON.stringify({
            HomeId: homeId, AwayId: awayId, LeagueId:leagueId, Round:round
        }),
        url: "/Fixtures/Views/FixtureIndex.aspx/addFixture",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            FixtureId = object.d;
        },
        complete: function () {
        },
        error: function (object) {
            console.log("error" + object);
        }
    });
    return FixtureId;
}


function fixtures(teams,legs) {
    var returned;
    var master = [];
    $.ajax({
        type: "post",
        data: JSON.stringify({
            teams: teams,
            legs: legs
        }),
        url: "/Fixtures/Views/FixtureIndex.aspx/gfixture",
        dataType: "json",
        async: false,
        contentType: "application/json",
        success: function (object) {
            returned = object.d;
            for (var p = 0; p <= legs - 1; p++) {
                var obj = [];
                var counter = 0;

                for (var i = 0; i <= totRounds; i++) {
                    var roundnew = [];
                    for (var j = 0; j <= matchesperRound - 1; j++) {

                        var match = [];
                        match.push(returned[p][counter])
                        roundnew.push(match);
                        
                        if (counter != returned[p].length) {
                            counter++;                            
                        }
                        else {
                            break;
                        }
                    }                                   
                    obj.push(roundnew);
                    if (counter == returned[p].length) {
                        break;
                    }
                }
                master.push(obj);
            }
        },
        complete: function () {
           
            
        },
        error: function (object) {
            console.log("error" + object);
        }
    });

    return master;
}

function splitString(string, Character) {
    var players = string.split(Character)

    return players;
}