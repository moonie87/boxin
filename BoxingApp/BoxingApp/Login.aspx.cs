﻿using System;
using System.Web.Services;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using BoxingApp.Views;
using System.Web.Security;
using System.Net.Mail;
using System.Net;

namespace BoxingApp
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static int LoginUser(string userName, string Password, string outhProvider, string outhId)
        {
            var id = 0;
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Users_LoginUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userName", userName));
                    stmt.Parameters.Add(new SqlParameter("@Password", Password));
                    stmt.Parameters.Add(new SqlParameter("@outhprovider", outhProvider));
                    stmt.Parameters.Add(new SqlParameter("@outhId", outhId));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return id;
        }

        [WebMethod]
        public static ArrayList getUserDetails(Int64 user)
        {
            ArrayList UserDetailsList = new ArrayList();
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Users_GetDetails]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        userDetails newobj = new userDetails();

                        newobj.id = reader.GetInt32(0);
                        newobj.FirstName = reader.GetString(1);
                        newobj.LastName = reader.IsDBNull(2) ? string.Empty : reader.GetString(2);
                        newobj.email = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);
                        newobj.seasonPass = (bool)reader.GetSqlBoolean(5);
                        newobj.UserName = reader.IsDBNull(8) ? string.Empty : reader.GetString(8);

                        UserDetailsList.Add(newobj);
                    }
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return UserDetailsList;
        }

        [WebMethod]
        public static int ResetupdatePassword(string code, string password)
        {
            int id = 0;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {
                    SqlCommand command = new SqlCommand();
                    SqlCommand stmt = new SqlCommand();

                    stmt.CommandText = "[Users_ResetPassword]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@code", code));
                    stmt.Parameters.Add(new SqlParameter("@password", password));

                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    id = (int)returnParamater.Value;
                    FN_DB.Close();

                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return id;
        }

        [WebMethod]
        public static string sendUserName(string email)
        {
            var userName = "";           
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))

                //  using (SqlConnection FN_DB = new SqlConnection("server=db696188249.db.1and1.com; initial catalog=db696188249;uid=dbo696188249;pwd=Moonie1987!"))
                {
                    SqlCommand command = new SqlCommand();
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Users_sendUserName]";
                    stmt.CommandType = CommandType.StoredProcedure;                    
                    stmt.Parameters.Add(new SqlParameter("@email", email));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        resetPassword reset = new resetPassword();
                        userName = reader.GetString(0);
                    }

                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            //MailMessage mail = new MailMessage();

            //if (username.Length > 0)
            //{
            //    id = 1;
            //    // set mail addreses
            //    mail.From = new MailAddress("forgotpassword@boxing5.com", "Boxing5");
            //    mail.To.Add(email);

            //    // set content 
            //    mail.Subject = " Boxing5 Username";
            //    mail.Body = "<br/><br/> Thank you for requesting your Username <br/> " +
            //        "<br/>Your Username is <br/><br/>"  + username +
            //        "<br/><br/>Your support is very much appreciated." +
            //        "<br/>If you have any ideas or requests please don't hesitate to contact me." +
            //        "<br/><br/>" +
            //        "Mark Griffiths <br/><br/><br/><br/><br/>" +
            //        "<a href= 'http://www.boxing5.com'><img alt = 'follow me on twitter' src ='http://boxing5.com/Content/logoresized.png' style='width:105px;'></a>" +
            //        "<a href= 'https://twitter.com/Boxing5dotcom'><img alt = 'follow me on twitter' src ='https://support.content.office.net/en-us/media/0da17e2a-9ed5-496a-b161-9fd4491ce5c7.png' border = 0 ></a>" +
            //        "<a href= 'http://www.facebook.com/Boxing5dotcom/'><img alt = 'follow me on facebook' src ='https://support.content.office.net/en-us/media/dc36f8aa-3677-4a31-aea2-04f8525a8131.png' border = 0 ></a>";

            //    mail.IsBodyHtml = true;

            //    SmtpClient smtp = new SmtpClient("auth.smtp.1and1.co.uk");
            //    smtp.Credentials = new NetworkCredential("forgotpassword@boxing5.com", "Moonie1987!");
            //    smtp.Send(mail);
            //}
            return userName;
        }
    

    [WebMethod]
        public static int resetUserCode(string email)
        {
            int ret = 0;
            var code = "";
            Random rand = new Random();

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))

                //  using (SqlConnection FN_DB = new SqlConnection("server=db696188249.db.1and1.com; initial catalog=db696188249;uid=dbo696188249;pwd=Moonie1987!"))
                {
                    SqlCommand command = new SqlCommand();
                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Users_resetPasswordCode]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@rand", finalString));
                    stmt.Parameters.Add(new SqlParameter("@email", email));
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        resetPassword reset = new resetPassword();

                        code = reader.GetString(0);
                    }
                    FN_DB.Close();

                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            MailMessage mail = new MailMessage();

            if (code.Length > 2)
            {
                ret = 1;
                // set mail addreses
                mail.From = new MailAddress("forgotpassword@boxing5.com","Boxing5");
                mail.To.Add(email);

                // set content 
                mail.Subject = " Boxing5 Password Reset";
                mail.Body = "<div style='background-color:black;'>" +
                 "<div style='background-color:#132774;'><a href= 'https://www.boxing5.com'><img alt = 'follow me on twitter' src ='https://boxing5.com/Content/logoresized.png' style='width:105px; display:table; margin:auto;'></a></div>"
                 + "<div style='display:table; margin:auto; color:white;'><br/><br/> Thank you for resetting your password <br/>" +
                         "<br/>Your code to reset your password is <br/><br/>" + code + "</div>" + "<br/><br/><p style='color: white; display: table; margin: auto;'>Your support is very much appreciated." +
                     "<br/>If you have any ideas or requests please don't hesitate to contact us.</p>" +
                     "<br/><br/>" +
                     "<div style='display:table; margin:auto; color:white'>Founder & Lead Developer </br> Mark Griffiths <br/><br/></div>" +
                     "<div style='background-color:#132774;'><div style='display:table; margin:auto; background-color:#132774;'><a href= 'https://twitter.com/Boxing5dotcom'><img alt = 'follow me on twitter' src ='https://login.create.net/images/icons/user/twitter-b_40x40.png' border = 0 ></a>" +
                     "<a href= 'https://www.facebook.com/Boxing5dotcom/'><img alt = 'follow me on facebook' src ='https://login.create.net/images/icons/user/facebook_40x40.png' border = 0 ></a><a href='http://boxing5.com/Views/Unsubscribe.aspx?email=" + email + "'style='color: white;display:table;margin:auto;text-decoration:none; '>unsubscribe </a></div></div></div>";

                mail.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("auth.smtp.1and1.co.uk");
                smtp.Credentials = new NetworkCredential("forgotpassword@boxing5.com", "Moonie1987!");
                smtp.Send(mail);
            }
            return ret;
        }
    }

    public class userDetails
    {
        public Int32 id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string email { set; get; }
        public bool seasonPass { set; get; }
        public string UserName { set; get; }


        // public Int64 userId { set; get; }
    }

    public class resetPassword
    {
        public string resetcode { get; set; }
    }
}