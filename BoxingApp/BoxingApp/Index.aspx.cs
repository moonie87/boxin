﻿using System;
using System.Web.Services;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System.Collections.Generic;
using System.IO;
using System.Configuration;
using BoxingApp.Views;
using System.Web.Security;
using System.Net.Mail;
using System.Net;

namespace BoxingApp
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

      

        [WebMethod]
        public static void mailNotification(string from, string fromAlias, string to, string subject, string mailBody )
        {           
            try
            {
                MailMessage mail = new MailMessage();
                
                    // set mail addreses
                    mail.From = new MailAddress(from, fromAlias);
                    mail.To.Add(to);

                    // set content 
                    mail.Subject = subject;
                    mail.Body = "<div style='background-color:black;'>" +
                    "<div style='background-color:#132774;'><a href= 'https://www.boxing5.com'><img alt = 'follow me on twitter' src ='https://boxing5.com/Content/logoresized.png' style='width:105px; display:table; margin:auto;'></a></div>"
                    + "<div style='display:table; margin:auto; color:white;'>" + mailBody + "</div>" +
                        "<br/><br/><p style='color: white; display: table; margin: auto;'>Your support is very much appreciated." +
                     "<br/>If you have any ideas or requests please don't hesitate to contact us.</p>" +
                        "</br></br><div style='display:table; margin:auto; color:white'>Founder & Lead Developer </br> Mark Griffiths <br/><br/></div>" +
                        "<div style='background-color:#132774;'><div style='display:table; margin:auto; background-color:#132774;'><a href= 'https://twitter.com/Boxing5dotcom'><img alt = 'follow me on twitter' src ='https://login.create.net/images/icons/user/twitter-b_40x40.png' border = 0 ></a>" +
                        "<a href= 'https://www.facebook.com/Boxing5dotcom/'><img alt = 'follow me on facebook' src ='https://login.create.net/images/icons/user/facebook_40x40.png' border = 0 ></a><a href='https://boxing5.com/Views/Unsubscribe.aspx?email=" + to + "'style='color: white;display:table;margin:auto;text-decoration:none; '>unsubscribe </a></div></div></div>";

                    mail.IsBodyHtml = true;

                    SmtpClient smtp = new SmtpClient("auth.smtp.1and1.co.uk");
                    smtp.Credentials = new NetworkCredential(from, "Moonie1987!");
                    smtp.Send(mail);
                
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            
        }

        [WebMethod]
        public static int checkEmail(int userId)
        {
            var returnedVal = 0;
          
            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    stmt.CommandText = "[Users_CheckEmail]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", userId));                   
                    stmt.Connection = FN_DB;
                    FN_DB.Open();

                    var returnParamater = stmt.Parameters.Add("@return value", SqlDbType.Int);
                    returnParamater.Direction = ParameterDirection.ReturnValue;
                    stmt.ExecuteNonQuery();
                    returnedVal = (int)returnParamater.Value;
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return returnedVal;
        }


        [WebMethod]
        public static bool checkSeasonPass(int user)
        {
            var returnedVal = false;

            try
            {
                using (SqlConnection FN_DB = new SqlConnection(BoxingAppConnection.Connection))
                {

                    SqlCommand stmt = new SqlCommand();
                    SqlDataReader reader;
                    stmt.CommandText = "[Users_GetSeasonPassForUser]";
                    stmt.CommandType = CommandType.StoredProcedure;
                    stmt.Parameters.Add(new SqlParameter("@userId", user));                  
                    stmt.Connection = FN_DB;
                    FN_DB.Open();
                    reader = stmt.ExecuteReader();
                    while (reader.Read())
                    {
                        returnedVal = reader.GetBoolean(0); 
                    }                   
                    FN_DB.Close();
                }
            }
            catch (Exception ex)
            {
                var errorTxt =
                    $"Error [{ex.Message}] occurred in {MethodBase.GetCurrentMethod().DeclaringType}::{MethodBase.GetCurrentMethod().Name}";
            }
            return returnedVal;
        }

    }
}